<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
    // return view('welcome');
});





// Route::get('/template1', function () {
//     return view('template.macb4');
// })->name('template1');


Route::get('/guest_template', function () {
    return view('template.guest_metronic');
});




Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/list_pengiriman', 'DashboardController@list_pengiriman')->name('dashboard_list_pengiriman');
    Route::get('/dashboard/list_penitipan', 'DashboardController@list_penitipan')->name('dashboard_list_penitipan');
    // Route::get('/dashboard', function () {
    //     return view('dashboard');
    // })->name('dashboard');

    Route::get('/master', 'MenuController@index')->name('master_view_index');
    Route::get('/master/list', 'MenuController@list')->name('master_view_list');

    Route::get('/dashboard/profile', 'DashboardController@profile')->name('dashboard_view_profile');
    Route::post('/dashboard/change', 'DashboardController@change')->name('dashboard_view_change');


    Route::group(['middleware' => 'dynamicprivilege'], function () {


        // Route::get('user', function () {
        //     return view('master.master_user.welcome');
        // })->name('user_view');
        // Route::get('/user/create', function () {
        //     return view('master.master_user.welcome');
        // })->name('user_create');
        // Route::get('/user/update', function () {
        //     return view('master.master_user.welcome');
        // })->name('user_update');
        // Route::get('/user/delete', function () {
        //     return view('master.master_user.welcome');
        // })->name('user_delete');

        Route::get('/user/list', 'Master\MasterUserController@list')->name('user_view_list');
        Route::get('/user/import', 'Master\MasterUserController@import')->name('user_import_import');
        Route::get('/user/export', 'Master\MasterUserController@export')->name('user_export_export');
        Route::resource('user', 'Master\MasterUserController', [
            'names' => [
                'index'     => 'user_view_index',
                'show'      => 'user_view_show',
                'create'    => 'user_create_create', // user/create with methode GET
                'store'     => 'user_create_store', // user with methode POST
                'edit'      => 'user_update_edit', // user/{{id}}/edit with methode GET
                'update'    => 'user_update_update', // user/{{id}} with methode PUT
                'destroy'   => 'user_delete_destroy', // user/{{id}} with methode DELETE
            ]
        ]);


        Route::get('/carousel/list', 'Master\MasterCarouselController@list')->name('carousel_view_list');
        Route::resource('carousel', 'Master\MasterCarouselController', [
            'names' => [
                'index'     => 'carousel_view_index',
                'show'      => 'carousel_view_show',
                'create'    => 'carousel_create_create',
                'store'     => 'carousel_create_store',
                'edit'      => 'carousel_update_edit',
                'update'    => 'carousel_update_update',
                'destroy'   => 'carousel_delete_destroy',
            ]
        ]);

        Route::get('/groupmenu/list', 'Master\MapGroupMenuController@list')->name('groupmenu_view_list');
        Route::resource('groupmenu', 'Master\MapGroupMenuController', [
            'names' => [
                'index'     => 'groupmenu_view_index',
                'show'      => 'groupmenu_view_show',
                'create'    => 'groupmenu_create_create',
                'store'     => 'groupmenu_create_store',
                'edit'      => 'groupmenu_update_edit',
                'update'    => 'groupmenu_update_update',
                'destroy'   => 'groupmenu_delete_destroy',
            ]
        ]);

        Route::get('kalkulasiharga', 'Pengiriman\KalkulasiHargaController@index')->name('kalkulasiharga_view_index');
        Route::post('kalkulasiharga/calculate', 'Pengiriman\KalkulasiHargaController@price')->name('kalkulasiharga_view_calculate');
        
        Route::get('/pengirimanbarang/payment/{pengirimanbarang}', 'Pengiriman\PengirimanBarangController@payment')->name('pengirimanbarang_update_payment');
        Route::get('/pengirimanbarang/list', 'Pengiriman\PengirimanBarangController@list')->name('pengirimanbarang_view_list');
        // Route::get('/pengirimanbarang/import', 'Pengiriman\PengirimanBarangController@import')->name('pengirimanbarang_import_import');
        Route::post('/pengirimanbarang/calculate', 'Pengiriman\PengirimanBarangController@price')->name('pengirimanbarang_view_calculate');
        Route::get('/pengirimanbarang/export', 'Pengiriman\PengirimanBarangController@export')->name('pengirimanbarang_view_export');
        Route::resource('pengirimanbarang', 'Pengiriman\PengirimanBarangController', [
            'names' => [
                'index'     => 'pengirimanbarang_view_index',
                'show'      => 'pengirimanbarang_view_show',
                'create'    => 'pengirimanbarang_create_create', // pengirimanbarang/create with methode GET
                'store'     => 'pengirimanbarang_create_store', // pengirimanbarang with methode POST
                'edit'      => 'pengirimanbarang_update_edit', // pengirimanbarang/{{id}}/edit with methode GET
                'update'    => 'pengirimanbarang_update_update', // pengirimanbarang/{{id}} with methode PUT
                'destroy'   => 'pengirimanbarang_delete_destroy', // pengirimanbarang/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/penitipanbarang/list', 'Pengiriman\PenitipanBarangController@list')->name('penitipanbarang_view_list');
        // Route::get('/penitipanbarang/import', 'Pengiriman\PenitipanBarangController@import')->name('penitipanbarang_import_import');
        Route::get('/penitipanbarang/export', 'Pengiriman\PenitipanBarangController@export')->name('penitipanbarang_view_export');
        Route::resource('penitipanbarang', 'Pengiriman\PenitipanBarangController', [
            'names' => [
                'index'     => 'penitipanbarang_view_index',
                'show'      => 'penitipanbarang_view_show',
                'create'    => 'penitipanbarang_create_create', // penitipanbarang/create with methode GET
                'store'     => 'penitipanbarang_create_store', // penitipanbarang with methode POST
                'edit'      => 'penitipanbarang_update_edit', // penitipanbarang/{{id}}/edit with methode GET
                'update'    => 'penitipanbarang_update_update', // penitipanbarang/{{id}} with methode PUT
                'destroy'   => 'penitipanbarang_delete_destroy', // penitipanbarang/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/zona/list', 'Settings\MasterZonaController@list')->name('zona_view_list');
        Route::get('/zona/import', 'Settings\MasterZonaController@import')->name('zona_import_import');
        Route::get('/zona/export', 'Settings\MasterZonaController@export')->name('zona_export_export');
        Route::resource('zona', 'Settings\MasterZonaController', [
            'names' => [
                'index'     => 'zona_view_index',
                'show'      => 'zona_view_show',
                'create'    => 'zona_create_create', // zona/create with methode GET
                'store'     => 'zona_create_store', // zona with methode POST
                'edit'      => 'zona_update_edit', // zona/{{id}}/edit with methode GET
                'update'    => 'zona_update_update', // zona/{{id}} with methode PUT
                'destroy'   => 'zona_delete_destroy', // zona/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/negara/list', 'Settings\MasterNegaraController@list')->name('negara_view_list');
        Route::get('/negara/import', 'Settings\MasterNegaraController@import')->name('negara_import_import');
        Route::get('/negara/export', 'Settings\MasterNegaraController@export')->name('negara_export_export');
        Route::resource('negara', 'Settings\MasterNegaraController', [
            'names' => [
                'index'     => 'negara_view_index',
                'show'      => 'negara_view_show',
                'create'    => 'negara_create_create', // negara/create with methode GET
                'store'     => 'negara_create_store', // negara with methode POST
                'edit'      => 'negara_update_edit', // negara/{{id}}/edit with methode GET
                'update'    => 'negara_update_update', // negara/{{id}} with methode PUT
                'destroy'   => 'negara_delete_destroy', // negara/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/admin/list', 'Settings\AdminController@list')->name('admin_view_list');
        Route::get('/admin/import', 'Settings\AdminController@import')->name('admin_import_import');
        Route::get('/admin/export', 'Settings\AdminController@export')->name('admin_export_export');
        Route::resource('admin', 'Settings\AdminController', [
            'names' => [
                'index'     => 'admin_view_index',
                'show'      => 'admin_view_show',
                'create'    => 'admin_create_create', // admin/create with methode GET
                'store'     => 'admin_create_store', // admin with methode POST
                'edit'      => 'admin_update_edit', // admin/{{id}}/edit with methode GET
                'update'    => 'admin_update_update', // admin/{{id}} with methode PUT
                'destroy'   => 'admin_delete_destroy', // admin/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/pelanggan/list', 'Settings\PelangganController@list')->name('pelanggan_view_list');
        Route::get('/pelanggan/import', 'Settings\PelangganController@import')->name('pelanggan_import_import');
        Route::get('/pelanggan/export', 'Settings\PelangganController@export')->name('pelanggan_export_export');
        Route::resource('pelanggan', 'Settings\PelangganController', [
            'names' => [
                'index'     => 'pelanggan_view_index',
                'show'      => 'pelanggan_view_show',
                'create'    => 'pelanggan_create_create', // pelanggan/create with methode GET
                'store'     => 'pelanggan_create_store', // pelanggan with methode POST
                'edit'      => 'pelanggan_update_edit', // pelanggan/{{id}}/edit with methode GET
                'update'    => 'pelanggan_update_update', // pelanggan/{{id}} with methode PUT
                'destroy'   => 'pelanggan_delete_destroy', // pelanggan/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/marketing/list', 'Settings\MarketingController@list')->name('marketing_view_list');
        Route::get('/marketing/import', 'Settings\MarketingController@import')->name('marketing_import_import');
        Route::get('/marketing/export', 'Settings\MarketingController@export')->name('marketing_export_export');
        Route::resource('marketing', 'Settings\MarketingController', [
            'names' => [
                'index'     => 'marketing_view_index',
                'show'      => 'marketing_view_show',
                'create'    => 'marketing_create_create', // marketing/create with methode GET
                'store'     => 'marketing_create_store', // marketing with methode POST
                'edit'      => 'marketing_update_edit', // marketing/{{id}}/edit with methode GET
                'update'    => 'marketing_update_update', // marketing/{{id}} with methode PUT
                'destroy'   => 'marketing_delete_destroy', // marketing/{{id}} with methode DELETE
            ]
        ]);

        Route::get('rekening', 'Settings\RekeningController@index')->name('rekening_view_index');
        Route::put('rekening/ganti', 'Settings\RekeningController@ganti')->name('rekening_view_ganti');

        Route::get('textpersetujuan', 'Settings\TextPersetujuanController@index')->name('textpersetujuan_view_index');
        Route::put('textpersetujuan/ganti', 'Settings\TextPersetujuanController@ganti')->name('textpersetujuan_view_ganti');

        Route::get('/corporate', 'Settings\CorporateController@index')->name('corporate_view_index');
        Route::get('/corporate/edit', 'Settings\CorporateController@edit')->name('corporate_update_edit');
        Route::put('/corporate', 'Settings\CorporateController@update')->name('corporate_update_update');

    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
