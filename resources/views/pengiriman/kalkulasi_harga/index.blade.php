@extends('template.macb4')

@section('title', 'Kalkulasi Harga')

@section('css_vendor')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4//app-assets/vendors/css/forms/selects/select2.min.css')}}">
    {{-- <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/switch.css')}}">
    <!-- END: Page CSS--> --}}
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Kalkulasi Harga</a>
</li>
<li class="breadcrumb-item active">Index
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Kalkulasi Harga</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1">

                            <div class="row">
                                <div class="col-md">
                                    <label>Panjang</label>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('panjang') is-invalid @enderror" value="{{ old('panjang') }}" name="panjang" id="panjang" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">cm</span>
                                                </div>
                                                @error('panjang')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Lebar</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('lebar') is-invalid @enderror" value="{{ old('lebar') }}" name="lebar" id="lebar" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">cm</span>
                                                </div>
                                                @error('lebar')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Tinggi</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('tinggi') is-invalid @enderror" value="{{ old('tinggi') }}" name="tinggi" id="tinggi" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">cm</span>
                                                </div>
                                                @error('tinggi')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Berat</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('berat') is-invalid @enderror" value="{{ old('berat') }}" name="berat" required min=0 max=32000>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">gram</span>
                                                </div>
                                                @error('berat')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleSelect1">Negara</label>
                                            <select class="select2-data-array form-control" id="" name="zona">
                                                <option value="">-- Select Data -- </option>
                                                @foreach ($negara as $negara)
                                                    <option value="{{ $negara->id_zonas }}">{{ $negara->nama_negara }}</option>
                                                @endforeach
                                            </select>
                                            @error('negara')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <button type="button" class="btn btn-primary form-control" id="calculate"><font color="white">Hitung Sekarang</font></button>
                                    </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Berat Volume</label>
                                        <div class="input-group">
                                            <input type="text"  class="form-control money @error('volume') is-invalid @enderror" value="{{ old('volume') }}" name="volume" id="volume" readonly>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">gram</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Tarif</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text"  class="form-control money @error('tarif') is-invalid @enderror" value="{{ old('tarif') }}" name="tarif" id="tarif" readonly>    
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')

@endsection

@section('js_custom')
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
<script>
    $( document ).ready(function() {
        // $("#kt_form_1").validate();
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            // errorClass: "alert alert-danger",
            // validClass: "valid success-alert",
            rules: {
                panjang: "required",
                lebar: "required",
                tinggi: "required",
                berat: "required",
                zona: "required",
            },
            messages: {
                panjang: "Kolom tidak boleh kosong",
                lebar: "Kolom tidak boleh kosong",
                tinggi: "Kolom tidak boleh kosong",
                berat: "Kolom tidak boleh kosong",
                zona: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

    $('#calculate').click( function (e) {
        e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $.ajax({
                type:'POST',
                url:"{{ route('kalkulasiharga_view_calculate') }}",
                data:$('#kt_form_1').serialize(),
                success:function(data){
                    let panjang = $('#panjang').val().replace("," , "");
                    let tinggi = $('#tinggi').val().replace("," , "");
                    let lebar = $('#lebar').val().replace("," , "");
                    // console.log(panjang);

                    let volume = panjang * tinggi * lebar / 5;


                    $('#volume').val(volume.toLocaleString());
                    let harga = parseInt(data.harga).toLocaleString(); 
                    $('#tarif').val(harga);
                },
                error:function(data){
                    console.log("error");
                    console.log(data);
                },
            });
    });


</script>
@endsection