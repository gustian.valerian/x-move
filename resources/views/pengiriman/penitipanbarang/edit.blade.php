@extends('template.macb4')

@section('title', 'Penitipan Barang Edit')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Penitipan Barang</a>
    </li>
    <li class="breadcrumb-item active">Edit
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Penitipan Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('penitipanbarang_update_update', $model->id)}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Keranjang : </label>
                                        <label  name="no_keranjang"><b>DC.xxxxxx</b></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>
                                </div>
                                @if (Auth::user()->id_groups == 1 || Auth::user()->id_groups == 3)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleSelect1">Nama Customer</label>
                                            <select class="form-control kt-select2" id="kt_select2_1" name="customer">
                                                @foreach ($customer as $customer)
                                                    <option value="{{ $customer->id }}" 
                                                        {{ $customer->id == $model->created_by ? 'selected' : ''}}>
                                                        {{ $customer->fullname }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('negara')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                            @endif
                                @if (Auth::user()->id_groups == 1 || Auth::user()->id_groups == 3)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number', $model->tracking_number) }}" name="tracking_number">
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number', $model->tracking_number) }}" name="tracking_number" readonly>
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                                @if (Auth::user()->id_groups  != 3)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Rencana Belanja</label>
                                            {{-- @error('rencana_belanja') is-invalid @enderror --}}
                                            <textarea class="form-control @error('rencana_belanja') is-invalid @enderror" name="rencana_belanja" rows="5" required>{{ old('rencana_belanja', $model->rencana_belanja) }}</textarea>
                                            <script>
                                                CKEDITOR.replace('rencana_belanja');
                                            </script>
                                            {{-- <div class="error invalid-feedback">test</div> --}}
                                            @error('rencana_belanja')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-6">
                                        <div class="form-group card">
                                            <label>Rencana Belanja</label>
                                            <br>
                                            <label for="">{!! $model->rencana_belanja !!}</label>
                                        </div>
                                    </div>
                                @endif
                                @if (Auth::user()->id_groups  == 1 || Auth::user()->id_groups  == 3)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Belanjaan di Gudang</label>
                                            {{-- @error('belanjaan_digudang') is-invalid @enderror --}}
                                            <textarea class="form-control @error('belanjaan_digudang') is-invalid @enderror" name="belanjaan_digudang" rows="5" required>{{ old('belanjaan_digudang', $model->belanjaan_digudang) }}</textarea>
                                            <script>
                                                CKEDITOR.replace('belanjaan_digudang');
                                            </script>
                                            {{-- <div class="error invalid-feedback">test</div> --}}
                                            @error('belanjaan_digudang')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Belanjaan di Gudang</label>
                                            {{-- @error('belanjaan_digudang') is-invalid @enderror --}}
                                            <textarea class="form-control @error('belanjaan_digudang') is-invalid @enderror" name="belanjaan_digudang" rows="5" readonly >{{ old('belanjaan_digudang', $model->belanjaan_digudang) }}</textarea>
                                            <script>
                                                CKEDITOR.replace('belanjaan_digudang');
                                            </script>
                                            {{-- <div class="error invalid-feedback">test</div> --}}
                                            @error('belanjaan_digudang')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="text-right mt-3">
                                <input type="hidden" name="date_pickup_keranjang" value="{{ $model->date_pickup_keranjang }}">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                rencana_belanja: "required",
                belanjaan_digudang: "required",
                // tracking_number: "required",
            },
            messages: {
                rencana_belanja: "Kolom tidak boleh kosong",
                belanjaan_digudang: "Kolom tidak boleh kosong",
                // tracking_number: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    };
</script>
@endsection