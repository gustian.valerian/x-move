@extends('template.macb4')

@section('title', 'Penitipan Barang Index')

@section('css_vendor')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Penitipan Barang</a>
    </li>
    <li class="breadcrumb-item active">Index
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Penitipan Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        @if ($menu->allow_create)
                            <a href="{{ route('penitipanbarang_create_create') }}" class="btn btn-primary mb-1 ml-1">
                                <i class="la la-plus"></i>
                                New Record
                            </a>
                        @endif
                        @if (Auth::user()->id_groups == 1)
                            <a href="{{ route('penitipanbarang_view_export') }}" class="btn btn-success mb-1 ml-1">
                                <i class="la la-file"></i>
                                Export Excel
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dataex-res-configuration" id="penitipanbarangDT">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Nomor Keranjang</th>
                                        <th>Nama Customer</th>
                                        <th>Tanggal di Ambil Keranjang</th>
                                        {{-- <th>Rencana Belanja</th> --}}
                                        {{-- <th>Belanjaan di Gudang</th> --}}
                                        <th>Tracking Number</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
    @include('template.support.dt_js')
    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/jszip.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/pdfmake.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/vfs_fonts.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.print.min.js')}}"></script> --}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    {{-- <script src="{{ asset('assets_macb4/app-assets/js/scripts/tables/datatables/datatable-advanced.js')}}"></script> --}}
    <!-- END: Page JS-->
@endsection

@section('js_custom')
<script>
    var datatableList ;
    var menu = {!! json_encode($menu->toArray()) !!};
    var user = {!! json_encode(Auth::user()) !!};

    $(document).ready(function() {
        funcpenitipanbarangDT();
        // console.log(user.id_groups);
    } );


    function funcpenitipanbarangDT(){
        datatableList = $('#penitipanbarangDT').DataTable({
            "order": [[ 0, "desc" ]],
            responsive: true,
            processing: true,
            // "language": {
            //     "processing": "<div class='circle-loader'></div>" //add a loading image,simply putting <img src="loader.gif" /> tag.
            // },
            serverSide: true,
            ajax: "{{ route('penitipanbarang_view_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'no_keranjang', name: 'no_keranjang' },
                { data: 'created_by', name: 'created_by' },
                { data: 'date_pickup_keranjang', name: 'date_pickup_keranjang' },
                // { data: 'rencana_belanja', name: 'rencana_belanja' },
                // { data: 'belanjaan_digudang', name: 'belanjaan_digudang' },
                { data: 'tracking_number', name: 'tracking_number' },
                { data: 'id', //primary key dari tabel
                render: function(data, type, row)
                    {
                        console.log(row);
                    let buttonAction = '<div class="dropdown dropdown-inline">' +
                                        '<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                        '    <i class="ft-more-horizontal"></i>' +
                                        '</button>' +
                                        '<div class="dropdown-menu dropdown-menu-right">';
                    if (menu.allow_view == true) {
                        buttonAction +=   '   <a class="dropdown-item" href="'+ row.show +'"><i class="la la-eye"></i> Show</a>' ;
                    }
                    if(user.id_groups == 2 && row.tracking_number == "-" || user.id_groups == 1){
                        if (menu.allow_update == true) {
                            buttonAction +=   '    <a class="dropdown-item" href="'+ row.edit +'"><i class="la la-pencil"></i> Edit </a>';
                        }
                    
                        if (menu.allow_delete == true) {
                            buttonAction +=   '<button type="button" class="dropdown-item" onclick="buttonHapus(\''+data+'\');"><i class="la la-trash"></i>Delete</button>';
                        }
                    }else if(user.id_groups == 3){
                        if (menu.allow_update == true) {
                            buttonAction +=   '    <a class="dropdown-item" href="'+ row.edit +'"><i class="la la-pencil"></i> Edit </a>';
                        }
                    }

                
                    buttonAction +=    '</div>' +
                            '</div>';

                    return buttonAction;
                    } 
                }
            ],
            // "order": [[ 1, "desc" ]]
        });
    }

    function buttonHapus(idx) {
            swal.fire({
                    title: 'Apa Kamu Yakin ?',
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $("input[name='_token']").val()
                            }
                        });
                        // let href = "{{ route('penitipanbarang_delete_destroy', ['penitipanbarang' => "idx"]) }}";
                        let href = "{{ url('penitipanbarang') }}/"+ idx;
                        $.ajax({
                            type: "DELETE",
                            url: href,
                            data: {
                            },
                            success: function (data) {
                                swal.fire(
                                    'Hapus!',
                                    'File Anda telah dihapus.',
                                    'success'
                                ),
                                datatableList.ajax.reload();
                            }
                        });
                    }
                });
    }
</script>
    {{-- isi dengan js --}}
@endsection