@extends('template.macb4')

@section('title', 'Penitipan Barang Create')

@section('css_vendor')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4//app-assets/vendors/css/forms/selects/select2.min.css')}}">
    {{-- <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/switch.css')}}">
    <!-- END: Page CSS--> --}}
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Penitipan Barang</a>
</li>
<li class="breadcrumb-item active">Create
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Penitipan Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('penitipanbarang_create_store')}}" name="post_data">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Keranjang : </label>
                                        <label  name="no_keranjang"><b>BOX.xxxxxxxxxx</b></label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>
                                </div>
                                @if (Auth::user()->id_groups  == 1 || Auth::user()->id_groups  == 3)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleSelect1">Nama Customer</label>
                                            <select class="select2-data-array form-control" id="" name="customer">
                                                <option value="">-- Select Data -- </option>
                                                @foreach ($customer as $customer)
                                                    <option value="{{ $customer->id }}">{{ $customer->fullname }}</option>
                                                @endforeach
                                            </select>
                                            @error('customer')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @endif

                                @if (Auth::user()->id_groups == 1 || Auth::user()->id_groups  == 3)
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number') }}" name="tracking_number">
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Rencana Belanja</label>
                                        {{-- @error('rencana_belanja') is-invalid @enderror --}}
                                        <textarea class="form-control @error('rencana_belanja') is-invalid @enderror" name="rencana_belanja" rows="5" required>{{ old('rencana_belanja') }}</textarea>
                                        <script>
                                            CKEDITOR.replace('rencana_belanja');
                                        </script>
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('rencana_belanja')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                @if (Auth::user()->id_groups  == 1 || Auth::user()->id_groups  == 3)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Belanjaan di Gudang</label>
                                            {{-- @error('belanjaan_digudang') is-invalid @enderror --}}
                                            <textarea class="form-control @error('belanjaan_digudang') is-invalid @enderror" name="belanjaan_digudang" rows="5" required>{{ old('belanjaan_digudang') }}</textarea>
                                            <script>
                                                CKEDITOR.replace('belanjaan_digudang');
                                            </script>
                                            {{-- <div class="error invalid-feedback">test</div> --}}
                                            @error('belanjaan_digudang')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Submit</button>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Text Persetujuan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row px-2">
                                                <div class="col-md">
                                                    {!! $text->text_persetujuan_penitipan_barang !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" onclick="modalToggle();">Setuju dan Lanjutkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Modal -->

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')

@endsection

@section('js_custom')
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    {{-- <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
    <!-- END: Page JS--> --}}
<script>
    $( document ).ready(function() {
        // $("#kt_form_1").validate();
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            // errorClass: "alert alert-danger",
            // validClass: "valid success-alert",
            rules: {
                rencana_belanja: "required",
                belanjaan_digudang: "required",
                // tracking_number: "required",
            },
            messages: {
                rencana_belanja: "Kolom tidak boleh kosong",
                belanjaan_digudang: "Kolom tidak boleh kosong",
                // tracking_number: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

    function modalToggle()
    {
        $('#exampleModalLong').modal('toggle');
    }

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

   
</script>
@endsection