                                <?php
                                    use App\Models\User;
                                    use App\Models\Enum;
                                    $customer = User::where('id_groups', 2)->get();
                                    $enum = Enum::where('category', 'status')->get();
                                ?>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Nomor Pengiriman : </label>
                                        <label name="nomor_pengiriman"><b>{{ $model->nomor_pengiriman }}</b></label>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label for="exampleSelect1">Nama Customer</label>
                                            <select class="form-control kt-select2" id="kt_select2_1" name="customer">
                                                @foreach ($customer as $customer)
                                                    <option value="{{ $customer->id }}" 
                                                        {{ $customer->id == $model->created_by ? 'selected' : ''}}>
                                                        {{ $customer->fullname }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('negara')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        {{-- @error('nama_penerima') is-invalid @enderror --}}
                                        <input type="text"  class="form-control @error('nama_penerima') is-invalid @enderror" value="{{ old('nama_penerima', $model->nama_penerima) }}" name="nama_penerima">
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('nama_penerima')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input type="number"  class="form-control @error('harga_real') is-invalid @enderror" value="{{ old('harga_real', $model->harga_real) }}" name="harga_real">
                                        @error('harga_real')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number', $model->tracking_number) }}" name="tracking_number">
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label>Nominal Bayar</label>
                                        <input type="number"  class="form-control @error('payment') is-invalid @enderror" value="{{ old('payment', $model->payment) }}" name="payment">
                                        @error('payment')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Bukti Pembayaran</label>
                                        <input type="file" accept="image/*" class="form-control" value="{{ old('gambar') }}" name="gambar">
                                        @error('gambar')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        @if (empty($model->gambar))
                                            <img class="w-50 img-thumbnail" src="{{ url('img/noimg.png') }}" alt="No Image Available">
                                        @else
                                            <img class="w-50 img-thumbnail" src="{{ url('img/uploads/' . $model->gambar)  }}" alt="{{ $model->gambar }}">
                                        @endif
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control kt-select2" id="kt_select2_1" name="enum">
                                                @foreach ($enum as $enum)
                                                    <option value="{{ $enum->id }}" 
                                                        {{ $enum->id == $model->id_enum ? 'selected' : ''}}>
                                                        {{ $enum->value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('enum')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <input type="hidden" name="tanggal" value="{{ $model->tanggal }}">
                                <input type="hidden" name="zipcode_penerima" value="{{ $model->zipcode_penerima }}">
                                <input type="hidden" name="telp_penerima" value="{{ $model->telp_penerima }}">
                                <input type="hidden" name="negara" value="{{ $model->id_negara }}">
                                <input type="hidden" name="provinsi_penerima" value="{{ $model->provinsi_penerima }}">
                                <input type="hidden" name="kota_penerima" value="{{ $model->kota_penerima }}">
                                <input type="hidden" name="alamat_penerima" value="{{ $model->alamat_penerima }}">
                                <input type="hidden" name="nilai_paket" value="{{ $model->nilai_paket }}">
                                <input type="hidden" name="berat_paket" value="{{ $model->berat_paket }}">
                                <input type="hidden" name="deskripsi_paket" value="{{ $model->deskripsi_paket }}">
                                <input type="hidden" name="berat_paket_real" value="{{ $model->berat_paket_real }}">
                                <input type="hidden" name="potongan_harga" value="{{ $model->potongan_harga }}">
                                <input type="hidden" name="biaya_tambahan" value="{{ $model->biaya_tambahan }}">
                                <input type="hidden" name="harga_real" value="{{ $model->harga_real }}">
                                @if (Auth::user()->id_groups == 2)
                                    <input type="hidden" name="enum" value="{{ $model->id_enum }}">
                                @endif
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>