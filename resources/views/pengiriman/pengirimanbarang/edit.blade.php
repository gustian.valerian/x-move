@extends('template.macb4')

@section('title', 'Pengiriman Barang Edit')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Pengiriman Barang</a>
    </li>
    <li class="breadcrumb-item active">Edit
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pengiriman Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('pengirimanbarang_update_update', $model->id)}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Nomor Pengiriman : </label>
                                        <label name="nomor_pengiriman"><b>{{ $model->nomor_pengiriman }}</b></label>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label for="exampleSelect1">Nama Customer</label>
                                            <select class="form-control" id="" name="customer">
                                                @foreach ($customer as $customer)
                                                    <option value="{{ $customer->id }}" 
                                                        {{ $customer->id == $model->created_by ? 'selected' : ''}}>
                                                        {{ $customer->fullname }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('negara')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        {{-- @error('nama_penerima') is-invalid @enderror --}}
                                        <input type="text"  class="form-control @error('nama_penerima') is-invalid @enderror" value="{{ old('nama_penerima', $model->nama_penerima) }}" name="nama_penerima" required>
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('nama_penerima')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Zip Code Penerima</label>
                                        <input type="text"  class="form-control @error('zipcode_penerima') is-invalid @enderror" value="{{ old('zipcode_penerima', $model->zipcode_penerima) }}" name="zipcode_penerima">
                                        @error('zipcode_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Telp Penerima</label>
                                        <input type="number"  class="form-control @error('telp_penerima') is-invalid @enderror" value="{{ old('telp_penerima', $model->telp_penerima) }}" name="telp_penerima">
                                        @error('telp_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleSelect1">Negara</label>
                                        <select class="form-control" id="negara" name="negara">
                                            @foreach ($negara as $negara)
                                                <option value="{{ $negara->id }}" 
                                                    {{ $negara->id == $model->id_negara ? 'selected' : ''}}>
                                                    {{ $negara->nama_negara }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('negara')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Provinsi/Negara Bagian</label>
                                        <input type="text"  class="form-control @error('provinsi_penerima') is-invalid @enderror" value="{{ old('provinsi_penerima', $model->provinsi_penerima) }}" name="provinsi_penerima">
                                        @error('provinsi_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Kota Penerima</label>
                                        <input type="text"  class="form-control @error('kota_penerima') is-invalid @enderror" value="{{ old('kota_penerima', $model->kota_penerima) }}" name="kota_penerima">
                                        @error('kota_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat Penerima</label>
                                        <textarea type="text"  class="form-control @error('alamat_penerima') is-invalid @enderror" name="alamat_penerima" rows="6">{{ old('alamat_penerima', $model->alamat_penerima) }}</textarea>
                                        @error('alamat_penerima')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number', $model->tracking_number) }}" name="tracking_number">
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Nilai Paket</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">USD</span>
                                            </div>
                                            <input type="text"  class="form-control money @error('nilai_paket') is-invalid @enderror" value="{{ old('nilai_paket', $model->nilai_paket) }}" name="nilai_paket">
                                            @error('nilai_paket')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Berat Paket</label>
                                        <div class="input-group">
                                            <input type="text"  class="form-control money @error('berat_paket') is-invalid @enderror" value="{{ old('berat_paket', $model->berat_paket) }}" name="berat_paket" id="berat_paket">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">gram</span>
                                            </div>
                                            @error('berat_paket')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Deskripsi Paket</label>
                                        <textarea type="text"  class="form-control @error('deskripsi_paket') is-invalid @enderror" name="deskripsi_paket" rows="5">{{ old('deskripsi_paket', $model->deskripsi_paket) }}</textarea>
                                        @error('deskripsi_paket')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Ongkos Kirim</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control money @error('ongkir') is-invalid @enderror" value="{{ old('ongkir', $model->ongkir) }}" name="ongkir" id="ongkir" readonly>
                                            @error('ongkir')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group menungguPembayaran">
                                            <label>Berat Paket Real</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('berat_paket_real') is-invalid @enderror" value="{{ old('berat_paket_real', $model->berat_paket_real) }}" name="berat_paket_real" id="berat_paket_real">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">gram</span>
                                                </div>
                                                @error('berat_paket_real')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group menungguPembayaran">
                                            <label>Potongan Harga</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <input type="text"  class="form-control money @error('potongan_harga') is-invalid @enderror" value="{{ old('potongan_harga', $model->potongan_harga) }}" name="potongan_harga" id="potongan_harga">
                                                @error('potongan_harga')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group menungguPembayaran">
                                            <label>Biaya Tambahan</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <input type="text" class="form-control money @error('biaya_tambahan') is-invalid @enderror" value="{{ old('biaya_tambahan', $model->biaya_tambahan) }}" name="biaya_tambahan" id="biaya_tambahan">
                                                @error('biaya_tambahan')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Harga</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control money @error('harga_real') is-invalid @enderror" value="{{ old('harga_real', $model->harga_real) }}" name="harga_real" id="harga_real" readonly>
                                            @error('harga_real')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group payment">
                                            <label>Nominal Bayar</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <input type="text"  class="form-control money @error('payment') is-invalid @enderror" value="{{ old('payment', $model->payment) }}" name="payment">
                                                @error('payment')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group payment">
                                            <label>Bukti Pembayaran</label>
                                            <input type="file" accept="image/*" class="form-control" value="{{ old('gambar') }}" name="gambar">
                                            @error('gambar')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group payment">
                                            @if (empty($model->gambar))
                                                <img class="w-50 img-thumbnail" src="{{ url('img/noimg.png') }}" alt="No Image Available">
                                            @else
                                                <img class="w-50 img-thumbnail" src="{{ url('img/uploads/' . $model->gambar)  }}" alt="{{ $model->gambar }}">
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleSelect1">Status</label>
                                            <select class="form-control kt-select2" id="kt_select2_1" name="enum">
                                                @foreach ($enum as $enum)
                                                    <option value="{{ $enum->id }}"
                                                        {{ $enum->id == $model->id_enum ? 'selected' : ''}}>
                                                        {{ $enum->value }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            @error('enum')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                @if (Auth::user()->id_groups == 2)
                                    <input type="hidden" name="enum" value="6">
                                @endif
                                <input type="hidden" name="tanggal" value="{{ $model->tanggal }}">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        validate();
        if ($('#kt_select2_1').val() != 8 || $('#kt_select2_1').val() != 9){
            tungguBayar();
        }
        if ($('#kt_select2_1').val() != 9){
            payment();
        }
        beratpaketkeyup();
        beratpaketrealkeyup();
        negarachange();
        potongantambahan();
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                nama_penerima: "required",
                zipcode_penerima: "required",
                telp_penerima: "required",
                negara: "required",
                provinsi_penerima: "required",
                kota_penerima: "required",
                alamat_penerima: "required",
                // tracking_number: "required",
                nilai_paket: "required",
                berat_paket: "required",
                deskripsi_paket: "required",
                berat_paket_real: "required",
                potongan_harga: "required",
                biaya_tambahan: "required",
                harga_real: "required",
                payment: "required",
                id_enum: "required",
            },
            messages: {
                nama_penerima: "Kolom tidak boleh kosong",
                zipcode_penerima: "Kolom tidak boleh kosong",
                telp_penerima: "Kolom tidak boleh kosong",
                negara: "Kolom tidak boleh kosong",
                provinsi_penerima: "Kolom tidak boleh kosong",
                kota_penerima: "Kolom tidak boleh kosong",
                alamat_penerima: "Kolom tidak boleh kosong",
                // tracking_number: "Kolom tidak boleh kosong",
                nilai_paket: "Kolom tidak boleh kosong",
                berat_paket: "Kolom tidak boleh kosong",
                deskripsi_paket: "Kolom tidak boleh kosong",
                berat_paket_real: "Kolom tidak boleh kosong",
                potongan_harga: "Kolom tidak boleh kosong",
                biaya_tambahan: "Kolom tidak boleh kosong",
                harga_real: "Kolom tidak boleh kosong",
                payment: "Kolom tidak boleh kosong",
                id_enum: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    };

    function tungguBayar(){
        $('.menungguPembayaran').hide();
        $('#kt_select2_1').on('change', function() {
            if ($(this).val() == 8 || $(this).val() == 9){
                $('.menungguPembayaran').show();
            }
            else{
                $('.menungguPembayaran').hide();
            }
        });
    }

    function payment(){
        $('.payment').hide();
        $('#kt_select2_1').on('change', function() {
            if ($(this).val() == 9){
                $('.payment').show();
            }
            else{
                $('.payment').hide();
            }
        });
    }

    function beratpaketkeyup(){
        $("#berat_paket").keyup(function(){
            if($("#negara").val() != ""){
                // console.log('negara1231');
                ajax();
            }
        });
    }

    function beratpaketrealkeyup(){
        $("#berat_paket_real").keyup(function(){
            if($("#negara").val() != ""){
                ajax();
            }
        });
    }

    function negarachange(){
        $("#negara").change(function(){
            if($("#berat_paket").val() != null || $("#berat_paket").val() != ""){
                ajax();
            }
        });
    }

    function potongantambahan(){ 
        $("#potongan_harga").keyup(function(){
            ajax();
        });
        $("#biaya_tambahan").keyup(function(){
            ajax();
        });
    }

    function ajax(){
        // e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type:'POST',
            url:"{{ route('pengirimanbarang_view_calculate') }}",
            data:{
                berat_paket_real: $("#berat_paket_real").val(),
                berat_paket: $("#berat_paket").val(),
                negara: $("#negara").val(),
                potongan_harga: $("#potongan_harga").val(),
                biaya_tambahan: $("#biaya_tambahan").val(),
            },
            success:function(data){
                // console.log(data);
                $('#harga_real').val(data.harga.toLocaleString());
                $('#ongkir').val(data.ongkir.toLocaleString());
            },
            error:function(data){
                console.log("error");
                console.log(data);
            },
        });
    }
</script>
@endsection