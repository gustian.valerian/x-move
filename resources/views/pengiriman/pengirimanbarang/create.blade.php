@extends('template.macb4')

@section('title', 'Pengiriman Barang Create')

@section('css_vendor')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4//app-assets/vendors/css/forms/selects/select2.min.css')}}">
    {{-- <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/switch.css')}}">
    <!-- END: Page CSS--> --}}
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Pengiriman Barang</a>
</li>
<li class="breadcrumb-item active">Create
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pengiriman Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('pengirimanbarang_create_store')}}" name="post_data">
                            @csrf

                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Nomor Pengiriman : </label>
                                        <label  name="nomor_pengiriman"><b>XM.xxxxxxxxxx</b></label>
                                    </div>

                                    @if (Auth::user()->id_groups  == 1)
                                        <div class="form-group">
                                            <label for="exampleSelect1">Nama Customer</label>
                                            <select class="select2-data-array form-control" id="" name="customer">
                                                <option value="">-- Select Data -- </option>
                                                @foreach ($customer as $customer)
                                                    <option value="{{ $customer->id }}">{{ $customer->fullname }}</option>
                                                @endforeach
                                            </select>
                                            @error('customer')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif
                                    

                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        {{-- @error('nama_penerima') is-invalid @enderror --}}
                                        <input type="text"  class="form-control @error('nama_penerima') is-invalid @enderror" value="{{ old('nama_penerima') }}" name="nama_penerima" required>
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('nama_penerima')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Zip Code Penerima</label>
                                        <input type="text"  class="form-control @error('zipcode_penerima') is-invalid @enderror" value="{{ old('zipcode_penerima') }}" name="zipcode_penerima">
                                        @error('zipcode_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Telp Penerima</label>
                                        <input type="number"  class="form-control @error('telp_penerima') is-invalid @enderror" value="{{ old('telp_penerima') }}" name="telp_penerima">
                                        @error('telp_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleSelect1">Negara Penerima</label>
                                        <select class="select2-data-array form-control" name="negara" id="negara">
                                            <option value="">-- Select Data -- </option>
                                            @foreach ($negara as $negara)
                                                <option value="{{ $negara->id }}">{{ $negara->nama_negara }}</option>
                                            @endforeach
                                        </select>
                                        @error('negara')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Provinsi/Negara Bagian</label>
                                        <input type="text"  class="form-control @error('provinsi_penerima') is-invalid @enderror" value="{{ old('provinsi_penerima') }}" name="provinsi_penerima">
                                        @error('provinsi_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Kota Penerima</label>
                                        <input type="text"  class="form-control @error('kota_penerima') is-invalid @enderror" value="{{ old('kota_penerima') }}" name="kota_penerima">
                                        @error('kota_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat Penerima</label>
                                        <textarea type="text"  class="form-control @error('alamat_penerima') is-invalid @enderror" name="alamat_penerima" rows="5">{{ old('alamat_penerima') }}</textarea>
                                        @error('alamat_penerima')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <input type="text"  class="form-control @error('tracking_number') is-invalid @enderror" value="{{ old('tracking_number') }}" name="tracking_number">
                                            @error('tracking_number')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Nilai Paket</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">USD</span>
                                            </div>
                                            <input type="text"  class="form-control money @error('nilai_paket') is-invalid @enderror" value="{{ old('nilai_paket') }}" name="nilai_paket">
                                            @error('nilai_paket')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                   

                                    <div class="form-group">
                                        <label>Berat Paket</label>
                                        <div class="input-group">
                                            <input type="text"  class="form-control money @error('berat_paket') is-invalid @enderror" value="{{ old('berat_paket') }}" name="berat_paket" id="berat_paket">
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">gram</span>
                                            </div>
                                            @error('berat_paket')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Deskripsi Paket</label>
                                        <textarea type="text"  class="form-control @error('deskripsi_paket') is-invalid @enderror" name="deskripsi_paket" rows="5">{{ old('deskripsi_paket') }}</textarea>
                                        @error('deskripsi_paket')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Ongkos Kirim</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text"  class="form-control money @error('ongkir') is-invalid @enderror" value="{{ old('ongkir') }}" name="ongkir" id="ongkir" readonly>
                                            @error('ongkir')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    @if (Auth::user()->id_groups == 1)
                                        <div class="form-group">
                                            <label>Berat Paket Real</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control money @error('berat_paket_real') is-invalid @enderror" value="{{ old('berat_paket_real') }}" name="berat_paket_real" id="berat_paket_real">
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">gram</span>
                                                </div>
                                                @error('berat_paket_real')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Potongan Harga</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <input type="text"  class="form-control money @error('potongan_harga') is-invalid @enderror" value="{{ old('potongan_harga') }}" name="potongan_harga" id="potongan_harga">
                                                @error('potongan_harga')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Biaya Tambahan</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <input type="text" class="form-control money @error('biaya_tambahan') is-invalid @enderror" value="{{ old('biaya_tambahan') }}" name="biaya_tambahan" id="biaya_tambahan">
                                                @error('biaya_tambahan')
                                                        <div class="error invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Estimasi Harga</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control money @error('harga_real') is-invalid @enderror" value="{{ old('harga_real') }}" name="harga_real" id="harga_real" readonly>
                                            @error('harga_real')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">Submit</button>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Text Persetujuan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row px-2">
                                                <div class="col-md">
                                                    {!! $text->text_persetujuan_pengiriman_barang !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" onclick="modalToggle();">Setuju dan Lanjutkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Modal -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')

@endsection

@section('js_custom')
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    {{-- <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
    <!-- END: Page JS--> --}}
<script>

    var realkalkulasifromdb;
    $( document ).ready(function() {
        // $("#kt_form_1").validate();
        validate();
        beratpaketkeyup();
        beratpaketrealkeyup();
        negarachange();
        potongantambahan();
    });

    function validate(){
        $("form[name='post_data']").validate({
            // errorClass: "alert alert-danger",
            // validClass: "valid success-alert",
            rules: {
                nama_penerima: "required",
                zipcode_penerima: "required",
                telp_penerima: "required",
                negara: "required",
                provinsi_penerima: "required",
                kota_penerima: "required",
                alamat_penerima: "required",
                // tracking_number: "required",
                nilai_paket: "required",
                berat_paket: "required",
                deskripsi_paket: "required",
                berat_paket_real: "required",
                potongan_harga: "required",
                biaya_tambahan: "required",
                ongkir: "required",
            },
            messages: {
                nama_penerima: "Kolom tidak boleh kosong",
                zipcode_penerima: "Kolom tidak boleh kosong",
                telp_penerima: "Kolom tidak boleh kosong",
                negara: "Kolom tidak boleh kosong",
                provinsi_penerima: "Kolom tidak boleh kosong",
                kota_penerima: "Kolom tidak boleh kosong",
                alamat_penerima: "Kolom tidak boleh kosong",
                // tracking_number: "Kolom tidak boleh kosong",
                nilai_paket: "Kolom tidak boleh kosong",
                berat_paket: "Kolom tidak boleh kosong",
                deskripsi_paket: "Kolom tidak boleh kosong",
                berat_paket_real: "Kolom tidak boleh kosong",
                potongan_harga: "Kolom tidak boleh kosong",
                biaya_tambahan: "Kolom tidak boleh kosong",
                ongkir: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

    function beratpaketkeyup(){
        $("#berat_paket").keyup(function(){
            if($("#negara").val() != ""){
                // console.log('negara1231');
                ajax();
            }
        });
    }

    function beratpaketrealkeyup(){
        $("#berat_paket_real").keyup(function(){
            if($("#negara").val() != ""){
                ajax();
            }
        });
    }

    function negarachange(){
        $("#negara").change(function(){
            if($("#berat_paket").val() != null || $("#berat_paket").val() != ""){
                ajax();
            }
        });
    }

    function potongantambahan(){ 
        $("#potongan_harga").keyup(function(){
            ajax();
        });
        $("#biaya_tambahan").keyup(function(){
            ajax();
        });
    }

    function ajax(){
        // e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $.ajax({
                type:'POST',
                url:"{{ route('pengirimanbarang_view_calculate') }}",
                data:$('#kt_form_1').serialize(),
                success:function(data){
                    // console.log(data);
                    $('#harga_real').val(data.harga.toLocaleString());
                    $('#ongkir').val(data.ongkir.toLocaleString());
                },
                error:function(data){
                    console.log("error");
                    console.log(data);
                },
            });
    }

    function modalToggle()
    {
        $('#exampleModalLong').modal('toggle');
    }
    
</script>
@endsection