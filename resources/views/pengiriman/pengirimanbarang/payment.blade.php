@extends('template.macb4')

@section('title', 'Pengiriman Barang Payment')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Pengiriman Barang</a>
    </li>
    <li class="breadcrumb-item active">Payment
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Payment</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('pengirimanbarang_update_update', $model->id)}}" name="post_data" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Nomor Pengiriman : </label>
                                        <label name="nomor_pengiriman"><b>{{ $model->nomor_pengiriman }}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        <label class="form-control">{{$model->nama_penerima}}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Harga</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <label class="form-control money">{{$model->harga_real}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label>Nomor Rekening</label><br>
                                        @foreach ($rekening as $rekening)
                                            <label>{!! $rekening->rekening !!}</label>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        <label>Nominal Bayar</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <input type="text"  class="form-control money @error('payment') is-invalid @enderror" value="{{ old('payment', $model->payment) }}" name="payment">
                                            @error('payment')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Bukti Pembayaran</label>
                                        <input type="file" accept="image/*" class="form-control" value="{{ old('gambar') }}" name="gambar">
                                        @error('gambar')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        @if (empty($model->gambar))
                                            <img class="w-50 img-thumbnail" src="{{ url('img/noimg.png') }}" alt="No Image Available">
                                        @else
                                            <img class="w-50 img-thumbnail" src="{{ url('img/uploads/' . $model->gambar)  }}" alt="{{ $model->gambar }}">
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <input type="hidden" name="nama_penerima" value="{{ $model->nama_penerima }}">
                                <input type="hidden" name="harga_real" value="{{ $model->harga_real }}">
                                <input type="hidden" name="tanggal" value="{{ $model->tanggal }}">
                                <input type="hidden" name="zipcode_penerima" value="{{ $model->zipcode_penerima }}">
                                <input type="hidden" name="telp_penerima" value="{{ $model->telp_penerima }}">
                                <input type="hidden" name="negara" value="{{ $model->id_negara }}">
                                <input type="hidden" name="provinsi_penerima" value="{{ $model->provinsi_penerima }}">
                                <input type="hidden" name="kota_penerima" value="{{ $model->kota_penerima }}">
                                <input type="hidden" name="alamat_penerima" value="{{ $model->alamat_penerima }}">
                                <input type="hidden" name="nilai_paket" value="{{ $model->nilai_paket }}">
                                <input type="hidden" name="berat_paket" value="{{ $model->berat_paket }}">
                                <input type="hidden" name="deskripsi_paket" value="{{ $model->deskripsi_paket }}">
                                <input type="hidden" name="berat_paket_real" value="{{ $model->berat_paket_real }}">
                                <input type="hidden" name="potongan_harga" value="{{ $model->potongan_harga }}">
                                <input type="hidden" name="biaya_tambahan" value="{{ $model->biaya_tambahan }}">
                                <input type="hidden" name="harga_real" value="{{ $model->harga_real }}">
                                <input type="hidden" name="ongkir" value="{{ $model->ongkir }}">
                                <input type="hidden" name="enum" value="9">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                customer: "required",
                nama_penerima: "required",
                harga_real: "required",
                tracking_number: "required",
                payment: "required",
                gambar: "required",
                id_enum: "required",
            },
            messages: {
                customer: "Kolom tidak boleh kosong",
                nama_penerima: "Kolom tidak boleh kosong",
                harga_real: "Kolom tidak boleh kosong",
                tracking_number: "Kolom tidak boleh kosong",
                payment: "Kolom tidak boleh kosong",
                gambar: "Kolom tidak boleh kosong",
                id_enum: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    };
</script>
@endsection