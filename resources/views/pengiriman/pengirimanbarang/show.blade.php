@extends('template.macb4')

@section('title', 'Pengiriman Barang Show')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Pengiriman Barang</a>
    </li>
    <li class="breadcrumb-item active">Show
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pengiriman Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('pengirimanbarang_update_update', $model->id)}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Nomor Pengiriman : </label>
                                        <label><b>{{ $model->nomor_pengiriman }}</b></label>
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Penerima</label>
                                        <label class="form-control">{{ $model->nama_penerima }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Zip Code Penerima</label>
                                        <label class="form-control">{{ $model->zipcode_penerima }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Telp Penerima</label>
                                        <label class="form-control">{{ $model->telp_penerima }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Negara Penerima</label>
                                        <label class="form-control">{{ $model->negara->nama_negara }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Provinsi/Negara Bagian</label>
                                        <label class="form-control">{{ $model->provinsi_penerima }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Kota Penerima</label>
                                        <label class="form-control">{{ $model->kota_penerima }}</label>
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat Penerima</label>
                                        <label class="form-control">{{ $model->alamat_penerima }}</label>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Date Today : </label>
                                        <label><b>{{ date('Y-m-d') }}</b></label>
                                    </div>

                                    @if (Auth::user()->id == 1)
                                        <div class="form-group">
                                            <label>Tracking Number</label>
                                            <label class="form-control">{{ $model->tracking_number }}</label>
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label>Nilai Paket</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">USD</span>
                                            </div>
                                            <label class="form-control money">{{ $model->nilai_paket }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Berat Paket</label>
                                        <div class="input-group">
                                            <label class="form-control money">{{ $model->berat_paket }}</label>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">gram</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Deskripsi Paket</label>
                                        <label class="form-control">{{ $model->deskripsi_paket }}</label>
                                    </div>

                                    @if (Auth::user()->id == 1)
                                        <div class="form-group">
                                            <label>Berat Paket Real</label>
                                                <div class="input-group">
                                                <label class="form-control money">{{ $model->berat_paket_real }}</label>
                                                <div class="input-group-append">
                                                    <span class="input-group-text" id="basic-addon2">gram</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Potongan Harga</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <label class="form-control money">{{ $model->potongan_harga }}</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Biaya Tambahan</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <label class="form-control money">{{ $model->biaya_tambahan }}</label>
                                            </div>
                                        </div>
                                    @else
                                        
                                    @endif

                                    
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon2">Rp.</span>
                                                </div>
                                                <label class="form-control money">{{ $model->harga_real }}</label>
                                            </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Nominal Bayar</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon2">Rp.</span>
                                            </div>
                                            <label class="form-control money">{{ $model->payment }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Bukti Pembayaran</label><br>
                                        @if (empty($model->gambar))
                                            <img class="w-50 img-thumbnail" src="{{ url('img/noimg.png') }}" alt="No Image Available">
                                        @else
                                            <img class="w-50 img-thumbnail" src="{{ url('img/uploads/' . $model->gambar)  }}" alt="{{ $model->gambar }}">
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <label class="form-control">{{ $model->enum->value }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')

@endsection

@section('js_custom')
<script>

</script>
@endsection