@extends('template.macb4')

@section('title', 'Pengiriman Barang Index')

@section('css_vendor')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Pengiriman Barang</a>
    </li>
    <li class="breadcrumb-item active">Index
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pengiriman Barang</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        @if ($menu->allow_create)
                            <a href="{{ route('pengirimanbarang_create_create') }}" class="btn btn-primary mb-1 ml-1">
                                <i class="la la-plus"></i>
                                Tambah Pengiriman
                            </a>
                        @endif
                        @if (Auth::user()->id_groups == 1)
                            <a href="{{ route('pengirimanbarang_view_export') }}" class="btn btn-success mb-1 ml-1">
                                <i class="la la-file"></i>
                                Export Excel
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dataex-res-configuration" id="pengirimanbarangDT">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Nomor Pengiriman</th>
                                        <th>Tanggal</th>
                                        <th>ID Customer</th>
                                        <th>Kode Member</th>
                                        <th>Nama Penerima</th>
                                        <th>Berat Paket Real</th>
                                        <th>Ongkir</th>
                                        <th>Biaya Tambahan</th>
                                        <th>Potongan Harga</th>
                                        <th>Harga</th>
                                        <th>Tracking Number</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
    @include('template.support.dt_js')
    <!-- BEGIN: Page Vendor JS-->
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/jszip.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/pdfmake.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/vfs_fonts.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.print.min.js')}}"></script> --}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    {{-- <script src="{{ asset('assets_macb4/app-assets/js/scripts/tables/datatables/datatable-advanced.js')}}"></script> --}}
    <!-- END: Page JS-->
@endsection

@section('js_custom')
<script>
    var datatableList ;
    var menu = {!! json_encode($menu->toArray()) !!};
    var user = {!! json_encode(Auth::user()) !!};

    $(document).ready(function() {
        funcpengirimanbarangDT();
        // console.log(user.id_groups);
    } );


    function funcpengirimanbarangDT(){
        datatableList = $('#pengirimanbarangDT').DataTable({
            "order": [[ 0, "desc" ]],
            responsive: true,
            processing: true,
            // "language": {
            //     "processing": "<div class='circle-loader'></div>" //add a loading image,simply putting <img src="loader.gif" /> tag.
            // },
            serverSide: true,
            ajax: "{{ route('pengirimanbarang_view_list')}}",
            columnDefs: [
                { className: 'text-right', targets: [7, 8, 9, 10] },
                // { className: 'text-center', targets: [5] },
            ],
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'nomor_pengiriman', name: 'nomor_pengiriman' },
                { data: 'tanggal', name: 'tanggal' },
                { data: 'id_customer', name: 'id_customer' },
                { data: 'kode_member', name: 'kode_member' },
                { data: 'nama_penerima', name: 'nama_penerima' },
                { data: 'berat_paket_real', render: $.fn.dataTable.render.number( ',') },
                { data: 'ongkir', render: $.fn.dataTable.render.number( ',') },
                // { data: 'ongkir', name: 'ongkir' },
                { data: 'biaya_tambahan', render: $.fn.dataTable.render.number( ',') },
                { data: 'potongan_harga', render: $.fn.dataTable.render.number( ',') },
                { data: 'harga_real', render: $.fn.dataTable.render.number( ',') },
                { data: 'tracking_number', name: 'tracking_number' },
                { data: 'id_enum', name: 'id_enum' },
                { data: 'created_at', type: 'num',
                    render: {
                        _: 'display',
                        sort: 'timestamp'
                    }
                },
                { data: 'id', //primary key dari tabel
                render: function(data, type, row)
                    {
                        // console.log(row);
                    let buttonAction = '<div class="dropdown dropdown-inline">' +
                                        '<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                        '    <i class="ft-more-horizontal"></i>' +
                                        '</button>' +
                                        '<div class="dropdown-menu dropdown-menu-right">';
                    if (menu.allow_view == true) {
                        buttonAction +=   '   <a class="dropdown-item" href="'+ row.show +'"><i class="la la-eye"></i> Show</a>' ;
                    }
                    if (menu.allow_update == true && row.id_enum == 'Menunggu Pembayaran Pelanggan' && user.id_groups == 2) {
                        buttonAction +=   '    <a class="dropdown-item" href="'+ row.payment +'"><i class="la la-money"></i> Payment </a>';
                    }
                    if(user.id_groups == 2 && row.edited_by == null || user.id_groups == 1){
                        if (menu.allow_update == true) {
                            buttonAction +=   '    <a class="dropdown-item" href="'+ row.edit +'"><i class="la la-pencil"></i> Edit </a>';
                        }

                        if (menu.allow_delete == true) {
                            buttonAction +=   '<button type="button" class="dropdown-item" onclick="buttonHapus(\''+data+'\');"><i class="la la-trash"></i>Delete</button>';
                        }
                    }
                
                    buttonAction +=    '</div>' +
                            '</div>';

                    return buttonAction;
                    } 
                }
            ],
            // "order": [[ 1, "desc" ]]
        });
    }

    function buttonHapus(idx) {
            swal.fire({
                    title: 'Apa Kamu Yakin ?',
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $("input[name='_token']").val()
                            }
                        });
                        // let href = "{{ route('pengirimanbarang_delete_destroy', ['pengirimanbarang' => "idx"]) }}";
                        let href = "{{ url('pengirimanbarang') }}/"+ idx;
                        $.ajax({
                            type: "DELETE",
                            url: href,
                            data: {
                            },
                            success: function (data) {
                                swal.fire(
                                    'Hapus!',
                                    'File Anda telah dihapus.',
                                    'success'
                                ),
                                datatableList.ajax.reload();
                            }
                        });
                    }
                });
    }
</script>
    {{-- isi dengan js --}}
@endsection