<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
	<title>{{ config('app.name', 'Laravel') }} | Reset Password</title>
	<link rel="apple-touch-icon" href="{{ asset('assets_macb4/app-assets/images/ico/apple-icon-120.png')}}">
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets_macb4/app-assets/images/ico/favicon.ico')}}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('company/img/xmove.jpeg')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/custom.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/components.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/pages/login-register.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    <style>
        body{
            background-image: url("{!! url('/storage/berkas/') . '/' .  \App\Helper\ComponentBuilder::corporate('Background Login') !!}");
            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-overlay-menu 1-column blank-page" data-open="click" data-menu="vertical-overlay-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="{!! url('/storage/berkas/') . '/' .  \App\Helper\ComponentBuilder::corporate('Logo PT') !!}" alt="branding logo" height="150">
                                        <br>
                                        <h4 class="mt-1">Reset Password</h4>
                                    </div>
                                    {{-- <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Easily Using</span></h6> --}}
                                </div>
                                <div class="card-content">
                                   
                                    <div class="card-body">
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}" name="post_data">
											@csrf
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Your Email" required>
                                                <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div>
                                            </fieldset>
                                            
                                            <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-unlock"></i> Reset Password</button>
                                            <a href="{{ route('login') }}" class="btn btn-outline-info btn-block"><i class="ft-user-plus"></i> Back To Login</a>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/form-login-register.js')}}"></script>
    <!-- END: Page JS-->

    <script>
        $( document ).ready(function() {
            validate();
        });
        function validate(){
            $("form[name='post_data']").validate({
                rules: {
                    email : {
                        required: true,
                    },
                   
                },
                messages: {
                    email: "Kolom tidak boleh kosong",
                },
                submitHandler: function(form) {
                form.submit();
                }
            });
        }
    </script>

</body>
<!-- END: Body-->

</html>