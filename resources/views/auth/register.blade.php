<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
	<title>{{ config('app.name', 'Laravel') }} | Register</title>
	<link rel="apple-touch-icon" href="{{ asset('assets_macb4/app-assets/images/ico/apple-icon-120.png')}}">
    {{-- <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets_macb4/app-assets/images/ico/favicon.ico')}}"> --}}
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('company/img/xmove.jpeg')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/custom.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/components.css')}}">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/pages/login-register.css')}}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/assets/css/style.css')}}">
    <!-- END: Custom CSS-->
    <style>
        body{
            background-image: url("{!! url('/storage/berkas/') . '/' .  \App\Helper\ComponentBuilder::corporate('Background Login') !!}");
            /* Full height */
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-overlay-menu 1-column blank-page" data-open="click" data-menu="vertical-overlay-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-8 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="{!! url('/storage/berkas/') . '/' .  \App\Helper\ComponentBuilder::corporate('Logo PT') !!}" alt="branding logo" height="150">
                                        <h4 class="mt-1">Register</h4>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal" method="POST" action="{{ route('register') }}" name='post_data' enctype="multipart/form-data">
											@csrf

											<div class="row">
												<div class="col-md">
													<fieldset class="form-group position-relative has-icon-left">
														<input type="text" class="form-control @error('fullname') is-invalid @enderror" id="fullname" name="fullname" placeholder="Fullname">
                                                        @error('fullname')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-user"></i>
														</div>
													</fieldset>
													
													<fieldset class="form-group position-relative has-icon-left">
														<input type="number" class="form-control @error('telpnumber') is-invalid @enderror" id="telpnumber" name="telpnumber" placeholder="Whatsapp Number">
                                                        @error('telpnumber')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-phone"></i>
														</div>
													</fieldset>

													<fieldset class="form-group position-relative has-icon-left">
														<textarea class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" placeholder="Address" rows="8"></textarea>
                                                        @error('alamat')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-map-pin"></i>
														</div>
													</fieldset>

                                                    <fieldset class="form-group position-relative has-icon-left">
														<input type="text" class="form-control @error('kode_member') is-invalid @enderror" id="kode_member" name="kode_member" placeholder="Member Code (Optional)">
                                                        @error('kode_member')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-qrcode"></i>
														</div>
													</fieldset>
												</div>
												<div class="col-md">
													<fieldset class="form-group position-relative has-icon-left">
														<input type="text" class="form-control @error('npwp') is-invalid @enderror" id="npwp" name="npwp" placeholder="Tax Number">
                                                        @error('npwp')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-credit-card"></i>
														</div>
													</fieldset>

													<fieldset class="form-group position-relative has-icon-left">
														<input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" placeholder="Your Username">
                                                        @error('username')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-user"></i>
														</div>
													</fieldset>

													<fieldset class="form-group position-relative has-icon-left">
														<input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Your Password">
                                                        @error('password')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-key"></i>
														</div>
													</fieldset>

													<fieldset class="form-group position-relative has-icon-left">
														<input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" placeholder="Repeat Password">
                                                        @error('password_confirmation')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-key"></i>
														</div>
													</fieldset>

													<fieldset class="form-group position-relative has-icon-left">
														<input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Your Email">
                                                        @error('email')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-envelope"></i>
														</div>
													</fieldset>

                                                    <fieldset class="form-group position-relative has-icon-left">
														<input type="file" class="form-control @error('ktp') is-invalid @enderror" accept="image/*" name="ktp">
                                                        @error('ktp')
                                                            <div class="error invalid-feedback">{{ $message }}</div>
                                                        @enderror
														<div class="form-control-position">
															<i class="la la-file-image-o"></i>
														</div>
													</fieldset>
												</div>
											</div>
                                            
                                            <button type="submit" class="btn btn-outline-info btn-block"><i class="ft-user"></i> Register</button>
                                            <button type="button" class="btn btn-outline-info btn-block" onclick="history.back();"><i class="ft-arrow-left"></i> Back to Login</button>
                                        </form>
                                    </div>
                                    {{-- <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>New to Modern
                                            ?</span></p>
                                    <div class="card-body">
                                        <a href="register-with-bg-image.html" class="btn btn-outline-danger btn-block"><i class="la la-user"></i>
                                            Register</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/core/app.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/form-login-register.js')}}"></script>
    <!-- END: Page JS-->
    <script>
        $(document).ready(function() {
            // $("#kt_form_1").validate();
            validate();
            $("#ktp").val('Your ID Card')
        });

        function validate() {
            $("form[name='post_data']").validate({
                // errorClass: "alert alert-danger",
                // validClass: "valid success-alert",
                rules: {
                    fullname: "required",
                    telpnumber: "required",
                    alamat: "required",
                    // npwp: "required",
                    username: {
                        required : true,
                        unique : true
                    },
                    password : {
                        required: true,
                        minlength : 8,
                    },
                    password_confirmation : {
                        required: true,
                        minlength : 8,
                        equalTo : "[name='password']"
                    },
                    email: {
                        required : true,
                        unique : true
                    },
                },
                messages: {
                    fullname: "Kolom tidak boleh kosong",
                    telpnumber: "Kolom tidak boleh kosong",
                    alamat: "Kolom tidak boleh kosong",
                    // npwp: "Kolom tidak boleh kosong",
                    username: {
                        required : "Kolom tidak boleh kosong",
                        unique : "Username sudah terdaftar",
                    },
                    password:{
                        required: "Kolom tidak boleh kosong",
                        minlength : "Minimal 8 karakter",
                    },
                    password_confirmation:{
                        required: "Kolom tidak boleh kosong",
                        minlength : "Minimal 8 karakter",
                        equalTo: "Password Tidak Sesuai",
                    },
                    email: {
                        required : "Kolom tidak boleh kosong",
                        unique : "Email sudah terdaftar",
                    },
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        }
    </script>
</body>
<!-- END: Body-->

</html>