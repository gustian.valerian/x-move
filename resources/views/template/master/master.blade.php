@extends('template.macb4')

@section('title', 'Master Menu')

@section('css_vendor')
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}"> --}}
@endsection

@section('css_custom')
		
  
@endsection

@section('breadcumbs')
<li class="breadcrumb-item active"><a href="{{ route('master_view_index') }}">Master Menu</a>
</li>
{{-- <li class="breadcrumb-item"><a href="#">Form</a>
</li>
<li class="breadcrumb-item active">Basic Elements
</li> --}}
@endsection

@section('body')
 <!-- Basic Inputs start -->
 <section class="basic-inputs">
    <div class="row match-height">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Master Menu</h4>
                </div>

                <div class="card-body">
                    <fieldset class="form-group">
                        <input type="text" class="form-control"  id="search" class="form-control" placeholder="Search">
                    </fieldset>

                    <div class="form-group row" id="scopeSearch">
                        {!! $masterMenu !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Basic Inputs end -->

@endsection

@section('js_vendor')

@endsection

@section('js_custom')
<script>
$(document).ready(function() {
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#scopeSearch div").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
} );

</script>
    {{-- isi dengan js --}}
@endsection