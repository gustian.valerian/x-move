{{-- <li class=" nav-item"><a href="index.html"><i class="la la-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span><span class="badge badge badge-info badge-pill float-right mr-2">3</span></a>
    <ul class="menu-content">
        <li><a class="menu-item" href="dashboard-ecommerce.html"><i></i><span data-i18n="eCommerce">eCommerce</span></a>
        </li>
        <li class="active"><a class="menu-item" href="dashboard-crypto.html"><i></i><span data-i18n="Crypto">Crypto</span></a>
        </li>
        <li><a class="menu-item" href="dashboard-sales.html"><i></i><span data-i18n="Sales">Sales</span></a>
        </li>
    </ul>
</li>
<li class=" nav-item"><a href="#"><i class="la la-calendar"></i><span class="menu-title" data-i18n="Calendars">Calendars</span></a>
    <ul class="menu-content">
        <li><a class="menu-item" href="#"><i></i><span data-i18n="Full Calendar">Full Calendar</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="full-calender-basic.html"><i></i><span data-i18n="Basic">Basic</span></a>
                </li>
                <li><a class="menu-item" href="full-calender-events.html"><i></i><span data-i18n="Events">Events</span></a>
                </li>
                <li><a class="menu-item" href="full-calender-advance.html"><i></i><span data-i18n="Advance">Advance</span></a>
                </li>
                <li><a class="menu-item" href="full-calender-extra.html"><i></i><span data-i18n="Extra">Extra</span></a>
                </li>
            </ul>
        </li>
        <li><a class="menu-item" href="calendars-clndr.html"><i></i><span data-i18n="CLNDR">CLNDR</span></a>
        </li>
    </ul>
</li> --}}
<li class=" nav-item {{ Route::currentRouteName() == 'dashboard' ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="la la-dashboard"></i><span class="menu-title" data-i18n="Master">Dashboard</span></a>


{!! \App\Helper\ComponentBuilder::sideMenuBuilder() !!}

{{-- <li class=" nav-item {{ Route::currentRouteName() == 'master_view_index' ? 'active' : '' }}"><a href="{{ route('master_view_index') }}"><i class="la la-archive"></i><span class="menu-title" data-i18n="Master">Master</span></a> --}}

{{-- <li class="kt-menu__item {{ Route::currentRouteName() == 'master_view_index' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('master_view_index') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-menu-1"></i><span class="kt-menu__link-text">Master</span></a></li> --}}