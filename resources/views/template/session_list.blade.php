<div class="col-md-12">
    @if(\Session::has('alert-failed'))
        <div class="alert alert-danger mb-2" role="alert">
            <div>{{Session::get('alert-failed')}}</div>
        </div>
    @endif
    @if(\Session::has('alert-success'))
        <div class="alert alert-success mb-2" role="alert">
            <div>{{Session::get('alert-success')}}</div>
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger mb-2" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        {{-- <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div> --}}
    @endif
</div>