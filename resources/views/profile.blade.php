@extends('template.macb4')

@section('title', 'Profile Edit')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Profile</a>
    </li>
    <li class="breadcrumb-item active">Edit
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Profile</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('dashboard_view_change')}}" name="post_data" enctype="multipart/form-data">
                            @csrf

                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text"  class="form-control @error('username') is-invalid @enderror" value="{{ old('username', Auth::User()->username) }}" name="username">
                                        @error('username')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Fullname</label>
                                        <input type="text"  class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname', Auth::User()->fullname) }}" name="fullname">
                                        @error('fullname')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email', Auth::User()->email) }}" name="email">
                                        @error('email')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Ganti Password</label>
                                        <input type="password"  class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password">
                                        @error('password')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Re-type Password</label>
                                        <input type="password"  class="form-control @error('password_confirm') is-invalid @enderror" value="{{ old('password_confirm') }}" name="password_confirm">
                                        @error('password_confirm')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Telp Number</label>
                                        <input type="text"  class="form-control @error('telpnumber') is-invalid @enderror" value="{{ old('telpnumber', Auth::user()->telpnumber) }}" name="telpnumber">
                                        @error('telpnumber')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md">

                                    {{-- {{dd(Auth::user()->pelanggan) }} --}}
                                    @if (Auth::User()->id_groups == 2)
                                        <div class="form-group">
                                            <label>Member Code</label>
                                            <input type="text"  class="form-control @error('kodemember') is-invalid @enderror" value="{{ old('kodemember', Auth::user()->pelanggan->kodemember ?? `-`) }}" name="kodemember">
                                            @error('kodemember')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input type="text"  class="form-control @error('alamat') is-invalid @enderror" value="{{ old('alamat', Auth::user()->pelanggan->alamat ?? `-`) }}" name="alamat">
                                            @error('alamat')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Tax Number</label>
                                            <input type="text"  class="form-control @error('npwp') is-invalid @enderror" value="{{ old('npwp', Auth::user()->pelanggan->npwp ?? `-`) }}" name="npwp">
                                            @error('npwp')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>ID Card</label>
                                            <input type="file" accept="image/*" class="form-control @error('ktp') is-invalid @enderror" name="ktp">
                                            @error('ktp')
                                                    <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            @if (empty(Auth::User()->pelanggan->ktp))
                                                <img class="w-50 img-thumbnail" src="{{ url('img/noimg.png') }}" alt="No Image Available">
                                            @else
                                                <img class="w-50 img-thumbnail" src="{{ url('img/uploads/' . Auth::User()->pelanggan->ktp)  }}" alt="{{ Auth::User()->pelanggan->ktp }}">
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        validate();
        console.log('Ya');
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                username: {
                    required : true,
                    unique : true
                },
                fullname: "required",
                email: {
                    required : true,
                    unique : true
                },
                password : {
                    minlength : 8,
                    // required: true,
                },
                password_confirm : {
                    // required: true,
                    minlength : 8,
                    equalTo : "[name='password']"
                },
                telpnumber: "required",
                alamat: "required",
                npwp: "required",
            },
            messages: {
                username: {
                    required : "Kolom tidak boleh kosong",
                    unique : "Username sudah terdaftar"
                },
                fullname: "Kolom tidak boleh kosong",
                email: {
                    required : "Kolom tidak boleh kosong",
                    unique : "Email sudah terdaftar"
                },
                password:{
                    // required: "Kolom tidak boleh kosong",
                    minlength : "Minimal 8 karakter"
                },
                password_confirm:{
                    // required: "Kolom tidak boleh kosong",
                    minlength : "Minimal 8 karakter",
                    equalTo: "Password Tidak Sesuai"
                },
                telpnumber: "Kolom tidak boleh kosong",
                alamat: "Kolom tidak boleh kosong",
                npwp: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    };
</script>
@endsection