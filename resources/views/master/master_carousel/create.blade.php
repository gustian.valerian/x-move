@extends('template.macb4')

@section('title', 'Carousel Create')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Carousel</a>
    </li>
    <li class="breadcrumb-item active">Create
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Carousel</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('carousel_create_store')}}" 
                        name="post_data" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label>Title</label>
                                <input type="text"  class="form-control @error('title') is-invalid @enderror" value="{{ old('title') }}" name="title">
                                @error('title')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputName">Description</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="exampleTextarea" rows="3">{{ old('description') }}</textarea>
                                @error('description')
                                    <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group ">
                                <img src="#" alt="" id="src_image" 
                                style="width: 100%;">
                            </div>

                            <div class="form-group validated is-invalid">
                                <label>Carousel Image</label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" name="path_image" class="custom-file-input" id="path_image">
                                    <label class="custom-file-label text-left" for="customFile">Choose file</label>
                                    @error('path_image')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->


@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}

@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#src_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#path_image").change(function(){
        readURL(this);
    });

    // $(function() {
    //     $("form[name='post_data']").validate({
    //         rules: {
    //             jenis_arsip: "required",
                
    //         },
    //         messages: {
    //             jenis_arsip: "Kolom tidak boleh kosong",
    //         },
    //         submitHandler: function(form) {
    //         form.submit();
    //         }
    //     });
    // });
</script>
@endsection