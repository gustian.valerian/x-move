@extends('template.macb4')

@section('title', 'Group Menu Create')

@section('css_vendor')
<link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/custom.css') }}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Group Menu</a>
</li>
<li class="breadcrumb-item active">Create
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Group Menu</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('groupmenu_create_store')}}" name="post_data">
                            @csrf

                            <div class="form-group">
                                <label>Group Name</label>
                                <input type="text"  class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" name="name"name>
                                @error('name')
                                    <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="form-group">
                                {{-- <h4 class="card-title">Square Skin iCheck</h4> --}}
                                {{-- <div class="col-md-6 col-sm-2"> --}}
                                    <table class="table table-hover col-12">
                                        <thead>
                                            <tr class="bg-info text-white text-center">
                                                <th class="border-3 border-white">Description</th>
                                                <th class="border-3 border-white">View</th>
                                                <th class="border-3 border-white">Create</th>
                                                <th class="border-3 border-white">Update</th>
                                                <th class="border-3 border-white">Delete</th>
                                                <th class="border-3 border-white">Import</th>
                                                <th class="border-3 border-white">Export</th>
                                                <th class="border-3 border-white">All</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center skin skin-square">
                                            @foreach ($listmenu as $item)
                                                @if ($item->sort == '#')
                                                    <tr class="bg-warning">
                                                        <td class="text-white" colspan="8"><b>{{ $item->menuname }}</b></td>
                                                        {{-- <td><input type="checkbox" id="input-10"></td> --}}
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td class="text-left">{{ $item->menuname }}</td>
                                                        <td><input class="checkbox_line master_view" type="checkbox" name="privilege[{{ $item->id }}][view]" id="input-10"></td>
                                                        <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $item->id }}][create]" id="input-10"></td>
                                                        <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $item->id }}][update]" id="input-10"></td>
                                                        <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $item->id }}][delete]" id="input-10"></td>
                                                        <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $item->id }}][import]" id="input-10"></td>
                                                        <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $item->id }}][export]" id="input-10"></td>
                                                        <td><input type="checkbox" class="check_all all"></td>
                                                    </tr>
                                                @endif
                                               
                                            @endforeach


                                            <tr class="bg-warning">
                                                <td class="text-white" colspan="8"><b>Master</b></td>
                                                {{-- <td><input type="checkbox" id="input-10"></td> --}}
                                            </tr>
                                            @foreach ($listmenumaster as $itemmaster)
                                                <tr>
                                                    <td class="text-left">{{ $itemmaster->menuname }}</td>
                                                    <td><input class="checkbox_line master_view" type="checkbox" name="privilege[{{ $itemmaster->id }}][view]" id="input-10"></td>
                                                    <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $itemmaster->id }}][create]" id="input-10"></td>
                                                    <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $itemmaster->id }}][update]" id="input-10"></td>
                                                    <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $itemmaster->id }}][delete]" id="input-10"></td>
                                                    <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $itemmaster->id }}][import]" id="input-10"></td>
                                                    <td><input class="checkbox_line master" type="checkbox" name="privilege[{{ $itemmaster->id }}][export]" id="input-10"></td>
                                                    <td><input type="checkbox" class="check_all all"></td>
                                                </tr>
                                            @endforeach
                                            
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                {{-- </div> --}}
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

@endsection

@section('js_custom')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/checkbox-radio.js') }}"></script>
    <!-- END: Page JS-->

<script>
    $( document ).ready(function() {
        
        validate();
        checkbox();
    });

    function checkbox(){
        $('.check_all').on('ifChanged', function(event){
           checkbox = event.target.checked;
           if(checkbox == true){
               $(this).parents('tr').find('.checkbox_line').iCheck('check');
           }else{
               count = 0;
               $(this).parents('tr').find('.checkbox_line').each(function(){
                    if(!this.checked){
                        count++;
                    }
                })
                // console.log(count);
                if(count == 0){
                    $(this).parents('tr').find('.checkbox_line').iCheck('uncheck');
                }
           }
       });

       $('.checkbox_line').on('ifChanged', function(event){
           checkbox = event.target.checked;
           if(checkbox == true){
               $(this).parents('tr').find('.master_view').iCheck('check');
               count = 0;
               $(this).parents('tr').find('.checkbox_line').each(function(){
                    if(!this.checked){
                        count++;
                    }
                })
                if(count == 0){
                    $(this).parents('tr').find('.check_all').iCheck('check');
                }
           }
           else{
                $(this).parents('tr').find('.check_all').iCheck('uncheck');
           }
       });

       $('.master_view').on('ifChanged', function(event){
           checkbox = event.target.checked;
           if(checkbox == false){
            //    var length = $(this).parents('tr').find('.master').val().length;
            //    console.log(length);
            
               $(this).parents('tr').find('.master').iCheck('uncheck');
               $(this).parents('tr').find('.check_all').iCheck('uncheck');
           }
       });
    }

    function validate(){
        $("form[name='post_data']").validate({
            
            rules: {
                name: "required",
            },
            messages: {
                name: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

    
</script>
@endsection