@extends('template.macb4')

@section('title', 'Group Menu Show')

@section('css_vendor')
<link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/custom.css') }}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Group Menu</a>
</li>
<li class="breadcrumb-item active">Show
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Group Menu</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1"  name="post_data">
                            <div class="form-group">
                                <label>Group Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $model->name) }}" name="name" readonly>
                                @error('name')
                                    <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="form-group">
                                {{-- <h4 class="card-title">Square Skin iCheck</h4> --}}
                                {{-- <div class="col-md-6 col-sm-2"> --}}
                                    <table class="table table-hover col-12">
                                        <thead>
                                            <tr class="bg-info text-white text-center">
                                                <th class="border-3 border-white">Description</th>
                                                <th class="border-3 border-white">View</th>
                                                <th class="border-3 border-white">Create</th>
                                                <th class="border-3 border-white">Update</th>
                                                <th class="border-3 border-white">Delete</th>
                                                <th class="border-3 border-white">Import</th>
                                                <th class="border-3 border-white">Export</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center skin skin-square">
                                            @foreach ($listmenu as $item)
                                                @if ($item->sort == '#')
                                                    <tr class="bg-warning">
                                                        <td class="text-white" colspan="6"><b>{{ $item->menuname }}</b></td>
                                                        <td><input type="checkbox" id="input-10"></td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <?php
                                                            $data = $model->mapgroupmenu->where('id_menus', $item->id)->where('id_groups', $model->id)->first()
                                                        ?>
                                                        <td class="text-left">{{ $item->menuname }}</td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][view]" id="input-10" 
                                                            {{ $data->allow_view ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][create]" id="input-10"
                                                            {{ $data->allow_create ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][update]" id="input-10"
                                                            {{ $data->allow_update ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][delete]" id="input-10"
                                                            {{ $data->allow_delete ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][import]" id="input-10"
                                                            {{ $data->allow_import ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" name="privilege[{{ $item->id }}][export]" id="input-10"
                                                            {{ $data->allow_export ?? false == 1 ? 'checked' : '' }}  disabled>
                                                        </td>
                                                    </tr>
                                                @endif
                                               
                                            @endforeach


                                            <tr class="bg-warning">
                                                <td class="text-white" colspan="6"><b>Master</b></td>
                                                <td><input type="checkbox" id="input-10"></td>
                                            </tr>
                                            @foreach ($listmenumaster as $itemmaster)
                                                <tr>
                                                    <?php
                                                        $data = $model->mapgroupmenu->where('id_menus', $itemmaster->id)->where('id_groups', $model->id)->first()
                                                    ?>
                                                    <td class="text-left">{{ $itemmaster->menuname }}</td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][view]" id="input-10" 
                                                        {{ $data->allow_view ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][create]" id="input-10"
                                                        {{ $data->allow_create ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][update]" id="input-10"
                                                        {{ $data->allow_update ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][delete]" id="input-10"
                                                        {{ $data->allow_delete ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][import]" id="input-10"
                                                        {{ $data->allow_import ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" name="privilege[{{ $itemmaster->id }}][export]" id="input-10"
                                                        {{ $data->allow_export ?? false == 1 ? 'checked' : '' }}  disabled>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            
                                            
                                        </tbody>
                                    </table>
                                    
                                    
                                {{-- </div> --}}
                            </div>

                            <div class="text-right mt-3">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')
    <script src="{{ asset('js/jquery.validate.min.js') }}')}}"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

@endsection

@section('js_custom')
    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/checkbox-radio.js') }}"></script>
    <!-- END: Page JS-->

<script>
    $( document ).ready(function() {
       
    });

</script>
@endsection