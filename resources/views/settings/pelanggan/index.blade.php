@extends('template.macb4')

@section('title', 'Pelanggan Index')

@section('css_vendor')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="#">Settings</a>
    </li>
    <li class="breadcrumb-item"><a href="{{ route('pelanggan_view_index')}}">Pelanggan</a>
    </li>
    <li class="breadcrumb-item active">Index
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pelanggan</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        @if ($menu->allow_create)
                            <a href="{{ route('pelanggan_create_create') }}" class="btn btn-primary mb-1 ml-1">
                                <i class="la la-plus"></i>
                                New Record
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dataex-res-configuration" id="userDT">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Full Name</th>
                                        <th>Email</th>
                                        <th>Telp Number</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
    @include('template.support.dt_js')
@endsection

@section('js_custom')
<script>
    var datatableList ;
    $(document).ready(function() {
        funcuserDT();
    } );

    var menu = {!! json_encode($menu->toArray()) !!};

    function funcuserDT(){
        datatableList = $('#userDT').DataTable({
            responsive: true,
            processing: true,
            // "language": {
            //     "processing": "<div class='circle-loader'></div>" //add a loading image,simply putting <img src="loader.gif" /> tag.
            // },
            serverSide: true,
            ajax: "{{ route('pelanggan_view_list')}}",
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'fullname', name: 'fullname' },
                { data: 'email', name: 'email' },
                { data: 'telpnumber', name: 'telpnumber' },
                { data: 'id', //primary key dari tabel
                render: function(data, type, row)
                    {

                    let buttonAction = '<div class="dropdown dropdown-inline">' +
                                        '<button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                                        '    <i class="ft-more-horizontal"></i>' +
                                        '</button>' +
                                        '<div class="dropdown-menu dropdown-menu-right">';
                    if (menu.allow_view == true) {
                        buttonAction +=   '   <a class="dropdown-item" href="'+ row.show +'"><i class="la la-eye"></i> Show</a>' ;
                    }
                    if (menu.allow_update == true) {
                        buttonAction +=   '    <a class="dropdown-item" href="'+ row.edit +'"><i class="la la-pencil"></i> Edit </a>';
                    }
                    if (menu.allow_delete == true) {
                        buttonAction +=   '<button type="button" class="dropdown-item" onclick="buttonHapus(\''+data+'\');"><i class="la la-trash"></i>Delete</button>';
                    }

                
                    buttonAction +=    '</div>' +
                            '</div>';

                    return buttonAction;
                    } 
                }
            ],
            // "order": [[ 1, "desc" ]]
        });
    }

    function buttonHapus(idx) {
            swal.fire({
                    title: 'Apa Kamu Yakin ?',
                    text: "Anda tidak akan dapat mengembalikan ini!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    reverseButtons: true
                }).then(function (result) {
                    if (result.value) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $("input[name='_token']").val()
                            }
                        });
                        // let href = "{{ route('user_delete_destroy', ['user' => "idx"]) }}";
                        let href = "{{ url('pelanggan') }}/"+ idx;
                        $.ajax({
                            type: "DELETE",
                            url: href,
                            data: {
                            },
                            success: function (data) {
                                swal.fire(
                                    'Hapus!',
                                    'File Anda telah dihapus.',
                                    'success'
                                ),
                                datatableList.ajax.reload();
                            }
                        });
                    }
                });
    }
</script>
    {{-- isi dengan js --}}
@endsection