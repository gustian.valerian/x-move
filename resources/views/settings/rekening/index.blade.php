@extends('template.macb4')

@section('title', 'Rekening')

@section('css_vendor')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4//app-assets/vendors/css/forms/selects/select2.min.css')}}">
    {{-- <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/icheck/icheck.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/forms/toggle/switchery.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/validation/form-validation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/css/plugins/forms/switch.css')}}">
    <!-- END: Page CSS--> --}}
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Rekening</a>
</li>
<li class="breadcrumb-item active">Index
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekening</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('rekening_view_ganti')}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nomor Rekening</label>
                                        {{-- @error('rekening') is-invalid @enderror --}}
                                        <textarea class="form-control @error('rekening') is-invalid @enderror" name="rekening" rows="5" required>{{ old('rekening', $rekening->rekening) }}</textarea>
                                        <script>
                                            CKEDITOR.replace('rekening');
                                        </script>
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('rekening')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="text-right mt-3">
                                <input type="hidden" name="id" value="{{ $rekening->id }}">
                                <input type="hidden" name="key" value="{{ $rekening->key }}">
                                <input type="hidden" name="value" value="{{ $rekening->value }}">
                                <input type="hidden" name="category" value="{{ $rekening->category }}">
                                <button type="submit" id="save" class="btn btn-primary" >Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')

@endsection

@section('js_custom')
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
    {{-- <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/icheck/icheck.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script>
    <!-- END: Page JS--> --}}
<script>
    $( document ).ready(function() {
        // $("#kt_form_1").validate();
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            // errorClass: "alert alert-danger",
            // validClass: "valid success-alert",
            rules: {
                rekening: "required",
            },
            messages: {
                rekening: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();

   
</script>
@endsection