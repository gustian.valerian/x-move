@extends('template.macb4')

@section('title', 'Corporate Index')

@section('css_vendor')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="#">Settings</a>
    </li>
    <li class="breadcrumb-item"><a href="{{ route('corporate_view_index')}}">Corporate</a>
    </li>
    <li class="breadcrumb-item active">Index
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Corporate</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        @if ($menu->allow_update)
                            <a href="{{ route('corporate_update_edit') }}" class="btn btn-primary mb-1">
                                <i class="la la-pencil"></i>
                                Edit All Record
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered dataex-res-configuration" id="userDT">
                                <thead>
                                    <tr>
                                        <th style="width: 10px;">No</th>
                                        <th>Setting Name</th>
                                        <th>Setting Value</th>
                                        {{-- <th>Actions</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                {{-- {{$data}} --}}
                                {{-- {{ url('/') }} --}}
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{ $item->key }}</td>
                                        <td>
                                            @if ($item->type == "image")
                                                <img src="{{ url('/storage/berkas/') . '/' . $item->value }}" alt="" width="150" height="150">
                                            @else
                                                {{ $item->value }}
                                            @endif
                                        </td>
                                        {{-- <td>
                                            <div class="dropdown dropdown-inline">
                                                <button type="button" class="btn btn-hover-brand btn-elevate-hover btn-icon btn-sm btn-icon-md btn-circle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ft-more-horizontal"></i>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    @if ($menu->allow_update)
                                                        <a class="dropdown-item" href="+ row.edit +"><i class="la la-pencil"></i> Edit </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td> --}}
                                    </tr>
                                @endforeach
                               </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
    {{-- @include('template.support.dt_js') --}}
@endsection

@section('js_custom')
<script>
    

    
</script>
    {{-- isi dengan js --}}
@endsection