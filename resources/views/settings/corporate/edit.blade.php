@extends('template.macb4')

@section('title', 'Corporate Index')

@section('css_vendor')
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/vendors.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4/app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="#">Settings</a>
    </li>
    <li class="breadcrumb-item"><a href="{{ route('corporate_view_index')}}">Corporate</a>
    </li>
    <li class="breadcrumb-item active">Edit
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Corporate</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('corporate_update_update')}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered dataex-res-configuration" id="userDT">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">No</th>
                                            <th>Setting Name</th>
                                            <th>Setting Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data as $key => $item)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{ $item->key }}</td>
                                            <td>
                                                @if ($item->type == "image")
                                                    <img src="{{  url('/storage/berkas/') . '/' . $item->value }}" class="previewImg" alt="" width="250" height="250" id="previewImg{{$item->id}}">
                                                    <input type="file" class="form-control mt-1 upldImg" name="{{ 'image['.$item->id.']'}}"  data-id="{{$item->id}}">
                                                @else
                                                    <input type="text" class="form-control" value="{{ $item->value }}" name="{{ 'text['.$item->id.']' }}">
                                                @endif
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                   </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-primary mb-1" style="float: right;">Save</button>
                        </div>
                    </div>

                   
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
    {{-- @include('template.support.dt_js') --}}
@endsection

@section('js_custom')
<script>
    $(document).ready(() => {
        $(".upldImg").change(function () {
            const file = this.files[0];

            var id = $(this).data("id");

            // console.log();
            if (file) {
                let reader = new FileReader();
                reader.onload = function (event) {
                    $("#previewImg"+id)
                        .attr("src", event.target.result);
                };
                reader.readAsDataURL(file);
            }
        });
    });
    
</script>
    {{-- isi dengan js --}}
@endsection