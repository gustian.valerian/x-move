@extends('template.macb4')

@section('title', 'Admin Create')

@section('css_vendor')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_macb4//app-assets/vendors/css/forms/selects/select2.min.css')}}">
@endsection

@section('css_custom')

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="#">Settings</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('admin_view_index')}}">Admin</a>
</li>
<li class="breadcrumb-item active">Create
</li>
@endsection

@section('body')


<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Admin</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('admin_create_store')}}" name="post_data">
                            @csrf

                            <div class="form-group">
                                <label>Username</label>
                                <input type="text"  class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" name="username" required>
                                
                                @error('username')
                                    <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Fullname</label>
                                <input type="text"  class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname') }}" name="fullname">
                                @error('fullname')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email">
                                @error('email')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password"  class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password">
                                @error('password')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Re-type Password</label>
                                <input type="password"  class="form-control @error('password_confirm') is-invalid @enderror" value="{{ old('password_confirm') }}" name="password_confirm">
                                @error('password_confirm')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>Telp Number</label>
                                <input type="text"  class="form-control @error('telpnumber') is-invalid @enderror" value="{{ old('telpnumber') }}" name="telpnumber">
                                @error('telpnumber')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->



@endsection

@section('js_vendor')

@endsection

@section('js_custom')
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
<script>
    $( document ).ready(function() {
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                username: "required",
                fullname: "required",
                email: "required",
                password : {
                    minlength : 6,
                    required: true,
                },
                password_confirm : {
                    required: true,
                    minlength : 6,
                    equalTo : "[name='password']"
                },
                telpnumber: "required",
            },
            messages: {
                username: "Kolom tidak boleh kosong",
                fullname: "Kolom tidak boleh kosong",
                email: "Kolom tidak boleh kosong",
                password:{
                    required: "Kolom tidak boleh kosong",
                    minlength : "Minimal 6 karakter"
                },
                password_confirm:{
                    required: "Kolom tidak boleh kosong",
                    minlength : "Minimal 6 karakter",
                    equalTo: "Password Tidak Sesuai"
                },
                telpnumber: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    }

   
</script>
@endsection