@extends('template.macb4')

@section('title', 'Zona Edit')

@section('css_vendor')

@endsection

@section('css_custom')
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Zona</a>
</li>
<li class="breadcrumb-item active">Edit
</li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
        @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Zona</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" action="{{ route('zona_update_update', $model->id)}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label>Nama Zona</label>
                                <input type="text" class="form-control @error('nama_zona') is-invalid @enderror" value="{{ old('nama_zona', $model->nama_zona) }}" name="nama_zona">
                                @error('nama_zona')
                                <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                @foreach ($model->hargaZona as $key => $model)
                                <div class="row mt-1 dinamis" id="row{{ $key }}">
                                    <div class="col-md pr-0">
                                        <label>Berat</label>
                                        {{-- @error('berat') is-invalid @enderror --}}
                                        <input type="text" class="form-control @error('berat') is-invalid @enderror" value="{{ old('berat', $model->berat) }}" name="berat[]">
                                        {{-- <div class="error invalid-feedback">test</div> --}}
                                        @error('berat')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md pr-0">
                                        <label>Harga</label>
                                        <input type="text" class="form-control @error('harga') is-invalid @enderror" value="{{ old('harga', $model->harga) }}" name="harga[]">
                                        @error('harga')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-md-1">
                                        <label>&nbsp;</label>
                                        @if ($key == 0)
                                            <button type="button" name="add" id="add" class="form-control btn btn-success">
                                            <font color="white">+</font>
                                        @else
                                            <button type="button" name="remove" id="{{ $key }}" class="form-control btn btn-danger btn_remove">
                                            <font color="white">-</font>
                                        @endif
                                        </button>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $(document).ready(function() {
        validate();

        dinamicallyadd();
    });

    function validate() {
        $("form[name='post_data']").validate({
            rules: {
                nama_zona: "required"
            },
            messages: {
                nama_zona: "Kolom tidak boleh kosong"
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    };

    function dinamicallyadd() {
        var i = 1000;
        $('#add').click(function() {
            i++;
            $('.dinamis').parent().append(
                '<div class="row mt-1" id="row' + i + '">' +
                '<div class="col-md pr-0">' +
                '<label>Berat</label>' +
                '<input type="text"  class="form-control @error(' +
                berat ') is-invalid @enderror" value="{{ old(' +
                berat ') }}" name="berat[]">' +
                '@error(' +
                berat ')' +
                '<div class="error invalid-feedback">{{ $message }}</div>' +
                '@enderror' +
                '</div>' +
                '<div class="col-md pr-0">' +
                '<label>Harga</label>' +
                '<input type="text"  class="form-control @error(' +
                harga ') is-invalid @enderror" value="{{ old(' +
                harga ') }}" name="harga[]">' +
                '@error(' +
                harga ')' +
                '<div class="error invalid-feedback">{{ $message }}</div>' +
                '@enderror' +
                '</div>' +
                '<div class="col-md-1">' +
                '<label>&nbsp;</label>' +
                '<button type="button" name="remove" id="' + i + '" class="form-control btn btn-danger btn_remove"><font color="white">-</font></button>' +
                '</div>' +
                '</div>'
            );
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
    }
</script>
@endsection