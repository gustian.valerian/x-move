@extends('template.macb4')

@section('title', 'Zona Show')

@section('css_vendor')

@endsection

@section('css_custom')
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
</li>
<li class="breadcrumb-item"><a href="#">Zona</a>
</li>
<li class="breadcrumb-item active">Show
</li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
        @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Zona</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label>Nama Zona</label>
                                <label class="form-control">{{ $model->nama_zona }}</label>
                            </div>

                            <div class="form-group">
                                @foreach ($model->hargaZona as $model)
                                <div class="row mt-1">
                                    <div class="col-md pr-0">
                                        <label>Berat</label>
                                        <label class="form-control">{{ $model->berat }}</label>
                                    </div>
                                    <div class="col-md">
                                        <label>Harga</label>
                                        <label class="form-control">{{ $model->harga }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')

@endsection

@section('js_custom')
<script>

</script>
@endsection