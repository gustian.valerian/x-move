@extends('template.macb4')

@section('title', 'Negara Edit')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('master_view_index') }}">Master</a>
    </li>
    <li class="breadcrumb-item"><a href="#">Negara</a>
    </li>
    <li class="breadcrumb-item active">Edit
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Negara</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ route('negara_update_update', $model->id)}}" name="post_data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label>Nama Negara</label>
                                <input type="text"  class="form-control @error('nama_negara') is-invalid @enderror" value="{{ old('nama_negara', $model->nama_negara) }}" name="nama_negara">
                                @error('nama_negara')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleSelect1">Zona</label>
                                <select class="form-control kt-select2" id="kt_select2_1" name="zona">
                                    @foreach ($zona as $zona)
                                        <option value="{{ $zona->id }}" 
                                            {{ $zona->id == $model->id_zonas ? 'selected' : ''}}>
                                            {{ $zona->nama_zona }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('zona')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                   

                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')
{{-- <script src="{{ asset('assets_metronic/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script> --}}
<script src="{{ asset('js/jquery.validate.min.js')}}"></script>
@endsection

@section('js_custom')
<script>
    $( document ).ready(function() {
        validate();
    });

    function validate(){
        $("form[name='post_data']").validate({
            rules: {
                nama_negara: "required",
                zona: "required",
            },
            messages: {
                nama_negara: "Kolom tidak boleh kosong",
                zona: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    };
</script>
@endsection