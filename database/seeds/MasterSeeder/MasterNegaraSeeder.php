<?php

use Illuminate\Database\Seeder;
use App\Models\MasterNegara;

class MasterNegaraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = ['Singapore'];
        $b = [
            'Brunai',
            'Cambodia',
            'East Timor',
            'Hongkong',
            'Laos',
            'Macau',
            'Malaysia',
            'Philippines',
            'Thailand',
            'Vietnam'
        ];
        $c = [
            'Canada',
            'Mexico',
            'United States'
        ];
        $d = [
            'Banglades',
            'India',
            'Maldives',
            'Nepal',
            'Sri Langka'
        ];
        $e = [
            'China',
            'Japan',
            'Korea',
            'Mongolia',
            'Myanmar',
            'Papua New Guinea',
            'Taiwan'
        ];
        $f = [
            'Bahrain',
            'Austria',
            'Belgium',
            'Bulgaria',
            'Croatia',
            'Czech Rep., The',
            'Denmark',
            'Estonia',
            'Finland',
            'France',
            'Germany',
            'Hungary',
            'Iceland',
            'Ireland, Rep. Of',
            'Italy',
            'Jordan',
            'Kuwait',
            'Liechtenstein',
            'Lithuania',
            'Luxembourg',
            'Monaco',
            'Netherlands',
            'Norway',
            'Oman',
            'Poland',
            'Portugal',
            'Qatar',
            'Romania',
            'Russian Federation',
            'San Marino',
            'Saudi Arabia',
            'Serbia, Rep. Of',
            'Spain',
            'Sweden',
            'Switzerland',
            'Ukraine',
            'Vatican City'
        ];
        $g = [
            'Albania',
            'Algeria',
            'American Samoa',
            'Andorra',
            'Angola',
            'Angulilla',
            'Antigua',
            'Argentina',
            'Armenia',
            'Aruba',
            'Azerbaijan',
            'Bahamas',
            'Barbados',
            'Belarus',
            'Belize',
            'Benin',
            'Bermuda',
            'Bhutan',
            'Bolivia',
            'Bonaire',
            'Bosnia herzegovina',
            'Botswana',
            'Brazil',
            'Burkina Faso',
            'Burundi',
            'Cameroon',
            'Canary Islands, The',
            'Cape Verde',
            'Cayman Islands',
            'Central African Rep',
            'Chad',
            'Chile',
            'Colombia',
            'Comoros',
            'Congo',
            'Congo, DPR',
            'Cook Islands',
            'Costa Rica',
            'Cote D Ivoire',
            'Cuba',
            'Curacao',
            'Cyprus',
            'Djibouti',
            'Dominica',
            'Dominican Rep',
            'Ecuardo',
            'Egypt',
            'El Salvador',
            'Eritrea',
            'Ethiopia',
            'Falkland Islands',
            'Faroe Islands',
            'Fiji',
            'French Guyana',
            'Gabon',
            'Gambia',
            'Georgia',
            'Ghana',
            'Gibraltar',
            'Greece',
            'Greenland',
            'Grenada',
            'Guadeloupe',
            'Guam',
            'Guatemala',
            'Guernsey',
            'Guinea Rep.',
            'Guinea Bissau',
            'Guiena Equatorial',
            'Guyana le',
            'Haiti',
            'Honduras',
            'Iran',
            'Israel',
            'Jamaica',
            'Jersey',
            'Kazakhstan',
            'Kenya',
            'Kiribati',
            'Kosovo',
            'Kyrgyzstan',
            'Latvia',
            'Lebanon',
            'Lesotho',
            'Liberia',
            'Macedonia, Rep. Of',
            'Madagastar',
            'Malawi',
            'Mali',
            'Malta',
            'Mariana Islands',
            'Marshall Islands',
            'Martinique',
            'Mauritania',
            'Mauritius',
            'Mayotte',
            'Micronesia',
            'Moldova, Rep. Of',
            'Montenegro, Rep. Of',
            'Monttserrat',
            'Morocco',
            'Mozambique',
            'Namibia',
            'Nauru, Rep. Of',
            'Nevis',
            'New Caledonia',
            'Nicaragua',
            'Niger',
            'Nigeria',
            'Niue',
            'Pakistan',
            'Palau',
            'Panama',
            'Paraguay',
            'Peru',
            'Puerto Rico',
            'Reunion, Island Of',
            'Rwanda',
            'Saint Helena',
            'Samoa',
            'Sao Tome And Pricipe',
            'Senegal',
            'Seychelles',
            'Sierra Leone',
            'Slovakia',
            'Solomos Islands',
            'Somalia',
            'Somaliland, Rep Of',
            'South Africa',
            'South Sudan',
            'St. Barthelemy',
            'St. Eustatius',
            'St. Kitts',
            'St. Lucia',
            'St. Maarten',
            'St. Vincent',
            'Suriname',
            'Swaziland',
            'Tahiti',
            'Tajikistan',
            'Tanzania',
            'Togo',
            'Tonga',
            'Trinidad And Tobago',
            'Tunisia',
            'Turkey',
            'Turks Caicos',
            'Tuvalu',
            'Uganda',
            'Uni Arab Emirates',
            'United Kingdom',
            'Uruguay',
            'Uzbekistan',
            'Vanuatu',
            'Venezuela',
            'Virgin Islans-British',
            'Virgin Islans-US',
            'Yemen, Rep. Of',
            'Zambia',
            'Zimbabwe'
        ];
        $h = [
            'Australia',
            'New Zealand'
        ];

        foreach ($a as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 1;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($b as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 2;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($c as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 3;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($d as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 4;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($e as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 5;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($f as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 6;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($g as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 7;
            $data->nama_negara = $value;
            $data->save();
        }

        foreach ($h as $key => $value) {
            $data = new MasterNegara;
            $data->id_zonas = 8;
            $data->nama_negara = $value;
            $data->save();
        }
    }
}
