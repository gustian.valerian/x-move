<?php

use Illuminate\Database\Seeder;
use App\Models\MasterKota;

class MasterKotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // insert data ke table kota
        $Kota             = new MasterKota;
        $Kota->nama_kota       =  "Jakarta";
        $Kota->save();

        $Kota             = new MasterKota;
        $Kota->nama_kota       =  "Bandung";
        $Kota->save();
    }
}
