<?php

use Illuminate\Database\Seeder;
use App\Models\MasterGroup;

class MasterGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        
        $group             = new MasterGroup;
        $group->name       =  "Admin";
        $group->save();

        $group             = new MasterGroup;
        $group->name       =  "Peserta";
        $group->save();
        
    }
}
