<?php

use Illuminate\Database\Seeder;
use App\Models\MasterZona;


class MasterZonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $char = range('A', 'H');
        foreach ($char as $abjad) {
            $zona = new MasterZona;
            $zona->nama_zona = 'Zona ' . $abjad;
            $zona->save();
        }
    }
}
