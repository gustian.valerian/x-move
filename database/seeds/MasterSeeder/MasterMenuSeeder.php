<?php

use Illuminate\Database\Seeder;
use App\Models\MasterMenu;

class MasterMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $category = ["A", "Z", "Z"]; // Z adalah category menu Master
        $sort = ["1", "1", "2"];
        $icon = ["flaticon-home", "flaticon2-group", "flaticon-users"];
        $menuname = ["Home", "Group", "User"];
        $action = ["View", "View", "View"];
        $urlname = ["/home", "/group", "/user"];
        $routename = ["home", "group", "user"];
        $method = ["GET", "GET", "GET"];

        for ($i=0; $i < count($icon); $i++) { 
            # code...
            $menu             = new MasterMenu;
            $menu->category   = $category[$i];
            $menu->sort       = $sort[$i];
            $menu->icon       = $icon[$i];
            $menu->menuname   = $menuname[$i];
            $menu->action     = $action[$i];
            $menu->urlname    = $urlname[$i];
            $menu->routename  = $routename[$i];
            $menu->method     = $method[$i];
            $menu->save();
        }

        for ($i=0; $i < count($icon); $i++) { 
            # code...
            $menu             = new MasterMenu;
            $menu->category   = $category[$i];
            $menu->sort       = $sort[$i];
            $menu->icon       = $icon[$i];
            $menu->menuname   = $menuname[$i];
            $menu->action     = $action[$i];
            $menu->urlname    = $urlname[$i];
            $menu->routename  = $routename[$i];
            $menu->method     = $method[$i];
            $menu->save();
        }

        for ($i=0; $i < count($icon); $i++) { 
            # code...
            $menu             = new MasterMenu;
            $menu->category   = $category[$i];
            $menu->sort       = $sort[$i];
            $menu->icon       = $icon[$i];
            $menu->menuname   = $menuname[$i];
            $menu->action     = $action[$i];
            $menu->urlname    = $urlname[$i];
            $menu->routename  = $routename[$i];
            $menu->method     = $method[$i];
            $menu->save();
        }

    }
}
