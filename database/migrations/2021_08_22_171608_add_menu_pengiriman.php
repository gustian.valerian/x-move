<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMenuPengiriman extends Migration
{
    public $dataParent = [
        'category' => 'PR',
        'sort' => '#',
        'icon' => 'la la-comment',
        'menuname' => 'Pengiriman',
        'action' => 'View',
        'urlname' => '/pengiriman',
        'routename' => 'pengiriman',
        'method' => 'GET'
    ];

    public $data = [
        'category' => 'PR',
        'sort' => '1',
        'icon' => 'la la-comment',
        'menuname' => 'Kalkulasi Harga',
        'action' => 'View',
        'urlname' => '/kalkulasiharga',
        'routename' => 'kalkulasiharga',
        'method' => 'GET'
    ];

    public function up()
    {
        /// add transaksi menu parent
        DB::table('master_menus')->insert($this->dataParent);

        // add transaksi menu child
        DB::table('master_menus')->insert($this->data);

        // add map group menu
        $masterMenu = DB::table('master_menus')->select('id')->where('menuname', $this->data['menuname'])->first();

        DB::table('map_groups_menus')->insert([
            [
                'id_groups'     => '1', // group admin
                'id_menus'      => $masterMenu->id,
                'allow_view'    => true,
                'allow_import'  => true,
                'allow_export'  => true,
            ],
            [
                'id_groups'     => '2', // group pelanggan
                'id_menus'      => $masterMenu->id,
                'allow_view'    => true,
                'allow_import'  => true,
                'allow_export'  => true,
            ],
            [
                'id_groups'     => '3', // group marketing
                'id_menus'      => $masterMenu->id,
                'allow_view'    => true,
                'allow_import'  => true,
                'allow_export'  => true,
            ]
        ]);
    }

    public function down()
    {
        // delete map group menu first
        $masterMenu = DB::table('master_menus')->select('id')->where('menuname', $this->data['menuname'])->first();
        DB::table('map_groups_menus')->where('id_menus', $masterMenu->id)->delete();

        // delete menu parent first
        $menuNameParent = array_column($this->dataParent, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuNameParent)->delete();


        // delete menu child first
        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    }
}
