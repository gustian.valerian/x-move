<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMasterGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $data_basic = [
        [
            'name' => 'Super Admin',
        ],
        [
            'name' => 'Pelanggan',
        ],
        [
            'name' => 'Marketing',
        ]
    ];


    public function up()
    {
        DB::table('master_groups')->insert($this->data_basic);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menuName = array_column($this->data_basic, 'name');
        DB::table('master_groups')->whereIn('name', $menuName)->delete();
    }
}
