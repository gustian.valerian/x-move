<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenitipanBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penitipan_barang', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('created_by');
            $table->string('no_keranjang');
            $table->date('date_pickup_keranjang');
            $table->text('rencana_belanja');
            $table->text('belanjaan_digudang');
            $table->string('tracking_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penitipan_barang');
    }
}
