<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\MapGroupMenu;

class AddMenuUserTable extends Migration
{
    public $data = [
        [
            'category' => 'ZO',
            'sort' => '3',
            'icon' => 'la la-comment',
            'menuname' => 'Admin',
            'action' => 'View',
            'urlname' => '/admin',
            'routename' => 'admin',
            'method' => 'GET'
        ],
        [
            'category' => 'ZO',
            'sort' => '4',
            'icon' => 'la la-comment',
            'menuname' => 'Pelanggan',
            'action' => 'View',
            'urlname' => '/pelanggan',
            'routename' => 'pelanggan',
            'method' => 'GET'
        ],
        [
            'category' => 'ZO',
            'sort' => '5',
            'icon' => 'la la-comment',
            'menuname' => 'Marketing',
            'action' => 'View',
            'urlname' => '/marketing',
            'routename' => 'marketing',
            'method' => 'GET'
        ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // add transaksi menu child
        DB::table('master_menus')->insert($this->data);

        $menu = DB::table('master_menus')->where('menuname', 'Admin')->first();

        $groupmenu                = new MapGroupMenu;
        $groupmenu->id_groups     = 1; // Super Admin
        $groupmenu->id_menus      = $menu->id;
        $groupmenu->allow_view        = true;
        $groupmenu->allow_create      = true;
        $groupmenu->allow_update      = true;
        $groupmenu->allow_delete      = true;
        $groupmenu->allow_import      = true;
        $groupmenu->allow_export      = true;
        $groupmenu->save();


        $menu = DB::table('master_menus')->where('menuname', 'Pelanggan')->first();

        $groupmenu                = new MapGroupMenu;
        $groupmenu->id_groups     = 1; // Super Admin
        $groupmenu->id_menus      = $menu->id;
        $groupmenu->allow_view        = true;
        $groupmenu->allow_create      = true;
        $groupmenu->allow_update      = true;
        $groupmenu->allow_delete      = true;
        $groupmenu->allow_import      = true;
        $groupmenu->allow_export      = true;
        $groupmenu->save();


        $menu = DB::table('master_menus')->where('menuname', 'Marketing')->first();

        $groupmenu                = new MapGroupMenu;
        $groupmenu->id_groups     = 1; // Super Admin
        $groupmenu->id_menus      = $menu->id;
        $groupmenu->allow_view        = true;
        $groupmenu->allow_create      = true;
        $groupmenu->allow_update      = true;
        $groupmenu->allow_delete      = true;
        $groupmenu->allow_import      = true;
        $groupmenu->allow_export      = true;
        $groupmenu->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menu = DB::table('master_menus')->select('id')
                ->whereIn('menuname', ['Admin','Pelanggan','Marketing'])->delete();
                

        // delete menu child first
        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    }
}
