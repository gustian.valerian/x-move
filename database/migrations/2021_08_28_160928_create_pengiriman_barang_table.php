<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengirimanBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman_barang', function (Blueprint $table) {
            $table->id();
            $table->string('nomor_pengiriman');
            $table->date('tanggal');
            $table->bigInteger('created_by');
            $table->bigInteger('edited_by')->nullable();
            $table->string('nama_penerima');
            $table->string('zipcode_penerima');
            $table->string('telp_penerima');
            $table->bigInteger('id_negara');
            $table->string('provinsi_penerima');
            $table->string('kota_penerima');
            $table->text('alamat_penerima');
            $table->bigInteger('nilai_paket');
            $table->bigInteger('berat_paket');
            $table->text('deskripsi_paket');
            $table->bigInteger('berat_paket_real')->nullable();
            $table->bigInteger('potongan_harga')->nullable();
            $table->bigInteger('biaya_tambahan')->nullable();
            $table->bigInteger('ongkir')->nullable();
            $table->bigInteger('harga_real')->nullable();
            $table->string('payment')->nullable();
            $table->string('tracking_number')->nullable();
            $table->string('gambar')->nullable();
            $table->bigInteger('id_enum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman_barang');
    }
}
