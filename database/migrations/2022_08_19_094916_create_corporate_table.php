<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorporateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $data = [
        'category' => 'ZO',
        'sort' => '8',
        'icon' => 'la la-comment',
        'menuname' => 'Corporate',
        'action' => 'View',
        'urlname' => '/corporate',
        'routename' => 'corporate',
        'method' => 'GET'
    ];

    public function up()
    {
        Schema::create('corporate', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('value')->nullable();
            $table->string('type'); // boolean, string, image, doc
            $table->timestamps();
        });

        DB::table('corporate')->insert([
            [
                'key'     => 'Nama PT',
                'value'   => 'Xmove',
                'type'    => 'text',
            ],
            [
                'key'     => 'Logo PT',
                'value'   => null,
                'type'    => 'image',
            ],
            [
                'key'     => 'Background Login',
                'value'   => null,
                'type'    => 'image',
            ],
            [
                'key'     => 'Nama PT Login',
                'value'   => null,
                'type'    => 'text',
            ],
        ]);

        // add transaksi menu child
        DB::table('master_menus')->insert($this->data);

        // add map group menu
        $masterMenu = DB::table('master_menus')->select('id')->where('menuname', $this->data['menuname'])->first();

        DB::table('map_groups_menus')->insert([
            [
                'id_groups'     => '1', // group admin
                'id_menus'      => $masterMenu->id,
                'allow_view'    => true,
                'allow_create'  => true,
                'allow_update'  => true,
                'allow_delete'  => true,
                'allow_import'  => true,
                'allow_export'  => true,
            ],
            [
                'id_groups'     => '2', // group peserta
                'id_menus'      => $masterMenu->id,
                'allow_view'    => false,
                'allow_create'  => true,
                'allow_update'  => true,
                'allow_delete'  => true,
                'allow_import'  => true,
                'allow_export'  => true,
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // delete menu child first
        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();

        Schema::dropIfExists('corporate');
    }
}
