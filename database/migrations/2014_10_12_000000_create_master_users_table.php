<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class CreateMasterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('fullname');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('telpnumber')->nullable();
            $table->boolean('isActive')->nullable();
            $table->boolean('isVerify')->nullable(); // activate token //bool
            $table->string('tokenVerify')->nullable(); //token string
            $table->unsignedBigInteger('id_groups');
            $table->rememberToken();
            $table->timestamps();

            // $table->foreign('id_groups')->references('id')->on('master_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_users');
    }
}
