<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\MapGroupMenu;

class AddMapGroupMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $count = DB::table('master_menus')->count();
        // dd($count);
        for ($i=1; $i <= $count; $i++) { 
            # code...
            $groupmenu                = new MapGroupMenu;
            $groupmenu->id_groups     = 1;
            $groupmenu->id_menus      = $i;
            $groupmenu->allow_view        = true;
            $groupmenu->allow_create      = true;
            $groupmenu->allow_update      = true;
            $groupmenu->allow_delete      = true;
            $groupmenu->allow_import      = true;
            $groupmenu->allow_export      = true;
            $groupmenu->save();

            // $groupmenu                = new MapGroupMenu;
            // $groupmenu->id_groups     = 2;
            // $groupmenu->id_menus      = $i;
            // $groupmenu->allow_view        = false;
            // $groupmenu->allow_create      = true;
            // $groupmenu->allow_update      = true;
            // $groupmenu->allow_delete      = true;
            // $groupmenu->allow_import      = true;
            // $groupmenu->allow_export      = true;
            // $groupmenu->save();

            
            
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('map_groups_menus')->delete();
    }
}
