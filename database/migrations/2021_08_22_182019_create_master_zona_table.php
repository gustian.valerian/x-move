<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterZonaTable extends Migration
{
    // public $data = [
    //     [
    //         'nama_zona' => 'Zona A',
    //     ],
    //     [
    //         'nama_zona' => 'Zona B',
    //     ],
    //     [
    //         'nama_zona' => 'Zona C',
    //     ],
    //     [
    //         'nama_zona' => 'Zona D',
    //     ],
    //     [
    //         'nama_zona' => 'Zona E',
    //     ],
    //     [
    //         'nama_zona' => 'Zona F',
    //     ],
    //     [
    //         'nama_zona' => 'Zona G',
    //     ],
    //     [
    //         'nama_zona' => 'Zona H',
    //     ],
    // ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_zona', function (Blueprint $table) {
            $table->id();
            $table->string('nama_zona')->unique();
            $table->timestamps();
        });
        
        // Call seeder
        Artisan::call('db:seed', [
            '--class' => 'MasterZonaSeeder',
            '--force' => true // <--- add this line
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_zona');
    }
}
