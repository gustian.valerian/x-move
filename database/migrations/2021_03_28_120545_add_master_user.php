<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
use App\Models\MasterGroup;
use Illuminate\Support\Str;


class AddMasterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        // DB::table('master_users')->insert($this->data);
        $user = new User;
        $user->username = 'admin';
        $user->fullname = 'admin';
        $user->email    = 'admin@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= 1; // as admin
        $user->save();

        $user = new User;
        $user->username = 'pelanggan';
        $user->fullname = 'pelanggan';
        $user->email    = 'pelanggan@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= 2; // as peserta
        $user->save();

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // $menuName = array_column($this->data, 'username');
        DB::table('master_users')->whereIn('username', ['admin', 'peserta'])->delete();
    }
}
