<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMenuPurchaseRequest extends Migration
{
    public $dataParent = [
        'category' => 'P',
        'sort' => '#',
        'icon' => 'la la-comment',
        'menuname' => 'Purchase',
        'action' => 'View',
        'urlname' => '/purchase',
        'routename' => 'purchase',
        'method' => 'GET'
    ];

    public $data = [
            'category' => 'P',
            'sort' => '1',
            'icon' => 'la la-comment',
            'menuname' => 'Purchase Request',
            'action' => 'View',
            'urlname' => '/purchase-request',
            'routename' => 'purchaseRequest',
            'method' => 'GET'
    ];

    // public $data1 = [
    //     'category' => 'P',
    //     'sort' => '2',
    //     'icon' => 'la la-tools',
    //     'menuname' => 'Purchase Request1',
    //     'action' => 'View',
    //     'urlname' => '/purchase-request1',
    //     'routename' => 'purchaseRequest1',
    //     'method' => 'GET'
    // ];

    public function up()
    {
        // /// add transaksi menu parent
        // DB::table('master_menus')->insert($this->dataParent);

        // // add transaksi menu child
        // DB::table('master_menus')->insert($this->data);

        // // // add transaksi menu child
        // // DB::table('master_menus')->insert($this->data1);


        // // add map group menu
        // $masterMenu = DB::table('master_menus')->
        //         select('id')->
        //         where('menuname', $this->data['menuname'])->
        //         first();

        // DB::table('map_groups_menus')->insert([
        //     [
        //         'id_groups'     => '1', // group admin
        //         'id_menus'      => $masterMenu->id,
        //         'allow_view'    => true,
        //         'allow_create'  => true,
        //         'allow_update'  => true,
        //         'allow_delete'  => true,
        //         'allow_import'  => true,
        //         'allow_export'  => true,
        //     ]
        //     ,
        //     [
        //         'id_groups'     => '2', // group peserta
        //         'id_menus'      => $masterMenu->id,
        //         'allow_view'    => false,
        //         'allow_create'  => true,
        //         'allow_update'  => true,
        //         'allow_delete'  => true,
        //         'allow_import'  => true,
        //         'allow_export'  => true,
        //     ]
        // ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // // delete map group menu first
        // $masterMenu = DB::table('master_menus')->
        //         select('id')->
        //         where('menuname', $this->data['menuname'])->
        //         first();
        // DB::table('map_groups_menus')->where('id_menus', $masterMenu->id)->delete();

        // // delete menu parent first
        // $menuNameParent = array_column($this->dataParent, 'menuname');
        // DB::table('master_menus')->whereIn('menuname', $menuNameParent)->delete();


        // // delete menu child first
        // $menuName = array_column($this->data, 'menuname');
        // DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    }
}
