<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMasterUserGroup extends Migration
{
    public $data = [
        [
            'category' => 'Z',
            'sort' => '3',
            'icon' => 'la la-layer-user',
            'menuname' => 'Group Menu',
            'action' => 'View',
            'urlname' => '/groupmenu',
            'routename' => 'groupmenu',
            'method' => 'GET'
        ]
    ];

    public function up()
    {
        DB::table('master_menus')->insert($this->data);

        $group = DB::table('master_groups')->where('name', 'Super Admin')->first();
        $menu = DB::table('master_menus')->where('menuname', 'Group Menu')->first();
        
        DB::table('map_groups_menus')->insert([
            "id_groups"     => $group->id,
            "id_menus"      => $menu->id,
            "allow_view"    => true,
            "allow_create"    => true,
            "allow_update"    => true,
            "allow_delete"    => true,
            "allow_import"    => true,
            "allow_export"    => true
        ]);
    }


    public function down()
    {
        $group = DB::table('master_groups')->where('name', 'Super Admin')->first();
        $menu = DB::table('master_menus')->where('menuname', 'Group Menu')->first();

        DB::table('map_groups_menus')->where('id_groups', $group->id)->where("id_menus", $menu->id)->delete();

        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    }
}
