<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Enum;

class AddEnumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $enumVoucher = new Enum;
        $enumVoucher->key          = 1;
        $enumVoucher->value        = 'potongan harga %';
        $enumVoucher->category     = 'voucher';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 2;
        $enumVoucher->value        = 'potongan harga Rp';
        $enumVoucher->category     = 'voucher';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 3;
        $enumVoucher->value        = 'saldo';
        $enumVoucher->category     = 'voucher';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 1;
        $enumVoucher->value        = 'Laki-laki';
        $enumVoucher->category     = 'jenis_kelamin';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 2;
        $enumVoucher->value        = 'Perempuan';
        $enumVoucher->category     = 'jenis_kelamin';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 1;
        $enumVoucher->value        = 'Menunggu Paket Sampai Gudang X-Move';
        $enumVoucher->category     = 'status';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 2;
        $enumVoucher->value        = 'Paket Sudah Digudang X-Move';
        $enumVoucher->category     = 'status';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 3;
        $enumVoucher->value        = 'Menunggu Pembayaran Pelanggan';
        $enumVoucher->category     = 'status';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 4;
        $enumVoucher->value        = 'Menunggu Pembayaran Disetujui Admin';
        $enumVoucher->category     = 'status';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 5;
        $enumVoucher->value        = 'Selesai';
        $enumVoucher->category     = 'status';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 1;
        $enumVoucher->value        = 'Rekening';
        $enumVoucher->category     = 'payment';
        $enumVoucher->save();

        $enumVoucher = new Enum;
        $enumVoucher->key          = 1;
        $enumVoucher->value        = 'Text Persetujuan';
        $enumVoucher->category     = 'text';
        $enumVoucher->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::table('enums')->whereIn('value', ['potongan harga %', 'potongan harga Rp', 'saldo'])->delete();

        DB::table('enums')->where('category', 'jenis_kelamin')->delete();

        DB::table('enums')->where('category', 'status')->delete();
    }
}
