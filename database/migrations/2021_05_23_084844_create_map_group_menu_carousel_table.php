<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\MapGroupMenu;

class CreateMapGroupMenuCarouselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dataObjectMenu = DB::table('master_menus')->where('menuname', 'Carousel')->first();

        $groupmenu                = new MapGroupMenu;
        $groupmenu->id_groups     = 1; // Super Admin
        $groupmenu->id_menus      = $dataObjectMenu->id;
        $groupmenu->allow_view        = true;
        $groupmenu->allow_create      = true;
        $groupmenu->allow_update      = true;
        $groupmenu->allow_delete      = true;
        $groupmenu->allow_import      = true;
        $groupmenu->allow_export      = true;
        $groupmenu->save();

        // $groupmenu                = new MapGroupMenu;
        // $groupmenu->id_groups     = 2; // Admin
        // $groupmenu->id_menus      = $dataObjectMenu->id;
        // $groupmenu->allow_view        = false;
        // $groupmenu->allow_create      = true;
        // $groupmenu->allow_update      = true;
        // $groupmenu->allow_delete      = true;
        // $groupmenu->allow_import      = true;
        // $groupmenu->allow_export      = true;
        // $groupmenu->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('map_group_menu_carousel');
    }
}
