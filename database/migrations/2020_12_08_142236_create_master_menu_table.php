<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_menus', function (Blueprint $table) {
            $table->id();
            $table->string('category'); // A = purchasing
            $table->string('sort'); // 1 = Permintaan Pembelian, 2 = Pesanan Pembelian
            $table->string('icon');
            $table->string('menuname')->unique();
            $table->string('action')->nullable();
            $table->string('urlname')->nullable();
            $table->string('routename')->nullable();
            $table->string('method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_menus');
    }
}
