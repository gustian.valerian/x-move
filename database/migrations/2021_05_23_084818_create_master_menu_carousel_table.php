<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterMenuCarouselTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $data = [
        [
            'category' => 'Z',
            'sort' => '2',
            'icon' => 'la la-file',
            'menuname' => 'Carousel',
            'action' => 'View',
            'urlname' => '/carousel',
            'routename' => 'carousel',
            'method' => 'GET'
        ],
        
    ];


    public function up()
    {
        DB::table('master_menus')->insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    
    }
}
