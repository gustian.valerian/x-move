<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('key');
            $table->string('value');
            $table->string('category');
            $table->string('rekening')->unique()->nullable();
            $table->text('text_persetujuan_pengiriman_barang')->nullable();
            $table->text('text_persetujuan_penitipan_barang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enums');
    }
}
