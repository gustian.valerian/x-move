<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMasterMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public $data = [
        // [
        //     'category' => 'Z',
        //     'sort' => '1',
        //     'icon' => 'la la-layer-group',
        //     'menuname' => 'Group',
        //     'action' => 'View',
        //     'urlname' => '/group',
        //     'routename' => 'group',
        //     'method' => 'GET'
        // ],
        [
            'category' => 'Z',
            'sort' => '1',
            'icon' => 'la la-user',
            'menuname' => 'User',
            'action' => 'View',
            'urlname' => '/user',
            'routename' => 'user',
            'method' => 'GET'
        ]
    ];

    public function up()
    {
        DB::table('master_menus')->insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $menuName = array_column($this->data, 'menuname');
        DB::table('master_menus')->whereIn('menuname', $menuName)->delete();
    }
}
