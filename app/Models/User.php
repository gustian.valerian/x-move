<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'master_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'fullname', 'email', 
        'password', 'telpnumber', 'isActive', 
        'isVerify', 'tokenVerify', 'id_groups'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function group(){
        return $this->belongsTo('App\Models\MasterGroup', 'id_groups', 'id');
    }

    public function log(){
        return $this->hasMany('App\Models\Log', 'id', 'id_users');
    }

    public function pelanggan(){
        return $this->hasOne('App\Models\Pelanggan', 'id_users', 'id');
    }

    public function marketing(){
        return $this->hasOne('App\Models\Marketing', 'id_users', 'id');
    }
}
