<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;

class PengirimanBarang extends Model
{
    //
    use AutoNumberTrait;
    protected $table = 'pengiriman_barang';
    public $fillable = [
        'nomor_pengiriman',
        'tanggal',
        'created_by',
        'edited_by',
        'nama_penerima',
        'zipcode_penerima',
        'telp_penerima',
        'id_negara',
        'provinsi_penerima',
        'kota_penerima',
        'alamat_penerima',
        'nilai_paket',
        'berat_paket',
        'deskripsi_paket',
        'berat_paket_real',
        'potongan_harga',
        'biaya_tambahan',
        'harga_real',
        'payment',
        'tracking_number',
        'gambar',
        'id_enum',
        'ongkir'
    ];

    public $timestamps = true;

    public function getAutoNumberOptions()
    {
        // $pengirimanBarang = PengirimanBarang::where('tanggal', Carbon::today())->count();
        // if ($pengirimanBarang == 0) {
            return [
                'nomor_pengiriman' => [
                    'format' => 'XM'. date('ymd'). '?', // Format kode yang akan digunakan.
                    'length' => 4 // Jumlah digit yang akan digunakan sebagai nomor urut
                ]
            ];
        // } else {
        //     return [
        //         'nomor_pengiriman' => [
        //             'format' => 'XM'. date('ymd'). '?', // Format kode yang akan digunakan.
        //             'length' => 4 // Jumlah digit yang akan digunakan sebagai nomor urut
        //         ]
        //     ];
        // }
    }

    public function negara()
    {
        return $this->belongsTo('App\Models\MasterNegara', 'id_negara', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    public function enum()
    {
        return $this->belongsTo('App\Models\Enum', 'id_enum', 'id');
    }
}
