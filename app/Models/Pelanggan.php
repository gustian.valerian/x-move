<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class Pelanggan extends Model
{
    use AutoNumberTrait;

    protected $table = 'customer';
    public $fillable = [
        'id_users',
        'alamat',
        'npwp',
        'kodemember',
        'kode_pelanggan'
    ];

    public $timestamps = true;

    public function getAutoNumberOptions()
    {
        return [
            'kode_pelanggan' => [
                'format' => 'XMOVE.?', // Format kode yang akan digunakan.
                'length' => 5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function user(){
        return $this->hasOne('App\Models\User', 'id_users', 'id');
    }
}
