<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;

class Customer extends Model
{
    //
    use AutoNumberTrait;
    protected $table = 'customer';
    public $fillable = [
        'id_users',
        'alamat',
        'npwp'
    ];

    public $timestamps = true;

    public function getAutoNumberOptions()
    {
        return [
            'kode_pelanggan' => [
                'format' => 'XMOVE?', // Format kode yang akan digunakan.
                'length' => 5 // Jumlah digit yang akan digunakan sebagai nomor urut
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_users', 'id');
    }
}
