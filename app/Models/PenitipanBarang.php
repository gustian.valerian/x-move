<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alfa6661\AutoNumber\AutoNumberTrait;
use Carbon\Carbon;

class PenitipanBarang extends Model
{
    //
    use AutoNumberTrait;
    protected $table = 'penitipan_barang';
    public $fillable = [
        'created_by',
        'no_keranjang',
        'date_pickup_keranjang',
        'rencana_belanja',
        'belanjaan_digudang',
        'tracking_number'
    ];

    public $timestamps = true;

    public function getAutoNumberOptions()
    {
        // $penitipanBarang = PenitipanBarang::where('date_pickup_keranjang', Carbon::today())->count();
        // if ($penitipanBarang == 0) {
            return [
                'no_keranjang' => [
                    'format' => 'BOX'. date('ymd'). '?', // Format kode yang akan digunakan.
                    'length' => 4 // Jumlah digit yang akan digunakan sebagai nomor urut
                ]
            ];
        // } else {
        //     return [
        //         'no_keranjang' => [
        //             'format' => 'BOX'. date('ymd'). '?', // Format kode yang akan digunakan.
        //             'length' => 4 // Jumlah digit yang akan digunakan sebagai nomor urut
        //         ]
        //     ];
        // }
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }
}
