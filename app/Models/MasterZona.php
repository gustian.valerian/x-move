<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterZona extends Model
{
    protected $table = 'master_zona';
    public $fillable = [
        'nama_zona'
    ];

    public $timestamps = true;

    public function hargaZona()
    {
        return $this->hasMany('App\Models\MasterHargaZona', 'id_zona', 'id')->orderBy('berat');
    }
}
