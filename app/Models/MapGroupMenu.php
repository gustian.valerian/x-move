<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MapGroupMenu extends Model
{
    //
    protected $table = 'map_groups_menus';
    public $fillable = [ 
        'id_groups', 
        'id_menus',
        'key1',
        'key2',
        'value',

        'allow_view',
        'allow_create',
        'allow_update',
        'allow_delete',
        'allow_import',
        'allow_export',
    ];
    
    public $timestamps = true;

    public function group(){
        return $this->belongsTo('App\Models\MasterGroup', 'id_groups', 'id');
    }

    public function menu(){
        return $this->belongsTo('App\Models\MasterMenu', 'id_menus', 'id');
    }
}
