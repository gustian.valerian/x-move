<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMenu extends Model
{
    //
    protected $table = 'master_menus';
    public $fillable = [ 
        'category',
        'sort', 
        'icon',
        'menuname',
        'urlname',
        'routename',
        'method'
    ];
    
    public $timestamps = true;
 
    public function mapgroupmenu(){
        return $this->hasMany('App\Models\MapGroupMenu', 'id_menus', 'id');
    }

    // public function permissions(){
    //     return $this->belongsToMany('App\Models\Permission');
    // }
}
