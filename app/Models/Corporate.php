<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporate extends Model
{
    protected $table = 'corporate';
    public $fillable = [ 
        'key', 
        'value',
        'type',
    ];
    
    public $timestamps = true;
}
