<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
    protected $table = 'marketing';
    public $fillable = [
        'id_users',
        'kodemember',
    ];

    public $timestamps = true;

    public function user(){
        return $this->belongsTo('App\Models\User', 'id_users', 'id');
    }
}
