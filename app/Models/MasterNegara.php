<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class MasterNegara extends Model
{
    protected $table = 'master_negara';
    public $fillable = [
        'nama_negara',
        'id_zonas'
    ];

    public $timestamps = true;

    public function zona()
    {
        return $this->belongsTo('App\Models\MasterZona', 'id_zonas', 'id');
    }
}
