<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterHargaZona extends Model
{
    protected $table = 'master_harga_zona';
    public $fillable = [
        'id_zona',
        'berat',
        'harga'
    ];

    public $timestamps = true;

    public function zona()
    {
        return $this->belongsTo('App\Models\MasterZona', 'id_zona', 'id');
    }

    public function getharga($idzona, $berat){
        $select = MasterHargaZona::where('id_zona', '=', $idzona)->where('berat', '>=', $berat)->orderBy('berat', 'ASC')->first();
        // dd($select);
        return $select;
    }
}
