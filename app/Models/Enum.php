<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enum extends Model
{
    protected $table = 'enums';
    public $fillable = [ 
        'key',
        'value', 
        'category'
    ];
    
    public $timestamps = true;

    static function getEnumbycategory($category){
        // dd($category);
        return self::where('category', $category)
            ->orderBy('key', 'ASC')->get();
    }
}
