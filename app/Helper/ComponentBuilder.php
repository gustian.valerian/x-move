<?php

namespace App\Helper;

use Request;
use Illuminate\Support\Facades\Session;

use Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\UrlGenerator;
use App\Models\MasterMenu;
use App\Models\Enum;
use App\Models\TicketHeader;
use App\Models\TicketDetail;
use App\Models\Corporate;
use Illuminate\Support\Facades\Cache;


class ComponentBuilder {
    public static function sideMenuBuilder() {
        // $route = Route::currentRouteName();
        $currentroute = Route::currentRouteName();


        // example user_view, and explode
        $explodeRouteName = explode("_", $currentroute);
        $routenameRequest = $explodeRouteName[0] ?? null;
        $routenameActionRequest = $explodeRouteName[1] ?? null;
        
        $user = Auth::user();
        $id_groups = $user->id_groups;
        $name_groups = $user->group->name;
        Cache::forget($name_groups);
        if (Cache::has($name_groups)) {
            $parentMenu = Cache::get($name_groups);
        }else{
        $parentMenu = MasterMenu::
                    where('category', '!=', 'Z')->
                    where('sort', '=', '#')->
                    orderby('category','ASC')->
                    orderby('sort','ASC')->
                    get();
            Cache::put($name_groups, $parentMenu, $seconds = 1200); // 2 jam
        }
        // $parentMenu = Cache::get('listMenuParent');
        // dd($parentMenu);
        // dd($parentMenu);
        $printParentMenu = '';
        $printMenu = '';
        foreach ($parentMenu as $menu) {
                $childMenu = MasterMenu::
                                whereHas('mapgroupmenu', function($q) use ($id_groups){
                                    $q->
                                    where('allow_view', '=', true)->
                                    where('id_groups', $id_groups);
                                })->
                                where('category', '=', $menu->category)->
                                where('sort', '!=', '#')->
                                orderBy('sort', 'ASC')->
                                get();
                if(count($childMenu) <= 0){
                    continue;
                }
                
                $active = false;
                $printMenuChild = '';
                foreach ($childMenu as $child) {
                    # code...
                    // echo $child;
                    if($routenameRequest == $child->routename ){
                        $active = true;
                        $printMenuChild .= '<li class="active"><a class="menu-item" href="' . url("/") . $child->urlname . '"><i></i><span data-i18n="' . $child->menuname . '">' . $child->menuname . '</span></a></li>';
                        
                        // '<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
                    }else{
                        $printMenuChild .= '<li><a class="menu-item" href="' . url("/") . $child->urlname . '"><i></i><span data-i18n="' . $child->menuname . '">' . $child->menuname . '</span></a></li>';
                        
                        // '<li class="kt-menu__item " aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
                    }
                }
                
                if($active == true){
                    
                    $printParentMenu .= '<li class=" nav-item"><a href="#"><i class="'. $menu->icon .'"></i><span class="menu-title" data-i18n="' . $menu->menuname . '">' . $menu->menuname . '</span></a>' .
                                            '<ul class="menu-content">';
                    
                    // '<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
                    // '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
                    //     '<ul class="kt-menu__subnav">' .
                    //         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

                }else{
                    $printParentMenu .= '<li class=" nav-item"><a href="#"><i class="'. $menu->icon .'"></i><span class="menu-title" data-i18n="' . $menu->menuname . '">' . $menu->menuname . '</span></a>' .
                                            '<ul class="menu-content">';
                    // $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
                    // '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
                    //     '<ul class="kt-menu__subnav">' .
                    //         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

                }       
                // var_dump($menu->icon);   
                // print_r($printParentMenu) ;     
                
                $printParentMenu .= $printMenuChild;
                // $printParentMenu .= '</ul></div></li>';
                $printParentMenu .= '</ul></li>';
                $printMenu .= $printParentMenu;
                $active = false;
                $printParentMenu = '';
                $printMenuChild = '';
            // };
            // echo $menu;
        }
        // echo $printMenu;
        return $printMenu;
    }

    // public static function sideMenuBuilder() {
    //     // $route = Route::currentRouteName();
    //     $currentroute = Route::currentRouteName();


    //     // example user_view, and explode
    //     $explodeRouteName = explode("_", $currentroute);
    //     $routenameRequest = $explodeRouteName[0] ?? null;
    //     $routenameActionRequest = $explodeRouteName[1] ?? null;
    //     // dd($routenameRequest);
        
    //     $user = Auth::user();
    //     $id_groups = $user->id_groups;
    //     $name_groups = $user->group->name;
    //     // dd($name_groups);
    //     // dd(Auth::user()->group->name);
    //     // $dataMasterMenu = MasterMenu::
    //     //                     whereHas('mapgroupmenu', function($q) use ($id_groups){
    //     //                         $q->
    //     //                         where('allow_view', '=', true)->
    //     //                         where('id_groups', $id_groups);
    //     //                     })->
    //     //                     where('category', '!=', 'Z')->
    //     //                     orWhere('sort', '=', '#')->
    //     //                     get();
    //     // $parentMenu = $dataMasterMenu->where('sort', '=', '#');
    //     // Cache::pull('listMenuParent');
    //     if (Cache::has($name_groups)) {
    //         $parentMenu = Cache::get($name_groups);
    //     }else{
    //     $parentMenu = MasterMenu::
    //                 // whereHas('mapgroupmenu', function($q) use ($id_groups){
    //                 //     $q->
    //                 //     // where('allow_view', '=', true)->
    //                 //     where('id_groups', $id_groups);
    //                 // })->
    //                 where('category', '!=', 'Z')->
    //                 where('sort', '=', '#')->
    //                 get();
    //         Cache::put($name_groups, $parentMenu, $seconds = 1200); // 2 jam
    //     }
    //     // $parentMenu = Cache::get('listMenuParent');
    //     // dd($parentMenu);
    //     // dd($parentMenu);
    //     $printParentMenu = '';
    //     $printMenu = '';
    //     foreach ($parentMenu as $menu) {
    //         // echo MasterMenu::where('category', '=', $menu->category);
    //         // if ($menu->sort == '#') {
    //             $childMenu = MasterMenu::
    //                             whereHas('mapgroupmenu', function($q) use ($id_groups){
    //                                 $q->
    //                                 where('allow_view', '=', true)->
    //                                 where('id_groups', $id_groups);
    //                             })->
    //                             where('category', '=', $menu->category)->
    //                             where('sort', '!=', '#')->
    //                             orderBy('sort', 'ASC')->
    //                             get();
    //             if(count($childMenu) <= 0){
    //                 continue;
    //             }
                
    //             $active = false;
    //             $printMenuChild = '';
    //             foreach ($childMenu as $child) {
    //                 # code...
    //                 // echo $child;
    //                 if($routenameRequest == $child->routename ){
    //                     $active = true;
    //                     $printMenuChild .= '<li class="kt-menu__item  kt-menu__item--active" aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
    //                 }else{
    //                     $printMenuChild .= '<li class="kt-menu__item " aria-haspopup="true"><a href="' . url("/") . $child->urlname . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $child->menuname . '</span></a></li>';
    //                 }
    //             }

    //             if($active == true){
    //                 $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--open kt-menu__item--here" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
    //                 '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
    //                     '<ul class="kt-menu__subnav">' .
    //                         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

    //             }else{
    //                 $printParentMenu .= '<li class="kt-menu__item  kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon2-browser-2"></i><span class="kt-menu__link-text">'. $menu->menuname .'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>' . 
    //                 '<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>' .
    //                     '<ul class="kt-menu__subnav">' .
    //                         '<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">' . $menu->menuname . '</span></span></li>';

    //             }              
                
    //             $printParentMenu .= $printMenuChild;
    //             $printParentMenu .= '</ul></div></li>';
    //             $printMenu .= $printParentMenu;
    //             $active = false;
    //             $printParentMenu = '';
    //             $printMenuChild = '';
    //         // };
    //         // echo $menu;
    //     }
    //     // echo $printMenu;
    //     return $printMenu;
    // }

    public static function masterMenuBuilder() {
        $id_groups = Auth::user()->id_groups;

        $dataMasterMenu = MasterMenu::
                            whereHas('mapgroupmenu', function($q) use ($id_groups){
                                $q->where('allow_view', '=', true)->where('id_groups', $id_groups);
                            })->
                            where('category', 'Z')->
                            get();
        // dd($dataMasterMenu);
        $masterMenu = "";
        if($dataMasterMenu->count() == 0){
            $masterMenu = "<h4 class='col-md-12 col-lg-12 text-center mb-4'>No Menu Found</h4>";
        }
        // dd($dataMasterMenu->count());
        for ($i=0; $i < count($dataMasterMenu); $i++) { 
            # code...
            $masterMenu = $masterMenu . 
            '<div class="col-md-3 col-lg-3">
                <a href="'. url("/") . $dataMasterMenu[$i]['urlname'] .'">
                    <div class="kt-portlet kt-callout kt-callout--brand kt-callout--diagonal-bg">
                        <div class="kt-portlet__body">
                            <div class="kt-callout__body">
                                <div class="kt-callout__content">
                                    <h3 class="kt-callout__title text-center">'. $dataMasterMenu[$i]['menuname'] .'</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>';
        }



        return $masterMenu;
    }

    public static function getLastMessage($number){
        $ticketHeader = TicketHeader::where('ticket_number', app('request')->input('number'))->first();
        $ticketDetail = TicketDetail::where('id_ticket_header', $ticketHeader->id)->get();


        $html = '';
        foreach ($option as $key) {
            $html .= '<option value="'. $key->enum_code  .'">'. $key->enum_name .'</option>';
        }
        return $html;
    }

   

    public static function countMasterMenu(){
        $id_groups = Auth::user()->id_groups;

        $dataMasterMenu = MasterMenu::
                            whereHas('mapgroupmenu', function($q) use ($id_groups){
                                $q->where('allow_view', '=', true)->where('id_groups', $id_groups);
                            })->
                            where('category', 'Z')->
                            get();

        return count($dataMasterMenu);
    }

    public static function corporate($key){
        $corporate = Corporate::where('key', $key)->first()->value;

        return $corporate;
    }
}
