<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class InformasiPerizinanBangunan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->all());
        $validate1 = [];
        $validate2 = [];
        $validate3 = [];
        $validate45 = [];

        $izin = $request->izin_type;
        if ($izin == 1 ) {
            $validate1 = [
                "nama_pemohon" => 'required',
                "nik_pemohon" => 'required',
                "alamat_pemohon" => 'required',
                "nomor_telp" => 'required',
                "email_pemohon" => 'required',
                "jenis_perizinan_bangunan" => 'required',
                "nomor_perizinan" => 'required',
                "tanggal_perizinan" => 'required',
                "lokasi_bangunan_perizinan" => 'required',
                "surat_permohonan" => 'required',
                "scan_ktp_pemohon" => 'required',
                "scan_perizinan_diajukan" => 'required',
                "surat_kehilangan_kepolisian" => 'required',
                "upload_file_pengumuman_media" => 'required',
                "sertifikat_tanah" => 'required',
            ];
        }elseif($izin == 2){
            $validate1 = [
                "izin_type" => 'required',
                "nama_pemohon" => 'required',
                "nik_pemohon" => 'required',
                "alamat_pemohon" => 'required',
                "nomor_telp" => 'required',
                "email_pemohon" => 'required',
                "nama_pemilik_perizinan" => 'required',
                "alamat_pemilik_perizinan" => 'required',
                "nomor_telp_pemilik_perizinan" => 'required',
                "jenis_perizinan_bangunan" => 'required',
                "nomor_perizinan" => 'required',
                "tanggal_perizinan" => 'required',
                "lokasi_bangunan_perizinan" => 'required',
                "surat_permohonan" => 'required',
                "scan_ktp_pemohon" => 'required',
                "scan_perizinan_diajukan" => 'required',
                "surat_kehilangan_kepolisian" => 'required',
                "upload_file_pengumuman_media" => 'required',
                "sertifikat_tanah" => 'required',
                "surat_kuasa" => 'required',
            ];
        }elseif($izin == 3){
            $validate1 = [
                "nama_pemohon" => 'required',
                "nik_pemohon" => 'required',
                "alamat_pemohon" => 'required',
                "nomor_telp" => 'required',
                "email_pemohon" => 'required',
                "jenis_perizinan_bangunan" => 'required',
                "jenis_arsip" => 'required',
                "nomor_perizinan" => 'required',
                "tanggal_perizinan" => 'required',
                "lokasi_bangunan_perizinan" => 'required',
                "surat_permohonan" => 'required',
                "scan_ktp_pemohon" => 'required',
                "scan_perizinan_diajukan" => 'required',
                "sertifikat_tanah" => 'required',
            ];
        }elseif($izin == 4){
            $validate1 = [
                "nama_pemohon" => 'required',
                "nik_pemohon" => 'required',
                "alamat_pemohon" => 'required',
                "nomor_telp" => 'required',
                "email_pemohon" => 'required',
                "nama_pemilik_perizinan" => 'required',
                "alamat_pemilik_perizinan" => 'required',
                "nomor_telp_pemilik_perizinan" => 'required',
                "jenis_perizinan_bangunan" => 'required',
                "jenis_arsip" => 'required',
                "nomor_perizinan" => 'required',
                "tanggal_perizinan" => 'required',
                "lokasi_bangunan_perizinan" => 'required',
                "surat_permohonan" => 'required',
                "scan_ktp_pemohon" => 'required',
                "scan_perizinan_diajukan" => 'required',
                "sertifikat_tanah" => 'required',
                "surat_kuasa" => 'required',
            ];
        }elseif($izin == 4){
            $validate1 = [
                "nama_pemohon" => 'required',
                "nik_pemohon" => 'required',
                "alamat_pemohon" => 'required',
                "nomor_telp" => 'required',
                "email_pemohon" => 'required',
                "jenis_perizinan_bangunan" => 'required',
                "nomor_perizinan" => 'required',
                "lokasi_bangunan_perizinan" => 'required',
            ];
        }

        return $validate1;
        // dd($izin);
        // if ($izin == 1 || $izin == 2 || $izin == 3) {
        //     $validate1 = [
        //         'nama_pemohon'          => 'required',
        //         'nomor_telp'            => 'required',
        //         'email_pemohon'         => 'required',
        //         'nik_pemohon'           => 'required',
        //         'alamat_pemohon'        => 'required',
                
        //         'nomor_perizinan'       => 'required',
        //         'tanggal_perizinan'     => 'required',
        //         'pemilik_perizinan'     => 'required',
        //         'lokasi_bangunan'       => 'required',

        //         'path_surat_pemohon'          => 'required',
        //         'path_ktp_pemohon'            => 'required',
        //         'path_perizinan_pemohon'      => 'required',
        //         'path_sertifikat_tanah'       => 'required',
        //     ];
        // }
        
        // if ($izin == 2 || $izin == 3) {
        //     $validate2 = [
        //         'path_scan_ktp_pemilik' => 'required',
        //         'path_surat_kuasa'      => 'required',
        //     ];
        // }

        // if ($izin == 3) {
        //     $validate3 = [
        //         'path_scan_ajb'            => 'required',
        //         'path_scan_kartu_keluarga' => 'required',
        //     ];
        // }

        // if ($izin == 4 || $izin == 5) {
        //     $validate45 = [
        //         'nama_pemohon'          => 'required',
        //         'nomor_telp'            => 'required',
        //         'nik_pemohon'           => 'required',
        //         'email_pemohon'         => 'required',
        //         'alamat_pemohon'        => 'required',
        //         'lokasi_bangunan'       => 'required',
        //         'informasi_perizinan'   => 'required',
        //         'nomor_perizinan_bangunan' => 'required',
        //     ];
        // }

        // $validate = array_merge( $validate1, $validate2, $validate3, $validate45 );
        
        // return $validate;
    }

    public function messages()
    {
        // dd($request->all());
        $messages = [
            'nama_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nik_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'alamat_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nomor_telp.required'                 => 'Kolom :attribute tidak boleh kosong',
            'email_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'jenis_perizinan_bangunan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nomor_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'tanggal_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'lokasi_bangunan_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'surat_permohonan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'scan_ktp_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'scan_perizinan_diajukan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'surat_kehilangan_kepolisian.required'                 => 'Kolom :attribute tidak boleh kosong',
            'upload_file_pengumuman_media.required'                 => 'Kolom :attribute tidak boleh kosong',
            'sertifikat_tanah.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nama_pemilik_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'alamat_pemilik_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nomor_telp_pemilik_perizinan.required'                 => 'Kolom :attribute tidak boleh kosong',
            'surat_kuasa.required'                 => 'Kolom :attribute tidak boleh kosong',
            'jenis_arsip.required'                 => 'Kolom :attribute tidak boleh kosong',
        ];
        return $messages;
    }
}
