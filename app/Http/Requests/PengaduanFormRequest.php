<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PengaduanFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pemohon'          => 'required|min:5',
            'nomor_telp'            => 'required',
            'email'                 => 'required',
            'nik_pemohon'           => 'required',
            'alamat_pemohon'        => 'required',
            'nomor_perizinan'       => 'required',
            'tanggal_perizinan'     => 'required',
            'pemilik_perizinan'     => 'required',
            'lokasi_bangunan_perizinan' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'nama_pemohon.required'                 => 'Kolom :attribute tidak boleh kosong',
            'nama_pemohon.min'                      => 'Kolom :attribute minimal 5 karakter',
            'nomor_telp.required'                   => 'Kolom :attribute tidak boleh kosong',
            'email.required'                        => 'Kolom :attribute tidak boleh kosong',
            'nik_pemohon.required'                  => 'Kolom :attribute tidak boleh kosong',
            'alamat_pemohon.required'               => 'Kolom :attribute tidak boleh kosong',
            'nomor_perizinan.required'              => 'Kolom :attribute tidak boleh kosong',
            'nomor_perizinan.required'              => 'Kolom :attribute tidak boleh kosong',
            'tanggal_perizinan.required'            => 'Kolom :attribute tidak boleh kosong',
            'pemilik_perizinan.required'            => 'Kolom :attribute tidak boleh kosong',
            'lokasi_bangunan_perizinan.required'    => 'Kolom :attribute tidak boleh kosong',
            // 'body' => 'required',
        ];
    }
}
