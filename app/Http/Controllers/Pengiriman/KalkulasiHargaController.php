<?php

namespace App\Http\Controllers\Pengiriman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterNegara;
use App\Models\MasterHargaZona;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class KalkulasiHargaController extends Controller
{
    //
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('KalkulasiHarga');
            return $next($request);
        });
    }

    public function index()
    {
        $negara = MasterNegara::get()->sortBy('nama_negara');
        return view('pengiriman.kalkulasi_harga.index', ['menu' => $this->menus, 'negara' => $negara]);
    }

    public function price(Request $request, MasterHargaZona $masterhargazona)
    {
        $data = $request->input();
        $data = str_replace(",", "", $data);

        $volume = $data['panjang'] * $data['lebar'] * $data['tinggi'] / 5;
        if($volume >=  $data['berat']){
            $data['berat'] = $volume;
        }
        
        $harga = $masterhargazona->getharga($data['zona'], $data['berat']);
        return response()->json($harga);
    }
}