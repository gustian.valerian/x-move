<?php

namespace App\Http\Controllers\Pengiriman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PengirimanBarang;
use App\Models\MasterNegara;
use App\Models\MasterHargaZona;
use App\Models\User;
use App\Models\Enum;
use DataTables;
use Auth;
use File;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PengirimanBarangController extends Controller
{
    //
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Pengiriman Barang');
            return $next($request);
        });
    }

    public function index()
    {
        return view('pengiriman.pengirimanbarang.index', ['menu' => $this->menus]);
    }

    public function list()
    {
        if(Auth::user()->id_groups == 1){ 
            $model = PengirimanBarang::latest()->get();
        }
        else if(Auth::user()->id_groups == 2){ 
            $model = PengirimanBarang::where('created_by', Auth::user()->id)->latest()->get();
        }
         //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('id_enum', function ($data) {
                            return $data->enum->value;
                        })
                        // ->editColumn('ongkir', function ($data) {
                        //     return number_format($data->ongkir, 2,',','.');
                        // })
                        ->addColumn('id_customer', function ($data) {
                            if (empty($data->user->pelanggan->kode_pelanggan)) {
                                
                            } else {
                                return $data->user->pelanggan->kode_pelanggan;
                            }
                        })
                        ->addColumn('kode_member', function ($data) {
                            if (empty($data->user->pelanggan->kodemember)) {
                                
                            } else {
                                return $data->user->pelanggan->kodemember;
                            }
                        })
                        ->addColumn('show', function ($data) {
                            return route('pengirimanbarang_view_show', ['pengirimanbarang' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('pengirimanbarang_update_edit', ['pengirimanbarang' => $data->id]);
                            return $url;
                        })
                        ->addColumn('payment', function ($data) {
                            $url = route('pengirimanbarang_update_payment', ['pengirimanbarang' => $data->id]);
                            return $url;
                        })
                        ->editColumn('created_at', function ($data) {
                            return [
                               'display' => e($data->created_at),
                               'timestamp' => $data->created_at->timestamp
                            ];
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $negara = MasterNegara::orderBy('nama_negara', 'ASC')->get();
        $customer = User::where('id_groups', 2)->get();
        $text = Enum::where('category', 'text')->first();
        return view('pengiriman.pengirimanbarang.create', ['negara' => $negara, 'customer' => $customer, 'text' => $text]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = new PengirimanBarang;
            $saveData->created_by           = $data['customer'] ?? Auth::user()->id;
            if(Auth::user()->id_groups == 1) {
                $saveData->edited_by            = Auth::user()->id;
            }
            $saveData->tanggal              = date('Y-m-d');
            $saveData->nama_penerima        = $data['nama_penerima'];
            $saveData->zipcode_penerima     = $data['zipcode_penerima'];
            $saveData->telp_penerima        = $data['telp_penerima'];
            $saveData->id_negara            = $data['negara'];
            $saveData->provinsi_penerima    = $data['provinsi_penerima'];
            $saveData->kota_penerima        = $data['kota_penerima'];
            $saveData->alamat_penerima      = $data['alamat_penerima'];
            $saveData->nilai_paket          = $this->str($data['nilai_paket']);
            $saveData->berat_paket          = $this->str($data['berat_paket']);
            $saveData->deskripsi_paket      = $data['deskripsi_paket'];
            $saveData->ongkir               = $this->str($data['ongkir']);
            $saveData->harga_real           = $this->str($data['harga_real']);
            (Auth::user()->id_groups == 1) ? $saveData->id_enum = 6 : $saveData->id_enum = 6;

            if(Auth::user()->id_groups == 1) {
                $saveData->tracking_number      = $data['tracking_number']; // nullable
                $saveData->berat_paket_real     = $this->str($data['berat_paket_real']); // nullable
                $saveData->potongan_harga       = $this->str($data['potongan_harga']); // nullable
                $saveData->biaya_tambahan       = $this->str($data['biaya_tambahan']); // nullable
            }
            
          
            $saveData->save();

            return redirect()->route('pengirimanbarang_view_index')->with('alert-success', 'Pengiriman Barang berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Pengiriman Barang gagal di buat'.$th);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = PengirimanBarang::where('id', $id)->first();
        return view('pengiriman.pengirimanbarang.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = PengirimanBarang::where('id', $id)->first();
        $negara = MasterNegara::orderBy('nama_negara', 'ASC')->get();
        $enum = Enum::where('category', 'status')->get();
        $customer = User::where('id_groups', 2)->get();

        return view('pengiriman.pengirimanbarang.edit', ['model' => $model, 'negara' => $negara, 'enum' => $enum, 'customer' => $customer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function payment($id)
    {
        $model = PengirimanBarang::where('id', $id)->first();
        $negara = MasterNegara::orderBy('nama_negara', 'ASC')->get();
        $enum = Enum::where('category', 'status')->get();
        $rekening = Enum::where('id', 11)->get();
        $customer = User::where('id_groups', 2)->get();

        return view('pengiriman.pengirimanbarang.payment', ['model' => $model, 'negara' => $negara, 'enum' => $enum, 'rekening' => $rekening, 'customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function str($data)
    {
        $koma = str_replace(",", "", $data);
        $titik = str_replace(".", "", $koma);
        return $titik;
    }

    public function update(Request $request, $id)
    {
        // dd($request->input());
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = PengirimanBarang::find($id);
            // dd($saveData);
            $saveData->created_by           = $data['customer'] ?? Auth::user()->id;
            if(Auth::user()->id_groups == 1) {
                $saveData->edited_by            = Auth::user()->id;
            }
            $saveData->tanggal              = $data['tanggal'];
            $saveData->nama_penerima        = $data['nama_penerima'];
            $saveData->zipcode_penerima     = $data['zipcode_penerima'];
            $saveData->telp_penerima        = $data['telp_penerima'];
            $saveData->id_negara            = $data['negara'];
            $saveData->provinsi_penerima    = $data['provinsi_penerima'];
            $saveData->kota_penerima        = $data['kota_penerima'];
            $saveData->alamat_penerima      = $data['alamat_penerima'];
            $saveData->nilai_paket          = $this->str($data['nilai_paket']);
            $saveData->berat_paket          = $this->str($data['berat_paket']);
            $saveData->deskripsi_paket      = $data['deskripsi_paket'];
            $saveData->ongkir               = $this->str($data['ongkir']);
            $saveData->harga_real           = $this->str($data['harga_real']);
            $saveData->id_enum              = $data['enum'];
            
            if (isset($data['payment'])) {
                if ($this->str($data['payment']) == $this->str($data['harga_real'])) {
                    $saveData->payment              = $this->str($data['payment']);
                    if (!empty($_FILES["gambar"]["name"])) {
                        $imageName = time().'.'.$request->file('gambar')->getClientOriginalName();
                        $request->file('gambar')->move(public_path('img/uploads'), $imageName);
                        $saveData->gambar               = $imageName;
                    }
                }
                else {
                    return back()->with('alert-failed', 'Nominal Bayar harus sama dengan Harga');
                }
            }

            if(Auth::user()->id_groups == 1) {
                $saveData->tracking_number      = $data['tracking_number']; // nullable
                $saveData->berat_paket_real     = (!empty($data['berat_paket_real'])) ? $this->str($data['berat_paket_real']) : null; // nullable
                $saveData->potongan_harga       = (!empty($data['potongan_harga'])) ? $this->str($data['potongan_harga']) : null; // nullable
                $saveData->biaya_tambahan       = (!empty($data['biaya_tambahan'])) ? $this->str($data['biaya_tambahan']) : null; // nullable
            }
          
            $saveData->save();

            return redirect()->route('pengirimanbarang_view_index')->with('alert-success', 'Pengiriman Barang berhasil di ubah');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Pengiriman Barang gagal di ubah'.$th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $load = PengirimanBarang::find($id);
        if(Auth::user()->id_groups == 2 && empty($load->edited_by)) {
            PengirimanBarang::destroy($id);
        }
        elseif (Auth::user()->id_groups == 1) {
            File::delete('img/uploads/'.$load->gambar);
            PengirimanBarang::destroy($id);
        }
    }

    public function export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nomor Pengiriman');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Nama Pelanggan');
        $sheet->setCellValue('D1', 'Nama Penerima');
        $sheet->setCellValue('E1', 'Zip Code Penerima');
        $sheet->setCellValue('F1', 'Telephone Penerima');
        $sheet->setCellValue('G1', 'Negara Penerima');
        $sheet->setCellValue('H1', 'Provinsi Penerima');
        $sheet->setCellValue('I1', 'Kota Penerima');
        $sheet->setCellValue('J1', 'Alamat Penerima');
        $sheet->setCellValue('K1', 'Nilai Paket');
        $sheet->setCellValue('L1', 'Berat Paket');
        $sheet->setCellValue('M1', 'Deskripsi Paket');
        $sheet->setCellValue('N1', 'Berat Paket Real');
        $sheet->setCellValue('O1', 'Potongan Harga');
        $sheet->setCellValue('P1', 'Biaya Tambahan');
        $sheet->setCellValue('Q1', 'Harga Real');
        $sheet->setCellValue('R1', 'Tracking Number');
        $sheet->setCellValue('S1', 'Status');
        $sheet->setCellValue('T1', 'ID Customer');
        $data = PengirimanBarang::take(5000)->get();
        $cell = 2;
        foreach($data as $row){
            // dd($row->user->username);
            $sheet->setCellValue('A'.$cell, $row->nomor_pengiriman);
            $sheet->setCellValue('B'.$cell, $row->tanggal);
            $sheet->setCellValue('C'.$cell, $row->user->fullname);
            $sheet->setCellValue('D'.$cell, $row->nama_penerima);
            $sheet->setCellValue('E'.$cell, $row->zipcode_penerima);
            $sheet->setCellValue('F'.$cell, $row->telp_penerima);
            $sheet->setCellValue('G'.$cell, $row->negara->nama_negara);
            $sheet->setCellValue('H'.$cell, $row->provinsi_penerima);
            $sheet->setCellValue('I'.$cell, $row->kota_penerima);
            $sheet->setCellValue('J'.$cell, $row->alamat_penerima);
            $sheet->setCellValue('K'.$cell, $row->nilai_paket);
            $sheet->setCellValue('L'.$cell, $row->berat_paket);
            $sheet->setCellValue('M'.$cell, $row->deskripsi_paket);
            $sheet->setCellValue('N'.$cell, $row->berat_paket_real);
            $sheet->setCellValue('O'.$cell, $row->potongan_harga);
            $sheet->setCellValue('P'.$cell, $row->biaya_tambahan);
            $sheet->setCellValue('Q'.$cell, $row->harga_real);
            $sheet->setCellValue('R'.$cell, $row->tracking_number);
            $sheet->setCellValue('S'.$cell, $row->enum->value);
            $sheet->setCellValue('T'.$cell, $row->user->pelanggan->kode_pelanggan ?? '-');
            $cell++;
        }
        $writer = new Xlsx($spreadsheet);
        $filename = "Pengiriman Barang-" . Carbon::now()->toDateTimeString();       
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename  .'.xlsx"');
        $writer->save('php://output');
    }


    public function price(Request $request, MasterHargaZona $masterhargazona)
    {
        $data = $request->input();
       
        // if(str_replace(",", "", $data['berat_paket_real'] ?? 0) > str_replace(",", "", $data['berat_paket'])){
        //     $berat_paket = $data['berat_paket_real'] ?? 0;
        // }else{ 
        //     $berat_paket = $data['berat_paket'];
        // }

        if(isset($data['berat_paket_real'])){
            $berat_paket = $data['berat_paket_real'] ?? 0;
        }else{ 
            $berat_paket = $data['berat_paket'];
        }
        
        $negara = MasterNegara::find($data['negara']);
        // dd($negara);
        $harga = $masterhargazona->getharga($negara['id_zonas'], str_replace(",", "", $berat_paket));
        // dd($harga);
        // potongan_harga
        // biaya_tambahan
        if(isset($data['potongan_harga']) || isset($data['biaya_tambahan'])){
            $potongan = str_replace(",", "", $data['potongan_harga'] ?? 0);
            $tambahan = str_replace(",", "", $data['biaya_tambahan'] ?? 0);
            $ongkir = $harga->harga;
            $harga = $harga->harga + $tambahan - $potongan;
        }else{ 
            $ongkir = $harga->harga;
            $harga = $harga->harga;
        }
        return response()->json(['harga' => $harga, 'ongkir' => $ongkir]);
    }
}
