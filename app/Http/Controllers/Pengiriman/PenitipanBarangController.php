<?php

namespace App\Http\Controllers\Pengiriman;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PenitipanBarang;
use App\Models\User;
use App\Models\Enum;
use DataTables;
use Auth;
use Illuminate\Support\Str;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PenitipanBarangController extends Controller
{
    //
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Penitipan Barang');
            return $next($request);
        });
    }

    public function index()
    {
        return view('pengiriman.penitipanbarang.index', ['menu' => $this->menus]);
    }

    public function list()
    {
        if(Auth::user()->id_groups == 3){
            $model = PenitipanBarang::whereHas('user', function($q){
                $q->whereHas('pelanggan', function($qw){
                    $qw->
                    where('kodemember', Auth::user()->marketing->kodemember);
                });
            })
            ->orderBy('created_at', 'DESC')
            ->get();
        }
        else if(Auth::user()->id_groups == 1){ 
            $model = PenitipanBarang::orderBy('created_at', 'DESC')->get(); 
        }
        else if(Auth::user()->id_groups == 2){ 
            $model = PenitipanBarang::where('created_by', Auth::user()->id)
            ->orderBy('created_at', 'DESC')->get(); 
        }
        //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('created_by', function ($data) {
                            return $data->user->fullname;
                        })
                        ->editColumn('tracking_number', function ($data) {
                            return $data->tracking_number ?? '-';
                        })
                        ->addColumn('show', function ($data) {
                            return route('penitipanbarang_view_show', ['penitipanbarang' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('penitipanbarang_update_edit', ['penitipanbarang' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->id_groups != 3){
            $customer = User::where('id_groups', 2)->get();
        }else{ 
            $customer = User::where('id_groups', 2)->
                        whereHas('pelanggan', function($qw){
                            $qw->
                            where('kodemember', Auth::user()->marketing->kodemember);
                        })->get();
        }
        $text = Enum::where('category', 'text')->first();
       
        return view('pengiriman.penitipanbarang.create', ['customer' => $customer, 'text' => $text]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = new PenitipanBarang;
            $saveData->created_by               = $data['customer'] ?? Auth::user()->id;
            $saveData->date_pickup_keranjang    = date('Y-m-d');
            $saveData->rencana_belanja          = $data['rencana_belanja'];
            $saveData->belanjaan_digudang       = $data['belanjaan_digudang'] ?? '';
            $saveData->tracking_number          = $data['tracking_number'] ?? null;
          
            $saveData->save();

            return redirect()->route('penitipanbarang_view_index')->with('alert-success', 'Penitipan Barang berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Penitipan Barang gagal di buat'.$th);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = PenitipanBarang::where('id', $id)->first();
        return view('pengiriman.penitipanbarang.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = PenitipanBarang::where('id', $id)->first();
        if(Auth::user()->id_groups != 3){
            $customer = User::where('id_groups', 2)->get();
        }else{ 
            $customer = User::where('id_groups', 2)->
                        whereHas('pelanggan', function($qw){
                            $qw->
                            where('kodemember', Auth::user()->marketing->kodemember);
                        })->get();
        }

        return view('pengiriman.penitipanbarang.edit', ['model' => $model, 'customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = PenitipanBarang::find($id);
            // dd($saveData);
            $saveData->created_by               = $data['customer'] ?? Auth::user()->id;
            $saveData->date_pickup_keranjang    = $data['date_pickup_keranjang'];
            if(Auth::user()->id_groups != 3){
                $saveData->rencana_belanja          = $data['rencana_belanja'];
            }
            $saveData->belanjaan_digudang       = $data['belanjaan_digudang'] ?? '';
            $saveData->tracking_number          = $data['tracking_number'] ?? null; // nullable
          
            $saveData->save();

            return redirect()->route('penitipanbarang_view_index')->with('alert-success', 'Penitipan Barang berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Penitipan Barang gagal di buat'.$th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PenitipanBarang::destroy($id);
    }

    public function export()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nama Pelanggan');
        $sheet->setCellValue('B1', 'Tanggal Pickup Keranjang');
        $sheet->setCellValue('C1', 'Nomor Keranjang');
        $sheet->setCellValue('D1', 'Rencana Belanja');
        $sheet->setCellValue('E1', 'Belanjaan Di Gudang');
        $sheet->setCellValue('F1', 'Tracking Number');
        $sheet->setCellValue('G1', 'ID Customer');

        $data = PenitipanBarang::take(5000)->get();
        $cell = 2;
        foreach($data as $row){
            $sheet->setCellValue('A'.$cell, $row->user->fullname);
            $sheet->setCellValue('B'.$cell, $row->date_pickup_keranjang);
            $sheet->setCellValue('C'.$cell, $row->no_keranjang);
            $sheet->setCellValue('D'.$cell, $row->rencana_belanja);
            $sheet->setCellValue('E'.$cell, $row->belanjaan_digudang);
            $sheet->setCellValue('F'.$cell, $row->tracking_number);
            $sheet->setCellValue('G'.$cell, $row->user->pelanggan->kode_pelanggan ?? "-");
            $cell++;
        }
        $writer = new Xlsx($spreadsheet);
        $filename = "Penitipan Barang-" . Carbon::now()->toDateTimeString();       
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename  .'.xlsx"');
        $writer->save('php://output');
    }
}
