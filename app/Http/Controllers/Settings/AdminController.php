<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MasterGroup;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Admin');
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.admin.index', ['menu' => $this->menus]);
    }

    public function list()
    {
        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = User::where('id_groups', 1)->get(); 

        $dataTables = DataTables::of($model)
                        ->addColumn('show', function ($data) {
                            return route('admin_view_show', ['admin' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('admin_update_edit', ['admin' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('settings.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            $saveData = new User;
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];
            $saveData->email           = $data['email'];
            $saveData->password        = bcrypt($data['password']);
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            $saveData->id_groups       = 1;

          
            $saveData->save();

            return redirect()->route('admin_view_index')->with('alert-success', 'Admin berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Admin gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = User::where('id_groups', 1)->where('id', $id)->first();
        return view('settings.admin.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::where('id_groups', 1)->where('id', $id)->first();
        return view('settings.admin.edit', ['model'=>$model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            $saveData = User::find($id);
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];//$documentStep->id;
            $saveData->email           = $data['email'];

            if(isset($data['password'])){
                $saveData->password        = bcrypt($data['password']);
            }
            
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            $saveData->id_groups       = 1;

            $saveData->save();

            return redirect()->route('admin_view_index')->with('alert-success', 'Admin berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Admin gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
    }
}
