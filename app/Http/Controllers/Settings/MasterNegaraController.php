<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterZona;
use App\Models\MasterNegara;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class MasterNegaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Negara');
            return $next($request);
        });
    }

    public function index()
    {
        return view('settings.negara.index', ['menu' => $this->menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list()
    {
        $model = MasterNegara::query(); //with('zona'); //->query();

        $dataTables = DataTables::of($model)
            ->editColumn('id_zonas', function ($data) {
                return $data->zona->nama_zona;
            })
            ->addColumn('show', function ($data) {
                return route('negara_view_show', ['negara' => $data->id]);
            })
            ->addColumn('edit', function ($data) {
                $url = route('negara_update_edit', ['negara' => $data->id]);
                return $url;
            });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function create()
    {
        $zona = MasterZona::get();
        return view('settings.negara.create', ['zona' => $zona]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = new MasterNegara();
            $saveData->nama_negara        = $data['nama_negara'];
            $saveData->id_zonas        = $data['zona'];

            $saveData->save();

            return redirect()->route('negara_view_index')->with('alert-success', 'Negara berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Negara gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MasterNegara::where('id', $id)->first();
        return view('settings.negara.show', ['model' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MasterNegara::where('id', $id)->first();
        $zona = MasterZona::get();

        return view('settings.negara.edit', ['model' => $model, 'zona' => $zona]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = MasterNegara::find($id);
            // dd($saveData);
            $saveData->nama_negara        = $data['nama_negara'];
            $saveData->id_zonas        = $data['zona'];


            $saveData->save();

            return redirect()->route('negara_view_index')->with('alert-success', 'Negara berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Negara gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterNegara::destroy($id);
    }
}
