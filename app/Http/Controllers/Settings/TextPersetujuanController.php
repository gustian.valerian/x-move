<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Enum;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class TextPersetujuanController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Text Persetujuan');
            return $next($request);
        });
    }

    public function index()
    {
        $textpersetujuan = Enum::where('category', 'text')->first();
        return view('settings.textpersetujuan.index', ['menu' => $this->menus, 'textpersetujuan' => $textpersetujuan]);
    }

    public function ganti(request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = Enum::find($data['id']);
            // dd($saveData);
            $saveData->key                                = $data['key'];
            $saveData->value                              = $data['value'];
            $saveData->category                           = $data['category'];
            $saveData->text_persetujuan_pengiriman_barang = $data['text_persetujuan_pengiriman_barang'];
            $saveData->text_persetujuan_penitipan_barang  = $data['text_persetujuan_penitipan_barang'];

            $saveData->save();

            return redirect()->route('textpersetujuan_view_index')->with('alert-success', 'Text Persetujuan berhasil di update');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Text Persetujuan gagal di update'.$th);
        }
    }
}
