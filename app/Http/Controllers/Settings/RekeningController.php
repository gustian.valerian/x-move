<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Enum;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class RekeningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Rekening');
            return $next($request);
        });
    }

    public function index()
    {
        $rekening = Enum::where('category', 'payment')->first();
        return view('settings.rekening.index', ['menu' => $this->menus, 'rekening' => $rekening]);
    }

    public function ganti(request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = Enum::find($data['id']);
            // dd($saveData);
            $saveData->key              = $data['key'];
            $saveData->value            = $data['value'];
            $saveData->category         = $data['category'];
            $saveData->rekening         = $data['rekening'];

            $saveData->save();

            return redirect()->route('rekening_view_index')->with('alert-success', 'Rekening berhasil di update');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Rekening gagal di update'.$th);
        }
    }
}
