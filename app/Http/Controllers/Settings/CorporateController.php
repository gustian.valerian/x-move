<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Corporate;
use Illuminate\Support\Facades\Storage;

class CorporateController extends Controller
{

    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Corporate');
            return $next($request);
        });

        $this->corporate = new Corporate;
    }

    public function index(){
        $data = $this->corporate->get();
        // dd($data);
        return view('settings.corporate.index', ['data'=> $data, 'menu' => $this->menus]);
    }

    public function edit(){
        $data = $this->corporate->get();
        // dd($data);
        return view('settings.corporate.edit', ['data'=> $data, 'menu' => $this->menus]);
    }

    public function update(Request $request){
        // dd($request->all());

        if(isset($request->text)){
            foreach ($request->text as $key => $value) {
                $text = $this->corporate->where('id', $key)->first();
                $text->value = $value;
                $text->save();
            }
        }

        if(isset($request->image)){
            foreach ($request->file('image') as $key => $value) {

                $value->storeAs('public/berkas', $value->hashName());
                
                $text = $this->corporate->where('id', $key)->first();

                $latepath = $text->value;
                // unlink(storage_path('app/public/berkas/' . $latepath));
                
                $text->value = $value->hashName();
                $text->save();
            }
        }

        // dd($request->image);
        return redirect()->route('corporate_view_index')->with('alert-success', 'Corporate berhasil di ubah');

        // $data = $this->corporate->get();
        // return redirect('settings.corporate.index');
    }
}
