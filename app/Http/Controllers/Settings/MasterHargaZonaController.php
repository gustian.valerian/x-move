<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterHargaZona;

class MasterHargaZonaController extends Controller
{
    //
    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('HargaZona');
            return $next($request);
        });
    }

    public function destroy($id)
    {
        MasterHargaZona::destroy('id_zona', $id);
    }
}
