<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterZona;
use App\Models\MasterHargaZona;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class MasterZonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Zona');
            return $next($request);
        });
    }

    public function index()
    {
        return view('settings.zona.index', ['menu' => $this->menus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list()
    {
        $model = MasterZona::query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
            ->addColumn('show', function ($data) {
                return route('zona_view_show', ['zona' => $data->id]);
            })
            ->addColumn('edit', function ($data) {
                $url = route('zona_update_edit', ['zona' => $data->id]);
                return $url;
            });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function create()
    {
        $zona = MasterZona::get();
        return view('settings.zona.create', ['zona' => $zona]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            $berat = count($data['berat']);
            // dd($data);
            $saveData = new MasterZona();
            $saveData->nama_zona = $data['nama_zona'];

            $saveData->save();

            $id_zona = $saveData->id;

            if ($berat > 0) {
                for ($i = 0; $i < $berat; $i++) {
                    $saveData = new MasterHargaZona();
                    $saveData->id_zona      = $id_zona;
                    $saveData->berat        = $data['berat'][$i];
                    $saveData->harga        = $data['harga'][$i];

                    $saveData->save();
                }
            }

            return redirect()->route('zona_view_index')->with('alert-success', 'Zona berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Zona gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = MasterZona::where('id', $id)->first();
        return view('settings.zona.show', ['model' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = MasterZona::where('id', $id)->first();
        return view('settings.zona.edit', ['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            $berat = count($data['berat']);
            // dd($data);
            $saveData = MasterZona::find($id);
            $saveData->nama_zona    = $data['nama_zona'];

            $saveData->save();
            $id_zona = $saveData->id;

            MasterHargaZona::where('id_zona', $id_zona)->delete();

            if ($berat > 0) {
                for ($i = 0; $i < $berat; $i++) {
                    $saveData = new MasterHargaZona();
                    $saveData->id_zona      = $id_zona;
                    $saveData->berat        = $data['berat'][$i];
                    $saveData->harga        = $data['harga'][$i];

                    $saveData->save();
                }
            }

            return redirect()->route('zona_view_index')->with('alert-success', 'Zona berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Zona gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MasterHargaZona::destroy('id_zona', $id);
        MasterZona::destroy($id);
    }
}
