<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\MapGroupMenu;
use DataTables;
use Illuminate\Support\Facades\DB;

class MapGroupMenuController extends Controller
{
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Group Menu');
            return $next($request);
        });
    }

    public function index()
    {
        return view('master.master_group_menu.index', ['menu' => $this->menus]);
    }

    public function list()
    {

        $model = MasterGroup::query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->addColumn('show', function ($data) {
                            return route('groupmenu_view_show', ['groupmenu' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('groupmenu_update_edit', ['groupmenu' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }


    public function create()
    {
        // $listmenu = MasterMenu::with('mapgroupmenu')->get();
        $listmenu = MasterMenu::select('id', 'category', 'sort', 'menuname')
                    ->where('category', '<>', 'Z')
                    ->groupBy(['category', 'id', 'sort', 'icon', 'menuname'])
                    ->get();
        // return response()->json(['data' => $listmenu]);
        $listmenumaster = MasterMenu::select('id', 'category', 'sort', 'menuname')
                    ->where('category', 'Z')
                    ->orderBy('sort', 'ASC')
                    ->get();
        // return response()->json(['data' => $listmenumaster]);
        return view('master.master_group_menu.create', [ 'listmenu' => $listmenu,'listmenumaster' => $listmenumaster]);
    }


    public function store(Request $request)
    {

        try {
            
            $validatedData = $request->validate([
                'name' => ['required']
            ]);

            $saveData = MasterGroup::create($validatedData);
            
            $arrayMenu = [];
            $privilege = $request->privilege;
            foreach ($privilege as $key => $value) {
                // echo $saveData->id;
                array_push($arrayMenu,[
                    'id_groups' => $saveData->id, 
                    'id_menus' => $key,
                    'allow_view' => $this->decisionTrueFalse($value['view'] ?? false),
                    'allow_create' => $this->decisionTrueFalse($value['create'] ?? false),
                    'allow_update' => $this->decisionTrueFalse($value['update'] ?? false),
                    'allow_delete' => $this->decisionTrueFalse($value['delete'] ?? false),
                    'allow_import' => $this->decisionTrueFalse($value['import'] ?? false),
                    'allow_export' => $this->decisionTrueFalse($value['export'] ?? false),
                ]);
            }
            MapGroupMenu::insert($arrayMenu);
            return redirect()->route('groupmenu_view_index')->with('alert-success', 'Group Menu berhasil di buat');

        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Group Menu gagal di buat');

        }
    }

    public function show($id)
    {
        $model = MasterGroup::with('mapgroupmenu')->where('id', $id)->first();
        // dd($model);
        $listmenu = MasterMenu::select('id', 'category', 'sort', 'menuname')
                ->where('category', '<>', 'Z')
                ->groupBy(['category', 'id', 'sort', 'icon', 'menuname'])
                ->get();
        // return response()->json(['data' => $listmenu]);
        $listmenumaster = MasterMenu::select('id', 'category', 'sort', 'menuname')
                ->where('category', 'Z')
                ->orderBy('sort', 'ASC')
                ->get();
        // return response()->json(['data' => $listmenumaster]);

        return view('master.master_group_menu.show', ['model'=>$model, 'listmenu' => $listmenu,'listmenumaster' => $listmenumaster]);
    }

    public function edit($id)
    {
        $model = MasterGroup::with('mapgroupmenu')->where('id', $id)->first();
        // dd($model);
        $listmenu = MasterMenu::select('id', 'category', 'sort', 'menuname')
                ->where('category', '<>', 'Z')
                ->groupBy(['category', 'id', 'sort', 'icon', 'menuname'])
                ->get();
        // return response()->json(['data' => $listmenu]);
        $listmenumaster = MasterMenu::select('id', 'category', 'sort', 'menuname')
                ->where('category', 'Z')
                ->orderBy('sort', 'ASC')
                ->get();
        // return response()->json(['data' => $listmenumaster]);

        return view('master.master_group_menu.edit', ['model'=>$model, 'listmenu' => $listmenu,'listmenumaster' => $listmenumaster]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        try {
            $validatedData = $request->validate([
                'name' => ['required']
            ]);

            $saveData = MasterGroup::where('id', $id)->first();//->update($validatedData);
            $saveData->name = $request->name;
            $saveData->update();

            // set all previlege false by id_groups
            MapGroupMenu::where('id_groups', $saveData->id)->update([
                'allow_view'   => false,
                'allow_create' => false,
                'allow_update' => false,
                'allow_delete' => false,
                'allow_import' => false,
                'allow_export' => false,
            ]);

            $privilege = $request->privilege;
            foreach ($privilege as $key => $value) {
                MapGroupMenu::updateOrCreate([
                    'id_groups' => $saveData->id, 
                    'id_menus' => $key
                ],[
                    'allow_view' => $this->decisionTrueFalse($value['view'] ?? false),
                    'allow_create' => $this->decisionTrueFalse($value['create'] ?? false),
                    'allow_update' => $this->decisionTrueFalse($value['update'] ?? false),
                    'allow_delete' => $this->decisionTrueFalse($value['delete'] ?? false),
                    'allow_import' => $this->decisionTrueFalse($value['import'] ?? false),
                    'allow_export' => $this->decisionTrueFalse($value['export'] ?? false),
                ]);
            }
            
            return redirect()->route('groupmenu_view_index')->with('alert-success', 'Group berhasil di Update');

        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Group gagal di update ' . $th);
        }
    }

    public function destroy($id)
    {
        $delete = MasterGroup::destroy($id);
        $deleteMap = MapGroupMenu::where('id_groups', $id)->delete();
        // dd($delete);
    }

    public function decisionTrueFalse($data = false){
        if($data){
            return true;
        }

        return $data;
    }
}
