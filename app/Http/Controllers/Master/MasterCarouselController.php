<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\MasterCarousel;
use Illuminate\Http\Request;
use DataTables;

class MasterCarouselController extends Controller
{
    protected $menus;

    function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('Carousel');
            return $next($request);
        });
    }

    public function index()
    {
        return view('master.master_carousel.index', ['menu' => $this->menus]);
    }

    public function list()
    {


        $model = MasterCarousel::query();

        $dataTables = DataTables::of($model)
            ->addColumn('show', function ($data) {
                return route('carousel_view_show', ['carousel' => $data->id]);
            })
            ->addColumn('edit', function ($data) {
                $url = route('carousel_update_edit', ['carousel' => $data->id]);
                return $url;
            });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function create()
    {
        return view('master.master_carousel.create');
    }


    public function store(Request $request)
    {
        try {

            $data = $request->input();
            // dd($data);
            $saveData = new MasterCarousel;
            $saveData->title          = $data['title'] ?? "";
            $saveData->description    = $data['description'] ?? "";

            if ($request->hasFile('path_image')) {
                // dd('masuk sini');
                $path_image = $request->file('path_image');
                $path_image->storeAs('public/carousel/', $path_image->hashName());
                $saveData->path_image       = $path_image->hashName()  ?? null;
            }

            if ($saveData->save()) {
                return redirect()->route('carousel_view_index')->with('alert-success', 'Simpan Carousel Berhasil');
            }
        } catch (\Throwable $th) {
            if ($request->hasFile('path_image')) {
                unlink(storage_path('app/public/carousel/' . $path_image->hashName()));
            }

            return back()->with('alert-failed', 'Carousel Tidak dapat di simpan'); // .$th);

        }
    }


    public function show($id)
    {
        $model = MasterCarousel::where('id', $id)->first();
        return view('master.master_carousel.show', ['model' => $model]);
    }


    public function edit($id)
    {
        $model = MasterCarousel::where('id', $id)->first();
        return view('master.master_carousel.edit', ['model' => $model]);
    }


    public function update(Request $request, $id)
    {
        $data = $request->input();
        // dd($data);
        $saveData = MasterCarousel::find($id);
        $saveData->title          = $data['title'] ?? "";
        $saveData->description    = $data['description'] ?? "";

        if ($request->hasFile('path_image')) {
            // dd('test');
            unlink(storage_path('app/public/carousel/' . $saveData->path_image));

            $path_image = $request->file('path_image');
            $path_image->storeAs('public/carousel/', $path_image->hashName());
            $saveData->path_image       = $path_image->hashName()  ?? null;
        }

        if ($saveData->save()) {
            return redirect()->route('carousel_view_index')->with('alert-success', 'Update Carousel Berhasil');
        }
    }


    public function destroy($id)
    {
        $saveData = MasterCarousel::find($id);
        unlink(storage_path('app/public/carousel/' . $saveData->path_image));
        MasterCarousel::destroy($id);
    }
}
