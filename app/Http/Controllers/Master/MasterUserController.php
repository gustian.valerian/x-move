<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MasterGroup;
use App\Models\Customer;
use DataTables;
use Auth;
use Hash;
use Illuminate\Support\Str;

class MasterUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission('User');
            return $next($request);
        });
    }

    public function index()
    {
        return view('master.master_user.index', ['menu' => $this->menus]);
    }

    public function list()
    {
        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = User::query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('id_groups', function ($data) {
                            return $data->group->name;
                        })
                        ->addColumn('show', function ($data) {
                            return route('user_view_show', ['user' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            return route('user_update_edit', ['user' => $data->id]);
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = MasterGroup::get();
        return view('master.master_user.create', ['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = new User;
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];//$documentStep->id;
            $saveData->email           = $data['email'];
            $saveData->password        = bcrypt($data['password']);
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            // $saveData->isActive        = $data['isActive']; // nullable
            // $saveData->isVerify        = $data['isVerify'];
            $saveData->id_groups       = $data['group'];

          
            $saveData->save();

            return redirect()->route('user_view_index')->with('alert-success', 'User berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'User gagal di buat');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = User::where('id', $id)->first();
        return view('master.master_user.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::where('id', $id)->first();
        $group = MasterGroup::get();

        return view('master.master_user.edit', ['model'=>$model, 'group' => $group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = User::find($id);
            // dd($saveData);
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];//$documentStep->id;
            $saveData->email           = $data['email'];

            if(isset($data['password'])){
                $saveData->password        = bcrypt($data['password']);
            }
            
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            // $saveData->isActive        = $data['isActive']; // nullable
            // $saveData->isVerify        = $data['isVerify'];
            $saveData->id_groups       = $data['group'];

          
            $saveData->save();

            return redirect()->route('user_view_index')->with('alert-success', 'User berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'User gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
    }

    public function import()
    {
        return view('master.master_user.welcome');
    }

    public function export()
    {
        return view('master.master_user.welcome');
    }
}
