<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\MapGroupMenu;
use Auth;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function permission($menuname){
        // dd(auth()->user());
        // $idgroup = User::find($user);
        // dd($idgroup);
        // $idgroup = Auth::user()->id_groups;
        $menu = MapGroupMenu::where('id_groups', auth()->user()->id_groups)->
        whereHas('menu', function ($q) use ($menuname) { // 
            $q->where('menuname', $menuname);
        })
        // ->
        ->first();
        return $menu;
    }
}
