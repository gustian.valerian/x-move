<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Route;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }

    public function login(Request $request)
    {   

        // dd(\Route::current());
        // dd(str_split(\Route::current()->uri, 3)[0]);
        $input = $request->all();
        // dd($input);
        $this->validate($request, [
            'email' => 'required',
            // 'username' => 'required',
            'password' => 'required',
        ]);
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'])))
        {
            // return response()->json(['admin_sucsess']);
            // if (\Route::current()->getName() == 'LoginApi') {
            if (str_split(Route::current()->uri, 3) == 'api') {
                $token = auth()->user()->createToken('DCKTRP2021')->accessToken;
                return response()->json(['user'=> auth()->user(),'token' => $token], 200);
                // return response()->json(['status' => 200, 'message' => 'Login Successed']);
            }
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
    }
}
