<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Customer;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\UploadedFile;
use Auth;
use File;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware(function ($request, $next) {
    //         $this->menus = $this->permission('guest');
    //         return $next($request);
    //     });
    // }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:master_users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    public function create(array $data)
    {
        $request = request();
        // dd($data);

        $request->validate([
            'username' => 'required|unique:master_users',
            'email' => 'required|email|unique:master_users',
        ], 
        [
            'username.unique' => 'Username sudah terdaftar',
            'email.unique' => 'Email sudah terdaftar',
        ]);

        $user = User::create([
            'fullname' => $data['fullname'],
            'telpnumber' => $data['telpnumber'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'id_groups' => 2, // pelanggan
        ]);

        $saveCustomer = new Customer;
        $saveCustomer->id_users     = $user->id;
        $saveCustomer->alamat       = $data['alamat'];
        $saveCustomer->npwp         = $data['npwp'];

        if (!empty($data['kode_member'])) {
            $saveCustomer->kodemember   = $data['kode_member'] ?? 'X-Move';
        }

        if (!empty($_FILES['ktp']['name'])) {
            $imageName = time().'.'.$request->file('ktp')->getClientOriginalName();
            $request->file('ktp')->move(public_path('/img/uploads'), $imageName);
            $saveCustomer->ktp          = $imageName;
        }

        $saveCustomer->save();
        
        // Customer::create([
            //     'id_users' => $user->id,
            //     'alamat' => $data['alamat'],
            //     'npwp' => $data['npwp'],
            //     'kodemember' => $data['kode_member'] ?? null,
            //     'ktp' => $imageName ?? null
            // ]);

        return $user;

        // try {
        //     //code...
        //     // $data = $request->input();
        //     // dd($data);
        //     $saveData = new User;
        //     $saveData->username        = $data['username'];
        //     $saveData->fullname        = $data['fullname'];//$documentStep->id;
        //     $saveData->email           = $data['email'];
        //     $saveData->password        = bcrypt($data['password']);
        //     $saveData->telpnumber      = $data['telpnumber']; // nullable
        //     // $saveData->isActive        = $data['isActive']; // nullable
        //     // $saveData->isVerify        = $data['isVerify'];
        //     $saveData->id_groups       = 2;
          
        //     $saveData->save();

        //     $saveCustomer = new Customer;
        //     $saveCustomer->id_users = $saveData->id;
        //     $saveCustomer->alamat = $data['alamat'];
        //     $saveCustomer->npwp = $data['npwp'];

        //     if (!empty($_POST['kode_member'])) {
        //         $saveCustomer->kodemember  = $data['kode_member'];
        //         if (!empty($_FILES["ktp"]["name"])) {
        //             $ktp = $data['ktp'];
        //             $imageName = time().'.'.$ktp->file('ktp')->getClientOriginalName();
        //             $ktp->file('ktp')->move(public_path('img/uploads'), $imageName);
        //             $saveCustomer->ktp     = $imageName;

        //             $saveCustomer->save();
        //         }
        //     }

        // } catch (\Throwable $th) {
        //     return back()->with('alert-failed', 'User gagal di buat');
        // }
    }
}
