<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\UrlGenerator;
use App\Models\MasterMenu;


class MenuController extends Controller
{
    public function index()
    {
        // dd('test');
        $id_groups = Auth::user()->id_groups;
        // dd($id_groups);
        $dataMasterMenu = MasterMenu::
                            whereHas('mapgroupmenu', function($q) use ($id_groups){
                                $q->where('allow_view', '=', true)->where('id_groups', $id_groups);
                            })->
                            where('category', 'Z')->
                            get();
        // dd($dataMasterMenu);
        $masterMenu = "";
        if($dataMasterMenu->count() == 0){
            $masterMenu = "<h4 class='col-md-12 col-lg-12 text-center mb-4'>No Menu Found</h4>";
        }
        // dd($dataMasterMenu->count());
        for ($i=0; $i < count($dataMasterMenu); $i++) { 
            # code...
            // $masterMenu = $masterMenu . 
            // '<div class="col-md-3 col-lg-3">
            //     <a href="'. url("/") . $dataMasterMenu[$i]['urlname'] .'" style="color:black;">
            //         <div class="kt-portlet kt-callout--brand kt-callout--diagonal-bg">
            //             <div class="kt-portlet__body">
            //                 <div class="kt-callout__body">
            //                     <div class="kt-callout__content">
            //                         <h4 class="kt-callout__title text-center">'. $dataMasterMenu[$i]['menuname'] .'</h4>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </a>
            // </div>';

            // $masterMenu .= 
            $masterMenu .=
            '<div class="col-sm-12 col-md-6 col-lg-2">
                <a href="'. url("/") . $dataMasterMenu[$i]['urlname'] .'">
                    <div class="card text-white bg-warning text-center">
                        <div class="card-content">
                            <div class="card-body">
                                <h4 class="text-white">'. $dataMasterMenu[$i]['menuname'] .'</h4>
                        </div>
                        </div>
                    </div>
                </a>
            </div>';
            // </a>
        }

        return view('template.master.master', compact('masterMenu'));
    }
}
