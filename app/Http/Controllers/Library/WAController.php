<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WAController extends Controller
{
    public $base_url;
    public $token;
    public $sender;

    function __construct()
    {
        $this->base_url  = "http://207.148.78.159:8000/wa-v4/api";
        $this->token     = "c8dfe29e3a0946519cceb81b5f7e001f8f8f72ba";
        $this->sender    = "6287873434070";
    }

    public function getSendMessage($number = null, $message = null){
        // https://localhost/pijatku_web/public/api/send_message/087873434070/test%20gustian
        $response = Http::post( $this->base_url . '/send-message.php',
            [ 
                "api_key" => $this->token,
                "sender" => $this->sender,
                "number" => $number,
                "message" => $message
            ])->json();

        return response()->json($response);
    }
}
