<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArrayController extends Controller
{

    function __construct() {
        $this->middleware(function ($request, $next) {
            return $next($request);
        });
    }

    public function singleArray($data, $object){
        
        if(!$data){
            return null;
        }
        $array = [];
        for ($i=0; $i < count($data); $i++) { 
           array_push($array, $data[$i][$object]);
        }
        return $array;
    }

    public function singleArrayDateTime($data, $object){
        if(!$data){
            return null;
        }
        $array = [];
        for ($i=0; $i < count($data); $i++) { 
            $date = $data[$i][$object];
            $date = \explode("-", $date);
            
            $newDate = [ $date[0] , $date[1]-1 , $date[2]];
            array_push($array, $newDate);
        }
        return $array;
    }
}
