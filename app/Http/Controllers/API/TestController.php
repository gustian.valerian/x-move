<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(){
        $data = [
            [
                'category' => 'Z',
                'sort' => '3',
                'icon' => 'la la-layer-user',
                'menuname' => 'User Group',
                'action' => 'View',
                'urlname' => '/usergroup',
                'routename' => 'usergroup',
                'method' => 'GET'
            ],
            [
                'category' => 'Z',
                'sort' => '33',
                'icon' => 'la la-layer-user',
                'menuname' => 'User Group1',
                'action' => 'View1',
                'urlname' => '/usergroup1',
                'routename' => 'usergroup1',
                'method' => 'GET'
            ]
        ];

        $menuName = array_column($data, 'menuname');
        dd($menuName);

        foreach ($data as $key => $value) {
            dd($value['sort']);
        }

       
    }
}
