<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PengirimanBarang;
use App\Models\PenitipanBarang;
use App\Models\User;
use App\Models\MasterGroup;
use App\Models\Customer;
use DataTables;
use Auth;
use Hash;
use File;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    //

    public function index(){
        return view('dashboard');
    }

    public function list_pengiriman(){
        $model = PengirimanBarang::whereHas('user', function($q){
                                        $q->whereHas('pelanggan', function($qw){
                                            $qw->
                                            where('kodemember', Auth::user()->marketing->kodemember);
                                        });
                                    })
                                    ->get();    //query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('tracking_number', function ($data) {
                            return $data->tracking_number ?? "-";
                        })
                        ->editColumn('created_by', function ($data) {
                            return $data->user->fullname;
                        })
                        ->editColumn('id_negara', function ($data) {
                            return $data->negara->nama_negara;
                        })
                        ->editColumn('id_enum', function ($data) {
                            return $data->enum->value;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function list_penitipan(){

        $model = PenitipanBarang::whereHas('user', function($q){
                                        $q->whereHas('pelanggan', function($qw){
                                            $qw->
                                            where('kodemember', Auth::user()->marketing->kodemember);
                                        });
                                    })
                                    ->get();

        $dataTables = DataTables::of($model)
                        ->editColumn('tracking_number', function ($data) {
                            return $data->tracking_number ?? "-";
                        })
                        ->editColumn('created_by', function ($data) {
                            return $data->user->fullname;
                        })
                        ->editColumn('tracking_number', function ($data) {
                            return $data->tracking_number ?? '-';
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function profile()
    {
        return view('profile');
    }

    public function change(request $request)
    {
        $user = User::find(Auth::user()->id);
        $unique = '';
        if ($user->username == $request->username && $user->email == $request->email) {
            $unique = '';
        }
        else{
            $unique = '|unique:master_users';
        }
        
        $request->validate([
            'username' => 'required'.$unique,
            'email' => 'required|email'.$unique,
        ], 
        [
            'username.unique' => 'Username sudah terdaftar',
            'email.unique' => 'Email sudah terdaftar',
        ]);

        try {
            $data = $request->input();
            // dd($data);

            $saveUser = User::find(Auth::user()->id);
            // dd($saveUser);
            $saveUser->username         = $data['username'];
            $saveUser->fullname         = $data['fullname'];
            $saveUser->telpnumber       = $data['telpnumber'];
            $saveUser->email            = $data['email'];
            if (!empty($data['password'])) {
                $saveUser->password         = Hash::make($data['password']);
            }
            $saveUser->id_groups        = 2;

            $saveUser->save();

            $saveCustomer = Customer::where('id_users', $saveUser->id)->firstOrNew();
            // dd($saveCustomer);
            $saveCustomer->id_users     = $saveUser->id;
            $saveCustomer->alamat       = $data['alamat'];
            $saveCustomer->npwp         = $data['npwp'];

            if (!empty($data['kodemember'])) {
                $saveCustomer->kodemember   = $data['kodemember'];
            }

            if (!empty($_FILES['ktp']['name'])) {
                $load = Customer::where('id_users', $saveUser->id)->first();
                File::delete('img/uploads/'.$load->ktp);
                $imageName = time().'.'.$request->file('ktp')->getClientOriginalName();
                $request->file('ktp')->move(public_path('/img/uploads'), $imageName);
                $saveCustomer->ktp          = $imageName;
            }

            $saveCustomer->save();

            return redirect()->route('dashboard')->with('alert-success', 'Profile berhasil di update');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Profile gagal di update'.$th);
        }
    }
}
