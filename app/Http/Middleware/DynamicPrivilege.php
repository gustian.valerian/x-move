<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\MasterMenu;
use App\Models\MapGroupMenu;
use Illuminate\Support\Facades\Route;

class DynamicPrivilege
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get current routename
        //  $currentroute = Route::currentRouteName();
        $route = Route::getRoutes()->match($request);
        $currentroute = $route->getName();


        // example user_view, and explode
        $explodeRouteName = explode("_", $currentroute);
        $routenameRequest = $explodeRouteName[0] ?? null;
        $routenameActionRequest = $explodeRouteName[1] ?? null;
       

        // check to DB for previlege
        $idgroup = Auth::user()->id_groups;
        $idmapgroupmenu = MapGroupMenu::
                            select('allow_view', 'allow_create', 'allow_update',
                                'allow_delete', 'allow_import', 'allow_export')->
                            whereHas('menu', function ($q) use ($routenameRequest) {
                                $q->where('routename', $routenameRequest);
                            })->
                            where('id_groups', $idgroup)->
                            first();

        // decision true/false dari setiap action
        switch ($routenameActionRequest){
            case "view":
                if($idmapgroupmenu->allow_view == true){
                    return $next($request);
                }
                break;
            case "create":
                if($idmapgroupmenu->allow_create == true){
                    return $next($request);
                }
                break;
            case "update":
                if($idmapgroupmenu->allow_update == true){
                    return $next($request);
                }
                break;
            case "delete":
                if($idmapgroupmenu->allow_delete == true){
                    return $next($request);
                }
                break;
            case "import":
                if($idmapgroupmenu->allow_import == true){
                    return $next($request);
                }
                break;
            case "export":
                if($idmapgroupmenu->allow_export == true){
                    return $next($request);
                }
                break;
            default:
            return abort(403, 'Unauthorized action');
          }
        return abort(403, 'Unauthorized action');
    }
}
